/*!
 * Kendo UI Admin v1.0.0 by IKKI & Amikoko - https://ikki2000.github.io/
 * Copyright 2018-2021 IKKI Studio
 * Released under the MIT License - https://ikki2000.gitee.io/kendoui-admin-site/LICENSE
 */

/* JS for Website | Written by IKKI | 2018-02-03 */

// 配置路径
var path = $('base').attr('href'),
    webType = $('base').attr('type'),
    accentColor,
    minorColor,
    chartTheme,
    tokenUrl = 'json/logout.json',
    loginMenuUrl = 'json/site_login.json',
    logoutMenuUrl = 'json/site_logout.json';

/* 初始化 ****************************************************************************/
$(function () {
    // 主题
    if (localStorage.hasOwnProperty('colorName')) {
        accentColor = localStorage.getItem('accentColor');
        minorColor = localStorage.getItem('minorColor');
        chartTheme = localStorage.getItem('chartTheme');
        changeTheme(localStorage.getItem('colorName'), accentColor, minorColor, chartTheme);
    } else {
        accentColor = '#1890ff';
        minorColor = '#69c0ff';
        chartTheme = 'sass';
        changeTheme('default', accentColor, minorColor, chartTheme);
    }
    // 语言
    if (navigator.language === 'zh-CN' || navigator.language === 'zh-SG') {
        kendo.culture('zh-CHS');
        localStorage.setItem('culture', 'zh-CHS');
    } else if (navigator.language === 'zh-HK' || navigator.language === 'zh-MO' || navigator.language === 'zh-TW') {
        kendo.culture('zh-CHT');
        localStorage.setItem('culture', 'zh-CHT');
    } else if (navigator.language === 'en-US' || navigator.language === 'ru-RU' || navigator.language === 'fr-FR' || navigator.language === 'de-DE' || navigator.language === 'it-IT' || navigator.language === 'pt-PT' || navigator.language === 'es-ES' || navigator.language === 'ar-SA' || navigator.language === 'ja-JP' || navigator.language === 'ko-KR') {
        kendo.culture(navigator.language);
        localStorage.setItem('culture', navigator.language);
    } else {
        kendo.culture('en-US');
        localStorage.setItem('culture', 'en-US');
    }
});

// 发送 Token 验证
function tokenAuth() {
    $.fn.ajaxPost({
        ajaxAsync: false,
        ajaxData: {
            userId: sessionStorage.getItem('userId')
        },
        ajaxUrl: tokenUrl,
        succeed: function (res) {
            // 已登录导航数据获取
            $.fn.ajaxPost({
                ajaxUrl: loginMenuUrl,
                succeed: function (res) {
                    showNav(res);
                }
            });
        },
        failed: function (res) {
            logout();
        }
    });
}

// 导航显示
function showNav(res) {
    $('#navPanelBar').kendoPanelBar({
        dataSource: res.data
    });
    $('#menuH').kendoMenu({
        dataSource: res.data
    });
}

// 面包屑导航
function showPath(hash) {
    $('#path').html('');
    $.each($('#menuH').find('.links-'+ hash).children('.k-link').parents('.k-item'), function (i, doms) {
        $('#path').prepend('<span><i class="fas fa-angle-double-right"></i>' + $(doms).children('.k-link').html() + '</span>');
    });
    if (hash === '404') {
        $('#path').prepend('<span><i class="fas fa-angle-double-right"></i><i class="fas fa-info-circle"></i>404<small>Error</small></span>');
    }
    $('#path').prepend('<a href="' + webType + '/#/home' + '"><i class="fas fa-home"></i>首页<span><small>Home</small></span></a>');
}

// 主题
function changeTheme(theme, accent, minor, chart) {
    $('#LessCommon, #LessMobile').remove();
    if (chart === 'sass') {
        $('#Amikoko').attr('href', 'css/themes/theme_' + theme + '.min.css');
    } else {
        if (chart === 'bootstrap') {
            $('#Amikoko').before('<link id="LessCommon" href="css/themes/less/kendo.common-bootstrap.min.css" rel="stylesheet">');
        } else if (chart === 'material' || chart === 'materialblack') {
            $('#Amikoko').before('<link id="LessCommon" href="css/themes/less/kendo.common-material.min.css" rel="stylesheet">');
        } else if (chart === 'nova') {
            $('#Amikoko').before('<link id="LessCommon" href="css/themes/less/kendo.common-nova.min.css" rel="stylesheet">');
        } else if (chart === 'fiori') {
            $('#Amikoko').before('<link id="LessCommon" href="css/themes/less/kendo.common-fiori.min.css" rel="stylesheet">');
        } else if (chart === 'office365') {
            $('#Amikoko').before('<link id="LessCommon" href="css/themes/less/kendo.common-office365.min.css" rel="stylesheet">');
        } else {
            $('#Amikoko').before('<link id="LessCommon" href="css/themes/less/kendo.common.min.css" rel="stylesheet">');
        }
        $('#Amikoko').attr('href', 'css/themes/less/kendo.' + theme + '.min.css');
        $('#Amikoko').after('<link id="LessMobile" href="css/themes/less/kendo.' + theme + '.mobile.min.css" rel="stylesheet">');
    }
    if ($('#hasChart').length > 0) {
        setTimeout(function () {
            kendo.dataviz.autoTheme(true);
            refresh();
        }, 300);
    }
    localStorage.setItem('colorName', theme);
    localStorage.setItem('accentColor', accent);
    accentColor = accent;
    localStorage.setItem('minorColor', minor);
    minorColor = minor;
    localStorage.setItem('chartTheme', chart);
    chartTheme = chart;
}

// 语言
function changeLang(lang) {
    $.getScript('js/global/kendo.' + lang + '.js', function () {
        kendo.culture(lang);
        localStorage.setItem('culture', lang);
        refresh();
    });
}

// 退出登录
function logout() {
    sessionStorage.clear();
    // 未登录导航数据获取
    $.fn.ajaxPost({
        ajaxUrl: logoutMenuUrl,
        succeed: function (res) {
            showNav(res);
        }
    });
}