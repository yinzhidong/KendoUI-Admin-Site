$(function () {
    // 提示框
    tipMsg($('#tipMsgTop'), '这里是提示的内容~', 'top');
    tipMsg($('#tipMsgBottom'), '这里是提示的内容~', 'bottom');
    tipMsg($('#tipMsgLeft'), '这里是提示的内容~', 'left');
    tipMsg($('#tipMsgRight'), '这里是提示的内容~', 'right');
    tipMsg($('#tipMsgCenter'), '这里是提示的内容~', 'center');
    // 无角标提示框
    $('#calloutTooltip').kendoTooltip({
        position: 'top',
        content: '这里是提示的内容~',
        callout: false
    });
    // 指定宽高提示框
    $('#widthTooltip').kendoTooltip({
        position: 'top',
        content: '这里是提示的内容~',
        width: 180,
        height: 120
    });
    // 指定元素提示框
    $('#filterTooltip').kendoTooltip({
        position: 'top',
        content: '这里是提示的内容~',
        filter: '.theme-m-box'
    });
    // 偏移提示框
    $('#offsetTooltip').kendoTooltip({
        position: 'top',
        content: '这里是提示的内容~',
        offset: 10
    });
    // iFrame 提示框
    $('#iframeTooltip').kendoTooltip({
        position: 'top',
        iframe: true,
        content: {
            url: 'resource/page.html'
        },
        width: 360,
        height: 120
    });
    // 点击弹出提示框
    $('#clickTooltip').kendoTooltip({
        showOn: 'click',
        position: 'top',
        content: '这里是提示的内容~'
    });
    // 自定义提示框
    $('#customTooltip').kendoTooltip({
        autoHide: false,
        showOn: 'click',
        content:
            '<div class="media mx-1 my-2">' +
                '<img class="rounded-lg mr-3" src="img/IKKI.png" alt="IKKI">' +
                '<div class="media-body text-nowrap">' +
                    '<h4 class="mt-2 mb-4">IKKI</h4>' +
                    '<p>国籍：中国</p>' +
                    '<p>星座：凤凰座</p>' +
                    '<p>职业：前端攻城狮</p>' +
                '</div>' +
            '</div>'
    });
});