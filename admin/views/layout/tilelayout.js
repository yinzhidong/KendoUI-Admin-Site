$(function () {
    $('#tileLayout').kendoTileLayout({
        columns: 16,
        width: '100%',
        height: '100%',
        gap: {
            rows: 15,
            columns: 15
        },
        reorderable: true,
        resizable: true,
        containers: [
            {
                colSpan: 1,
                rowSpan: 1,
                header: {
                    text: '<small><i class="fas fa-grip-lines-vertical"></i>天马座</small>'
                },
                bodyTemplate: '<img class="w-100" src="img/temp/Pegasus.png" alt="天马座">'
            },
            {
                colSpan: 1,
                rowSpan: 1,
                header: {
                    text: '<small><i class="fas fa-grip-lines-vertical"></i><strong>一辉</strong></small>'
                },
                bodyTemplate: '<img class="w-100" src="img/avatar.png" alt="一辉">'
            },
            {
                colSpan: 2,
                rowSpan: 2,
                header: {
                    text: '<i class="fas fa-grip-lines-vertical"></i>双子座'
                },
                bodyTemplate: '<img class="w-100" src="img/temp/Gemini.png" alt="双子座">'
            },
            {
                colSpan: 2,
                rowSpan: 2,
                header: {
                    text: '<i class="fas fa-grip-lines-vertical"></i>仙女座'
                },
                bodyTemplate: '<img class="w-100" src="img/temp/Andromeda.png" alt="仙女座">'
            },
            {
                colSpan: 8,
                rowSpan: 4,
                header: {
                    text: '<i class="fas fa-grip-lines-vertical"></i>星系'
                },
                bodyTemplate: '<img class="w-100" src="img/lock_bg.jpg" alt="星系">'
            },
            {
                colSpan: 2,
                rowSpan: 2,
                header: {
                    text: '<i class="fas fa-grip-lines-vertical"></i>天龙座'
                },
                bodyTemplate: '<img class="w-100" src="img/temp/Dragon.png" alt="天龙座">'
            },
            {
                colSpan: 1,
                rowSpan: 1,
                header: {
                    text: '<small><i class="fas fa-grip-lines-vertical"></i>金牛座</small>'
                },
                bodyTemplate: '<img class="w-100" src="img/temp/Taurus.png" alt="金牛座">'
            },
            {
                colSpan: 1,
                rowSpan: 1,
                header: {
                    text: '<small><i class="fas fa-grip-lines-vertical"></i>山羊座</small>'
                },
                bodyTemplate: '<img class="w-100" src="img/temp/Capricorn.png" alt="山羊座">'
            },
            {
                colSpan: 4,
                rowSpan: 4,
                header: {
                    text: '<i class="fas fa-grip-lines-vertical"></i>女神'
                },
                bodyTemplate: '<img class="w-100" src="img/temp/Goddess.png" alt="女神">'
            },
            {
                colSpan: 2,
                rowSpan: 2,
                header: {
                    text: '<i class="fas fa-grip-lines-vertical"></i>凤凰座'
                },
                bodyTemplate: '<img class="w-100" src="img/temp/Phoenix.png" alt="凤凰座">'
            },
            {
                colSpan: 2,
                rowSpan: 2,
                header: {
                    text: '<i class="fas fa-grip-lines-vertical"></i>天鹅座'
                },
                bodyTemplate: '<img class="w-100" src="img/temp/Cygnus.png" alt="天鹅座">'
            },
            {
                colSpan: 2,
                rowSpan: 2,
                header: {
                    text: '<i class="fas fa-grip-lines-vertical"></i>狮子座'
                },
                bodyTemplate: '<img class="w-100" src="img/temp/Leo.png" alt="狮子座">'
            },
            {
                colSpan: 2,
                rowSpan: 2,
                header: {
                    text: '<i class="fas fa-grip-lines-vertical"></i>射手座'
                },
                bodyTemplate: '<img class="w-100" src="img/temp/Sagittarius.png" alt="射手座">'
            },
            {
                colSpan: 2,
                rowSpan: 2,
                header: {
                    text: '<i class="fas fa-grip-lines-vertical"></i>白羊座'
                },
                bodyTemplate: '<img class="w-100" src="img/temp/Aries.png" alt="白羊座">'
            },
            {
                colSpan: 2,
                rowSpan: 2,
                header: {
                    text: '<i class="fas fa-grip-lines-vertical"></i>天秤座'
                },
                bodyTemplate: '<img class="w-100" src="img/temp/Libra.png" alt="天秤座">'
            },
            {
                colSpan: 1,
                rowSpan: 1,
                header: {
                    text: '<small><i class="fas fa-grip-lines-vertical"></i>天蝎座</small>'
                },
                bodyTemplate: '<img class="w-100" src="img/temp/Scorpion.png" alt="天蝎座">'
            },
            {
                colSpan: 1,
                rowSpan: 1,
                header: {
                    text: '<small><i class="fas fa-grip-lines-vertical"></i>水瓶座</small>'
                },
                bodyTemplate: '<img class="w-100" src="img/temp/Aquarius.png" alt="水瓶座">'
            },
            {
                colSpan: 2,
                rowSpan: 2,
                header: {
                    text: '<i class="fas fa-grip-lines-vertical"></i>处女座'
                },
                bodyTemplate: '<img class="w-100" src="img/temp/Virgo.png" alt="处女座">'
            },
            {
                colSpan: 1,
                rowSpan: 1,
                header: {
                    text: '<small><i class="fas fa-grip-lines-vertical"></i>巨蟹座</small>'
                },
                bodyTemplate: '<img class="w-100" src="img/temp/Cancer.png" alt="巨蟹座">'
            },
            {
                colSpan: 1,
                rowSpan: 1,
                header: {
                    text: '<small><i class="fas fa-grip-lines-vertical"></i>双鱼座</small>'
                },
                bodyTemplate: '<img class="w-100" src="img/temp/Picses.png" alt="双鱼座">'
            }
        ]
    });
});