$(function () {
    // 圆点徽标
    $('#dotBadge').kendoBadge({
        shape: 'dot'
    });
    // 圆形徽标
    $('#circleBadge').kendoBadge({
        shape: 'circle',
        text: 88
    });
    // 胶囊徽标
    $('#pillBadge').kendoBadge({
        shape: 'pill',
        text: 'Badge'
    });
    // 圆角徽标
    $('#roundedBadge').kendoBadge({
        shape: 'rounded',
        text: 'Badge'
    });
    // 直角徽标
    $('#rectangleBadge').kendoBadge({
        shape: 'rectangle',
        text: 'Badge'
    });
    // 图标徽标
    $('#iconBadge').kendoBadge({
        icon: 'heart'
    });
    // 继承色徽标
    $('#inheritBadge').kendoBadge({
        themeColor: 'inherit',
        text: 'Badge'
    });
    // 默认色徽标
    $('#defaultBadge').kendoBadge({
        themeColor: 'default',
        text: 'Badge'
    });
    // 主色徽标
    $('#primaryBadge').kendoBadge({
        themeColor: 'primary',
        text: 'Badge'
    });
    // 次色徽标
    $('#secondaryBadge').kendoBadge({
        themeColor: 'secondary',
        text: 'Badge'
    });
    // 第三色徽标
    $('#tertiaryBadge').kendoBadge({
        themeColor: 'tertiary',
        text: 'Badge'
    });
    // 信息徽标
    $('#infoBadge').kendoBadge({
        themeColor: 'info',
        text: 'Badge'
    });
    // 成功徽标
    $('#successBadge').kendoBadge({
        themeColor: 'success',
        text: 'Badge'
    });
    // 警告徽标
    $('#warningBadge').kendoBadge({
        themeColor: 'warning',
        text: 'Badge'
    });
    // 错误徽标
    $('#errorBadge').kendoBadge({
        themeColor: 'error',
        text: 'Badge'
    });
    // 暗色徽标
    $('#darkBadge').kendoBadge({
        themeColor: 'dark',
        text: 'Badge'
    });
    // 亮色徽标
    $('#lightBadge').kendoBadge({
        themeColor: 'light',
        text: 'Badge'
    });
    // 反色徽标
    $('#invertedBadge').kendoBadge({
        themeColor: 'inverted',
        text: 'Badge'
    });
    // 实心徽标
    $('#solidBadge').kendoBadge({
        fill: 'solid',
        text: 'Badge'
    });
    // 空心徽标
    $('#outlineBadge').kendoBadge({
        fill: 'outline',
        text: 'Badge'
    });
    // 描边徽标
    $('#cutoutBorderBadge').kendoBadge({
        cutoutBorder: true,
        text: 'Badge'
    });
    // 内嵌徽标
    $('#inlineBadge').kendoBadge({
        position: 'inline'
    });
    // 左上角徽标
    $('#topStartBadge').kendoBadge({
        align: 'top start',
        position: 'edge'
    });
    // 右上角徽标
    $('#topEndBadge').kendoBadge({
        align: 'top end',
        position: 'edge'
    });
    // 左下角徽标
    $('#bottomStartBadge').kendoBadge({
        align: 'bottom start',
        position: 'edge'
    });
    // 右下角徽标
    $('#bottomEndBadge').kendoBadge({
        align: 'bottom end',
        position: 'edge'
    });
    // 边沿徽标
    $('#edgeBadge').kendoBadge({
        align: 'top end',
        position: 'edge'
    });
    // 内沿徽标
    $('#insideBadge').kendoBadge({
        align: 'top end',
        position: 'inside'
    });
    // 外沿徽标
    $('#outsideBadge').kendoBadge({
        align: 'top end',
        position: 'outside'
    });
    // 小徽标
    $('#smallBadge').kendoBadge({
        size: 'small',
        text: 88
    });
    // 中徽标
    $('#mediumBadge').kendoBadge({
        size: 'medium',
        text: 88
    });
    // 大徽标
    $('#largeBadge').kendoBadge({
        size: 'large',
        text: 88
    });
    // 最大值徽标
    $('#maxBadge').kendoBadge({
        text: 100,
        max: 99
    });
    // 自定义徽标
    $('#customBadge').kendoBadge({
        data: {
            current: 88,
            total: 100
        },
        template: '#= current # of #= total #'
    });
    // 隐藏徽标
    $('#hiddenBadge').kendoBadge({
        text: 0,
        visible: false
    });
});