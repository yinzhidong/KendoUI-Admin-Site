$(function () {
    // 普通进度条
    $('#generalProgressBar').kendoProgressBar();
    // 范围进度条
    $('#rangeProgressBar').kendoProgressBar({
        min: -100,
        max: 100
    });
    // 无值进度条
    $('#indeterminateProgressBar').kendoProgressBar({
        value: false
    });
    // 禁用进度条
    $('#disabledProgressBar').kendoProgressBar({
        enable: false,
        value: 61.8
    });
    // 默认值进度条
    $('#defaultValueProgressBar').kendoProgressBar({
        value: 61.8
    });
    // 反转进度条
    $('#reverseProgressBar').kendoProgressBar({
        reverse: true,
        value: 61.8
    });
    // 百分比进度条
    $('#percentProgressBar').kendoProgressBar({
        type: 'percent',
        value: 61.8
    });
    // 分块进度条
    $('#chunkProgressBar').kendoProgressBar({
        type: 'chunk',
        chunkCount: 10,
        value: 61.8
    });
    // 垂直进度条
    $('#verticalProgressBar').kendoProgressBar({
        orientation: 'vertical',
        value: 61.8
    });
    // 无状态进度条
    $('#noStatusProgressBar').kendoProgressBar({
        showStatus: false,
        value: 61.8
    });
    // 自定义进度条
    var customProgressBar = $('#customProgressBar').kendoProgressBar({
        animation: {
            duration: 200
        },
        min: 5,
        max: 16,
        change: function (e) {
            if (e.value < 6) {
                this.progressStatus.text('密码强度');
            } else if (e.value <= 7) {
                this.progressStatus.text('弱');
                this.progressWrapper.removeClass('bg-danger bg-warning bg-success bg-info').addClass('bg-danger');
            } else if (e.value <= 9) {
                this.progressStatus.text('中');
                this.progressWrapper.removeClass('bg-danger bg-warning bg-success bg-info').addClass('bg-warning');
            } else if (e.value <= 12) {
                this.progressStatus.text('强');
                this.progressWrapper.removeClass('bg-danger bg-warning bg-success bg-info').addClass('bg-success');
            } else {
                this.progressStatus.text('超强');
                this.progressWrapper.removeClass('bg-danger bg-warning bg-success bg-info').addClass('bg-info');
            }
        }
    }).data('kendoProgressBar');
    customProgressBar.progressStatus.text('密码强度');
    customProgressBar.value($('input[name="customProgressBar"]').val().length);
    $('input[name="customProgressBar"]').keyup(function () {
        customProgressBar.value(this.value.length);
    });
    // 动态进度条
    $('#animateProgressBar').kendoProgressBar({
        animation: {
            duration: 100
        },
        showStatus: false,
        change: function (e) {
            $('#animateStatus strong').text(e.value + '%');
            $('#animateStatus span').text('加载中……');
        },
        complete: function (e) {
            $('#animateStatus strong').text('');
            $('#animateStatus span').text('加载完成');
            $('#animateStatus button').show();
        }
    });
    progress();
    $('#animateStatus button').click(function () {
        $(this).hide();
        progress();
    });
    // 等宽进度条
    $('#widthProgressBar').kendoProgressBar({
        value: 61.8
    });
});

// 进度条变化
function progress() {
    var animateProgressBar = $('#animateProgressBar').data('kendoProgressBar');
    animateProgressBar.value(0);
    var intervalID = setInterval(function () {
        if (animateProgressBar.value() < 100) {
            animateProgressBar.value(animateProgressBar.value() + 1);
        } else {
            clearInterval(intervalID);
        }
    }, 100);
    router.bind('change', function () {
        clearInterval(intervalID);
    });
}