$(function () {
    // 普通加载中
    $('#generalLoader').kendoLoader();
    // 主色加载中
    $('#primaryLoader').kendoLoader({
        themeColor: 'primary'
    });
    // 次色加载中
    $('#secondaryLoader').kendoLoader({
        themeColor: 'secondary'
    });
    // 第三色加载中
    $('#tertiaryLoader').kendoLoader({
        themeColor: 'tertiary'
    });
    // 信息加载中
    $('#infoLoader').kendoLoader({
        themeColor: 'info'
    });
    // 成功加载中
    $('#successLoader').kendoLoader({
        themeColor: 'success'
    });
    // 警告加载中
    $('#warningLoader').kendoLoader({
        themeColor: 'warning'
    });
    // 错误加载中
    $('#errorLoader').kendoLoader({
        themeColor: 'error'
    });
    // 暗色加载中
    $('#darkLoader').kendoLoader({
        themeColor: 'dark'
    });
    // 亮色加载中
    $('#lightLoader').kendoLoader({
        themeColor: 'light'
    });
    // 反色加载中
    $('#invertedLoader').kendoLoader({
        themeColor: 'inverted'
    });
    // 按钮加载中
    $('#buttonLoader').kendoLoader({
        size: 'small'
    });
    // 两点加载中
    $('#pulsingLoader').kendoLoader({
        type: 'pulsing'
    });
    // 三点加载中
    $('#infiniteLoader').kendoLoader({
        type: 'infinite-spinner'
    });
    // 四点加载中
    $('#convergingLoader').kendoLoader({
        type: 'converging-spinner'
    });
    // 小加载中
    $('#smallLoader').kendoLoader({
        size: 'small'
    });
    // 中加载中
    $('#mediumLoader').kendoLoader({
        size: 'medium'
    });
    // 大加载中
    $('#largeLoader').kendoLoader({
        size: 'large'
    });
});