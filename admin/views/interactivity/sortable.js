$(function () {
    // 普通拖放排序
    $('#generalSortable').kendoSortable({
        cursor: 'move',
        hint: function (element) {
            return element.clone().width(element.width() + 16).addClass('theme-m-bg').css('opacity', .6);
        },
        placeholder: function (element) {
            return element.clone().addClass('theme-s-bg').css('opacity', .3);
        }
    });
    // 禁用拖放排序
    $('#disabledSortable').kendoSortable({
        cursor: 'move',
        disabled: '.disabled',
        hint: function (element) {
            return element.clone().width(element.width() + 16).addClass('theme-m-bg').css('opacity', .6);
        },
        placeholder: function (element) {
            return element.clone().addClass('theme-s-bg').css('opacity', .3);
        }
    });
    // 限定范围拖放排序
    $('#containerSortable').kendoSortable({
        container: $('#containerSortable'),
        cursor: 'move',
        hint: function (element) {
            return element.clone().addClass('theme-m-bg').css('opacity', .6);
        },
        placeholder: function (element) {
            return element.clone().addClass('theme-s-bg').css('opacity', .3);
        }
    });
    // 水平拖放排序
    $('#horizontalSortable').kendoSortable({
        cursor: 'move',
        axis: 'x',
        hint: function (element) {
            return element.clone().addClass('theme-m-bg').css({'filter': 'drop-shadow(0 12px 6px rgba(0, 0, 0, .6))', 'opacity': .6});
        },
        placeholder: function (element) {
            return element.clone().addClass('theme-s-bg').css({'filter': 'blur(2px)', 'opacity': .3});
        }
    });
    // 垂直拖放排序
    $('#verticalSortable').kendoSortable({
        cursor: 'move',
        axis: 'y',
        hint: function (element) {
            return element.clone().addClass('theme-m-bg').css({'filter': 'drop-shadow(0 12px 6px rgba(0, 0, 0, .6))', 'opacity': .6});
        },
        placeholder: function (element) {
            return element.clone().addClass('theme-s-bg').css({'filter': 'blur(2px)', 'opacity': .3});
        }
    });
    // 偏移拖放排序
    $('#offsetSortable').kendoSortable({
        cursor: 'move',
        cursorOffset: {
            top: 12,
            left: -36
        },
        hint: function (element) {
            return element.clone().addClass('theme-m-bg').css({'filter': 'drop-shadow(0 12px 6px rgba(0, 0, 0, .6))', 'opacity': .6});
        },
        placeholder: function (element) {
            return element.clone().addClass('theme-s-bg').css({'filter': 'blur(2px)', 'opacity': .3});
        }
    });
    // 自动滚动拖放排序
    $('#scrollSortable').kendoSortable({
        cursor: 'move',
        autoScroll: true,
        hint: function (element) {
            return element.clone().addClass('theme-m-bg').css('opacity', .6);
        },
        placeholder: function (element) {
            return element.clone().addClass('theme-s-bg').css({'filter': 'grayscale(100%)', 'opacity': .3});
        }
    });
    // 指定元素拖放排序
    $('#filterSortable').kendoSortable({
        filter: '.canSortable',
        cursor: 'move',
        hint: function (element) {
            return element.clone().addClass('theme-m-bg').css('opacity', .6);
        },
        placeholder: function (element) {
            return element.clone().addClass('theme-s-bg').css('opacity', .3);
        }
    });
    // 忽略元素拖放排序
    $('#ignoreSortable').kendoSortable({
        ignore: 'img',
        cursor: 'move',
        hint: function (element) {
            return element.clone().width(element.width() + 16).addClass('theme-m-bg').css('opacity', .6);
        },
        placeholder: function (element) {
            return element.clone().addClass('theme-s-bg').css('opacity', .3);
        }
    });
    // 手柄拖放排序
    $('#handlerSortable').kendoSortable({
        handler: '.k-i-handler-drag',
        cursor: 'move',
        hint: function (element) {
            return element.clone().width(element.width() + 16).addClass('theme-m-bg').css('opacity', .6);
        },
        placeholder: function (element) {
            return element.clone().addClass('theme-s-bg').css('opacity', .3);
        }
    });
    // 单向拖放排序
    $('#oneWaySortableLeft').kendoSortable({
        connectWith: '#oneWaySortableRight',
        cursor: 'move',
        hint: function (element) {
            return element.clone().width(element.width() + 16).addClass('theme-m-bg').css('opacity', .6);
        },
        placeholder: function (element) {
            return element.clone().addClass('theme-s-bg').css('opacity', .3);
        }
    });
    $('#oneWaySortableRight').kendoSortable({
        cursor: 'move',
        hint: function (element) {
            return element.clone().width(element.width() + 16).addClass('theme-m-bg').css('opacity', .6);
        },
        placeholder: function (element) {
            return element.clone().addClass('theme-s-bg').css('opacity', .3);
        }
    });
    // 双向拖放排序
    $('#twoWaySortableLeft').kendoSortable({
        connectWith: '#twoWaySortableRight',
        cursor: 'move',
        hint: function (element) {
            return element.clone().width(element.width() + 16).addClass('theme-m-bg').css('opacity', .6);
        },
        placeholder: function (element) {
            return element.clone().addClass('theme-s-bg').css('opacity', .3);
        }
    });
    $('#twoWaySortableRight').kendoSortable({
        connectWith: '#twoWaySortableLeft',
        cursor: 'move',
        hint: function (element) {
            return element.clone().width(element.width() + 16).addClass('theme-m-bg').css('opacity', .6);
        },
        placeholder: function (element) {
            return element.clone().addClass('theme-s-bg').css('opacity', .3);
        }
    });
    // 移动端拖放排序
    $('#mobileSortable').kendoSortable({
        cursor: 'move',
        holdToDrag: true,
        hint: function (element) {
            return element.clone().addClass('theme-m-bg').css('opacity', .6);
        },
        placeholder: function (element) {
            return element.clone().addClass('theme-s-bg').css({'filter': 'blur(2px)', 'opacity': .3});
        }
    });
});