$(function () {
    // DOM 文本域框
    $('#domTextArea').kendoTextArea();
    // 普通文本域框
    $('#generalTextArea').kendoTextArea({
        rows: 3,
        cols: 20,
        maxLength: 100,
        placeholder: '文本域框'
    });
    // 只读文本域框
    $('#readonlyTextArea').kendoTextArea({
        rows: 3,
        cols: 20,
        value: '中国'
    }).data('kendoTextArea').readonly();
    // 禁用文本域框
    $('#disabledTextArea').kendoTextArea({
        rows: 3,
        cols: 20
    }).data('kendoTextArea').enable(false);
    // 默认值文本域框
    $('#defaultValueTextArea').kendoTextArea({
        rows: 3,
        cols: 20,
        value: '中国'
    });
    // 标签文本域框
    $('#labelTextArea').kendoTextArea({
        rows: 3,
        cols: 20,
        label: '标签：'
    });
    // 浮动标签文本域框
    $('#floatingTextArea').kendoTextArea({
        rows: 3,
        cols: 20,
        label: {
            content: '浮动标签',
            floating: true
        }
    });
    // 调整大小文本域框
    $('#resizableTextArea').kendoTextArea({
        rows: 3,
        cols: 20,
        resizable: 'both'
    });
    // 水平调整大小文本域框
    $('#horizontalResizableTextArea').kendoTextArea({
        rows: 3,
        cols: 20,
        resizable: 'horizontal'
    });
    // 垂直调整大小文本域框
    $('#verticalResizableTextArea').kendoTextArea({
        rows: 3,
        cols: 20,
        resizable: 'vertical'
    });
    // 字数统计文本域框
    $('#countTextArea').kendoTextArea({
        rows: 3,
        cols: 20,
        maxLength: 100,
        resizable: 'both'
    }).on('input', function (e) {
        $('#counterValue span').text($(e.target).val().length);
    });
    // 等宽文本域框
    $('#widthTextArea').kendoTextArea({
        rows: 3,
        cols: 20
    });
});