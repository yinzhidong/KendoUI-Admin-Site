$(function () {
    // DOM 文本框
    $('#domTextBox').kendoTextBox();
    // 普通文本框
    $('#generalTextBox').kendoTextBox({
        placeholder: '文本框'
    });
    // 只读文本框
    $('#readonlyTextBox').kendoTextBox({
        value: '中国'
    }).data('kendoTextBox').readonly();
    // 禁用文本框
    $('#disabledTextBox').kendoTextBox().data('kendoTextBox').enable(false);
    // 默认值文本框
    $('#defaultValueTextBox').kendoTextBox({
        value: '中国'
    });
    // 标签文本框
    $('#labelTextBox').kendoTextBox({
        label: '标签：'
    });
    // 浮动标签文本框
    $('#floatingTextBox').kendoTextBox({
        label: {
            content: '浮动标签',
            floating: true
        }
    });
    // 等宽文本框
    $('#widthTextBox').kendoTextBox();
});