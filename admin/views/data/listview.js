$(function () {
    // 普通列表视图
    $('#generalListView').kendoListView({
        dataSource: {
            transport: {
                read: function (options) { readItem(options, 'json/list.json') }
            },
            schema: {
                total: 'total',
                data: 'data',
                model: {
                    id: 'id',
                    fields: {
                        userName: { type: 'string' },
                        realName: { type: 'string' },
                        nickName: { type: 'string' },
                        password: { type: 'string' },
                        confirmPassword: { type: 'string' },
                        online: { type: 'boolean' },
                        gender: { type: 'string' },
                        age: { type: 'number' },
                        height: { type: 'number' },
                        bloodType: { type: 'string' },
                        birthday: { type: 'date',
                            parse: function (e) {
                                return kendo.toString(kendo.parseDate(e), 'yyyy-MM-dd');
                            }
                        },
                        mateBirthday: { type: 'date',
                            parse: function (e) {
                                return kendo.toString(kendo.parseDate(e), 'yyyy-MM-dd');
                            }
                        },
                        creditCard: { type: 'string' },
                        asset: { type: 'number' },
                        nativePlace: { type: 'object' },
                        domicile: { type: 'object' },
                        nation: { type: 'object' },
                        zodiac: { type: 'object' },
                        language: { type: 'string' },
                        education: { type: 'object' },
                        graduation: { type: 'date',
                            parse: function (e) {
                                return kendo.toString(new Date(e), 'yyyy');
                            }
                        },
                        firstJob: { type: 'date',
                            parse: function (e) {
                                return kendo.toString(new Date(e), 'yyyy-MM');
                            }
                        },
                        mobile: { type: 'string' },
                        email: { type: 'string' },
                        homepage: { type: 'string' },
                        getUp: { type: 'date',
                            parse: function (e) {
                                return kendo.toString(kendo.parseDate(e), 'HH:mm');
                            }
                        },
                        importantMoment: { type: 'date',
                            parse: function (e) {
                                return kendo.toString(kendo.parseDate(e), 'yyyy-MM-dd HH:mm');
                            }
                        },
                        character: { type: 'number' },
                        color: { type: 'string' },
                        constellation: { type: 'object' },
                        tourism: { type: 'object' },
                        evaluation: { type: 'number' },
                        summary: { type: 'string' },
                        photo: { type: 'object' },
                        sign: { type: 'string' }
                    }
                }
            },
            pageSize: 3
        },
        template: kendo.template($('#listTemplate').html()),
        height: 750,
        scrollable: 'endless',
        navigatable: true,
        selectable: 'multiple'
    });
    // 拖放列表视图
    var dragListView = $('#dragListView').kendoListView({
        dataSource: {
            transport: {
                read: function (options) { readItem(options, 'json/list.json') }
            },
            schema: {
                total: 'total',
                data: 'data',
                model: {
                    id: 'id',
                    fields: {
                        userName: { type: 'string' },
                        realName: { type: 'string' },
                        nickName: { type: 'string' },
                        password: { type: 'string' },
                        confirmPassword: { type: 'string' },
                        online: { type: 'boolean' },
                        gender: { type: 'string' },
                        age: { type: 'number' },
                        height: { type: 'number' },
                        bloodType: { type: 'string' },
                        birthday: { type: 'date',
                            parse: function (e) {
                                return kendo.toString(kendo.parseDate(e), 'yyyy-MM-dd');
                            }
                        },
                        mateBirthday: { type: 'date',
                            parse: function (e) {
                                return kendo.toString(kendo.parseDate(e), 'yyyy-MM-dd');
                            }
                        },
                        creditCard: { type: 'string' },
                        asset: { type: 'number' },
                        nativePlace: { type: 'object' },
                        domicile: { type: 'object' },
                        nation: { type: 'object' },
                        zodiac: { type: 'object' },
                        language: { type: 'string' },
                        education: { type: 'object' },
                        graduation: { type: 'date',
                            parse: function (e) {
                                return kendo.toString(new Date(e), 'yyyy');
                            }
                        },
                        firstJob: { type: 'date',
                            parse: function (e) {
                                return kendo.toString(new Date(e), 'yyyy-MM');
                            }
                        },
                        mobile: { type: 'string' },
                        email: { type: 'string' },
                        homepage: { type: 'string' },
                        getUp: { type: 'date',
                            parse: function (e) {
                                return kendo.toString(kendo.parseDate(e), 'HH:mm');
                            }
                        },
                        importantMoment: { type: 'date',
                            parse: function (e) {
                                return kendo.toString(kendo.parseDate(e), 'yyyy-MM-dd HH:mm');
                            }
                        },
                        character: { type: 'number' },
                        color: { type: 'string' },
                        constellation: { type: 'object' },
                        tourism: { type: 'object' },
                        evaluation: { type: 'number' },
                        summary: { type: 'string' },
                        photo: { type: 'object' },
                        sign: { type: 'string' }
                    }
                }
            },
            pageSize: 12
        },
        layout: 'flex',
        flex: {
            wrap: 'wrap'
        },
        template: kendo.template(
            '<div class="listItem border-0 p-3 col-md-4 col-lg-3 col-xl-2">' +
                '<div class="card">' +
                    '<img class="card-img-top" src="#= photo.url #" alt="#= nickName #" title="#= realName #">' +
                    '<div class="card-footer p-2 text-center">' +
                        '<h5 class="mb-0"><strong class="text-dark">#= realName #</strong></h5>' +
                        '<h6 class="mb-0"><small class="text-black-50">#= nickName #</small></h6>' +
                    '</div>' +
                '</div>' +
            '</div>'
        ),
        dataBound: function () {
            this.element.css('cursor', 'move');
        }
    }).data('kendoListView');
    $('#dragListView').kendoSortable({
        filter: '.k-listview-content > div.listItem',
        cursor: 'move',
        hint: function (element) {
            return element.clone().css('opacity', .8);
        },
        placeholder: function (element) {
            return element.clone().css('opacity', .3);
        },
        change: function (e) {
            var dataItem = dragListView.dataSource.getByUid(e.item.data('uid'));
            dragListView.dataSource.remove(dataItem);
            dragListView.dataSource.insert(e.newIndex + dragListView.dataSource.skip(), dataItem);
        }
    });
});