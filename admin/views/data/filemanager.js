$(function () {
    // 普通文件管理
    $('#generalFileManager').kendoFileManager({
        dataSource: {
            transport: {
                create: function (options) {
                    $.fn.ajaxPost({
                        ajaxData: options.data,
                        ajaxUrl: 'json/response.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                },
                destroy: function (options) {
                    $.fn.ajaxPost({
                        ajaxData: options.data,
                        ajaxUrl: 'json/response.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                },
                update: function (options) {
                    $.fn.ajaxPost({
                        ajaxData: options.data,
                        ajaxUrl: 'json/response.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                },
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxData: options.data,
                        ajaxUrl: 'json/filemanager.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data',
                model: {
                    id: 'path',
                    children: 'items',
                    hasChildren: 'hasDirectories',
                    fields: {
                        name: { type: 'string',
                            editable: true,
                            defaultValue: '新建文件夹'
                        },
                        extension: { type: 'string',
                            editable: false
                        },
                        size: { type: 'number',
                            editable: false
                        },
                        path: { type: 'string',
                            editable: false
                        },
                        isDirectory: { type: 'boolean',
                            editable: false,
                            defaultValue: true
                        },
                        hasDirectories: { type: 'boolean',
                            editable: false,
                            defaultValue: false
                        },
                        created: { type: 'date',
                            editable: false
                        },
                        createdUtc: { type: 'date',
                            editable: false
                        },
                        modified: { type: 'date',
                            editable: false
                        },
                        modifiedUtc: { type: 'date',
                            editable: false
                        },
                        items: { type: 'object',
                            editable: false,
                            defaultValue: []
                        }
                    }
                }
            }
        },
        uploadUrl: 'json/upload.json'
    });
    // 自定义文件管理
    $('#customFileManager').kendoFileManager({
        dataSource: {
            transport: {
                create: function (options) {
                    $.fn.ajaxPost({
                        ajaxData: options.data,
                        ajaxUrl: 'json/response.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                },
                destroy: function (options) {
                    $.fn.ajaxPost({
                        ajaxData: options.data,
                        ajaxUrl: 'json/response.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                },
                update: function (options) {
                    $.fn.ajaxPost({
                        ajaxData: options.data,
                        ajaxUrl: 'json/response.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                },
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxData: options.data,
                        ajaxUrl: 'json/filemanager.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data',
                model: {
                    id: 'path',
                    children: 'items',
                    hasChildren: 'hasDirectories',
                    fields: {
                        name: { type: 'string',
                            editable: true,
                            defaultValue: '新建文件夹'
                        },
                        extension: { type: 'string',
                            editable: false
                        },
                        size: { type: 'number',
                            editable: false
                        },
                        path: { type: 'string',
                            editable: false
                        },
                        isDirectory: { type: 'boolean',
                            editable: false,
                            defaultValue: true
                        },
                        hasDirectories: { type: 'boolean',
                            editable: false,
                            defaultValue: false
                        },
                        created: { type: 'date',
                            editable: false
                        },
                        createdUtc: { type: 'date',
                            editable: false
                        },
                        modified: { type: 'date',
                            editable: false
                        },
                        modifiedUtc: { type: 'date',
                            editable: false
                        },
                        items: { type: 'object',
                            editable: false,
                            defaultValue: []
                        }
                    }
                }
            }
        },
        upload: {
            async: {
                saveUrl: 'json/upload.json',
                removeUrl: 'json/upload.json',
                batch: true
            },
            multiple: true,
            directory: true,
            directoryDrop: true,
            validation: {
                allowedExtensions: ['.html', '.css', '.js', '.json', '.png', '.jpg', '.gif'],
                minFileSize: 1024,
                maxFileSize: 10485760
            },
            success: function (e) {
                if (e.response.result === 'y') {
                    if (e.operation === 'upload') {
                        alertMsg(e.response.msg, 'success');
                    }
                } else {
                    $('.k-upload-files').remove();
                    alertMsg(e.response.msg, 'error');
                }
            }
        },
        width: 'auto',
        height: 520,
        initialView: 'grid',
        resizable: true,
        draggable: true,
        toolbar: {
            items: [
                {
                    name: 'createFolder'
                },
                {
                    name: 'upload'
                },
                {
                    name: 'sortDirection'
                },
                {
                    name: 'sortField'
                },
                {
                    name: 'changeView'
                },
                {
                    type: 'separator'
                },
                {
                    type: 'button',
                    text: '剪切',
                    icon: 'cut',
                    click: function (e) {
                        alertMsgNoBtn('你点击了<strong class="theme-m">' + e.target.text() + '</strong>~', 'info');
                    }
                },
                {
                    type: 'button',
                    text: '复制',
                    icon: 'copy',
                    click: function (e) {
                        alertMsgNoBtn('你点击了<strong class="theme-m">' + e.target.text() + '</strong>~', 'info');
                    }
                },
                {
                    type: 'button',
                    text: '粘帖',
                    icon: 'paste',
                    click: function (e) {
                        alertMsgNoBtn('你点击了<strong class="theme-m">' + e.target.text() + '</strong>~', 'info');
                    }
                },
                {
                    name: 'spacer'
                },
                {
                    name: 'details'
                },
                {
                    name: 'search'
                }
            ]
        },
        views: {
            tree: {
                loadOnDemand: true
            },
            grid: {
                allowCopy: {
                    delimeter: '\n'
                },
                reorderable: true,
                resizable: true
            },
            list: {
                navigatable: true
            }
        },
        previewPane: {
            metaFields: [
                'name',
                'extension',
                'size',
                'created',
                'modified'
            ],
            singleFileTemplate:
                '<div class="d-flex flex-column">' +
                    '<h3 class="theme-s mt-5">' +
                    '# if (data.selection[0].extension === "") { #' +
                        '<i class="fas fa-5x fa-folder-open"></i>' +
                    '# } else if (data.selection[0].extension === ".html") { #' +
                        '<i class="fab fa-5x fa-html5"></i>' +
                    '# } else if (data.selection[0].extension === ".css") { #' +
                        '<i class="fab fa-5x fa-css3-alt"></i>' +
                    '# } else if (data.selection[0].extension === ".js") { #' +
                        '<i class="fab fa-5x fa-js-square"></i>' +
                    '# } else if (data.selection[0].extension === ".json") { #' +
                        '<i class="fas fa-5x fa-code"></i>' +
                    '# } else if (data.selection[0].extension === ".png" || data.selection[0].extension === ".jpg" || data.selection[0].extension === ".gif") { #' +
                        '<i class="fas fa-5x fa-image"></i>' +
                    '# } #' +
                    '</h3>' +
                    '<h4 class="mb-5">#= data.selection[0].name + data.selection[0].extension #</h4>' +
                    '<dl class="row text-left">' +
                        '<dt class="col-sm-4 offset-sm-1">类型：</dt>' +
                        '<dd class="col-sm-7">' +
                        '# if (data.selection[0].extension === "") { #' +
                            '文件夹' +
                        '# } else { #' +
                            '#= data.selection[0].extension.substr(1) # 文件' +
                        '# } #' +
                        '</dd>' +
                        '<dt class="col-sm-4 offset-sm-1">大小：</dt>' +
                        '<dd class="col-sm-7">#= kendo.toString(data.selection[0].size / 1024, "0.00") # KB</dd>' +
                        '<dt class="col-sm-4 offset-sm-1">创建日期：</dt>' +
                        '<dd class="col-sm-7">#= kendo.toString(data.selection[0].created, "yyyy-MM-dd HH:mm") #</dd>' +
                        '<dt class="col-sm-4 offset-sm-1">修改日期：</dt>' +
                        '<dd class="col-sm-7">#= kendo.toString(data.selection[0].modified, "yyyy-MM-dd HH:mm") #</dd>' +
                    '</dl>' +
                '</div>',
            multipleFilesTemplate:
                '<div class="d-flex flex-column">' +
                    '<h3 class="theme-s mt-5">' +
                        '<i class="fas fa-5x fa-copy"></i>' +
                    '</h3>' +
                    '<h4 class="my-5">已选择 #= data.selection.length # 个项</h4>' +
                    '# var totalSize = 0; #' +
                    '# for (var i = 0; i < data.selection.length; i++) { #' +
                        '# totalSize += data.selection[i].size; #' +
                    '# } #' +
                    '<h6><strong>总大小：</strong>#= kendo.toString(totalSize / 1024, "0.00") # KB</h6>' +
                '</div>',
            noFileTemplate:
                '<p class="my-5"><i class="fas fa-5x fa-couch"></i></p>' +
                '<p>请先点选左侧文件或文件夹</p>'
        },
        breadcrumb: {
            rootIcon: 'globe'
        },
        contextMenu: {
            items: [
                {
                    name: 'rename'
                },
                {
                    name: 'delete'
                },
                {
                    text: '属性',
                    spriteCssClass: 'k-icon k-i-txt',
                    url: 'javascript:;'
                }
            ]
        },
        dialogs: {
            upload: {
                animation: {
                    open: {
                        effects: 'fade:in'
                    },
                    close: {
                        effects: 'fade:out'
                    }
                }
            },
            moveConfirm: {
                animation: {
                    open: {
                        effects: 'fade:in'
                    },
                    close: {
                        effects: 'fade:out'
                    }
                }
            },
            deleteConfirm: {
                animation: {
                    open: {
                        effects: 'fade:in'
                    },
                    close: {
                        effects: 'fade:out'
                    }
                }
            }
        }
    });
});