$(function () {
    // 定义数据源
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) { readItem(options, 'json/grid.json') }
        },
        schema: {
            total: 'total',
            data: 'data',
            model: {
                id: 'id',
                fields: {
                    userName: { type: 'string' },
                    realName: { type: 'string' },
                    nickName: { type: 'string' },
                    password: { type: 'string' },
                    confirmPassword: { type: 'string' },
                    online: { type: 'boolean' },
                    gender: { type: 'string' },
                    age: { type: 'number' },
                    height: { type: 'number' },
                    bloodType: { type: 'string' },
                    birthday: { type: 'date' },
                    mateBirthday: { type: 'date' },
                    creditCard: { type: 'string' },
                    asset: { type: 'number' },
                    nativePlace: { type: 'string',
                        parse: function (e) {
                            return e.provinceName + ' - ' + e.cityName + ' - ' + e.areaName;
                        }
                    },
                    domicile: { type: 'string',
                        parse: function (e) {
                            return e.name;
                        }
                    },
                    nation: { type: 'string',
                        parse: function (e) {
                            return e.nationName;
                        }
                    },
                    zodiac: { type: 'string',
                        parse: function (e) {
                            return e.zodiacName;
                        }
                    },
                    language: { type: 'string' },
                    education: { type: 'string',
                        parse: function (e) {
                            var str = '';
                            for (var i = 0; i < e.length; i++) {
                                if (e[i] === '1') {
                                    str += '小学 ';
                                } else if (e[i] === '2') {
                                    str += '初中 ';
                                } else if (e[i] === '3') {
                                    str += '高中 ';
                                } else if (e[i] === '4') {
                                    str += '中专 ';
                                } else if (e[i] === '5') {
                                    str += '大专 ';
                                } else if (e[i] === '6') {
                                    str += '本科 ';
                                } else if (e[i] === '7') {
                                    str += '硕士 ';
                                } else if (e[i] === '8') {
                                    str += '博士 ';
                                } else if (e[i] === '9') {
                                    str += '其他 ';
                                }
                            }
                            return str;
                        }
                    },
                    graduation: { type: 'date',
                        parse: function (e) {
                            return new Date(e);
                        }
                    },
                    firstJob: { type: 'date',
                        parse: function (e) {
                            return new Date(e);
                        }
                    },
                    mobile: { type: 'string' },
                    email: { type: 'string' },
                    homepage: { type: 'string' },
                    getUp: { type: 'date' },
                    importantMoment: { type: 'date' },
                    character: { type: 'number' },
                    color: { type: 'string' },
                    constellation: { type: 'string',
                        parse: function (e) {
                            var str = '';
                            for (var i = 0; i < e.length; i++) {
                                if (e[i] === '1') {
                                    str += '白羊座 ';
                                } else if (e[i] === '2') {
                                    str += '金牛座 ';
                                } else if (e[i] === '3') {
                                    str += '双子座 ';
                                } else if (e[i] === '4') {
                                    str += '巨蟹座 ';
                                } else if (e[i] === '5') {
                                    str += '狮子座 ';
                                } else if (e[i] === '6') {
                                    str += '处女座 ';
                                } else if (e[i] === '7') {
                                    str += '天秤座 ';
                                } else if (e[i] === '8') {
                                    str += '天蝎座 ';
                                } else if (e[i] === '9') {
                                    str += '射手座 ';
                                } else if (e[i] === '10') {
                                    str += '山羊座 ';
                                } else if (e[i] === '11') {
                                    str += '水瓶座 ';
                                } else if (e[i] === '12') {
                                    str += '双鱼座 ';
                                }
                            }
                            return str;
                        }
                    },
                    tourism: { type: 'string',
                        parse: function (e) {
                            var str = '';
                            for (var i = 0; i < e.length; i++) {
                                str += e[i].name + ' ';
                            }
                            return str;
                        }
                    },
                    evaluation: { type: 'number' },
                    summary: { type: 'string' },
                    photo: { type: 'string',
                        parse: function (e) {
                            return '<a href="javascript:showBigPic(\'' + e.url + '\');"><img class="w-25 rounded-circle" src="' + e.url + '" alt="' + e.name + e.extension + '"></a><small class="ml-2 text-muted">[' + kendo.toString(e.size / 1024, '0.00') + ' KB]</small>';
                        }
                    },
                    sign: { type: 'string',
                        parse: function (e) {
                            return e;
                        }
                    }
                }
            }
        },
        pageSize: 12
    });
    // 获取数据源生成过滤器
    $('#filter').kendoFilter({
        dataSource: dataSource,
        applyButton: true,
        mainLogic: 'and',
        fields: [
            { name: 'userName', type: 'string', label: '用户名',
                editorTemplate: function (container, options) {
                    $('<input name="' + options.field + '" data-bind="value: value" style="width: 200px;">')
                        .appendTo(container)
                        .kendoAutoComplete({
                            dataSource: dataSource,
                            dataTextField: options.field
                        });
                }
            },
            { name: 'realName', type: 'string', label: '姓名',
                editorTemplate: function (container, options) {
                    $('<input name="' + options.field + '" data-bind="value: value" style="width: 200px;">')
                        .appendTo(container)
                        .kendoAutoComplete({
                            dataSource: dataSource,
                            dataTextField: options.field
                        });
                }
            },
            { name: 'nickName', type: 'string', label: '昵称',
                editorTemplate: function (container, options) {
                    $('<input name="' + options.field + '" data-bind="value: value" style="width: 200px;">')
                        .appendTo(container)
                        .kendoAutoComplete({
                            dataSource: dataSource,
                            dataTextField: options.field
                        });
                }
            },
            { name: 'online', type: 'boolean', label: '状态',
                operators: {
                    boolean: {
                        eq: '等于',
                        neq: '不等于',
                        none: {
                            text: '全部',
                            handler: function (item) {
                                return item !== '';
                            }
                        }
                    }
                },
                editorTemplate: function (container, options) {
                    $('<input name="' + options.field + '" type="checkbox" data-bind="value: value">')
                        .appendTo(container)
                        .kendoSwitch({
                            messages: {
                                checked: '',
                                unchecked: ''
                            }
                        });
                }
            },
            { name: 'gender', type: 'string', label: '性别',
                editorTemplate: function (container, options) {
                    $('<label class="k-radio-label mr-3"><input class="k-radio" name="' + options.field + '" type="radio" value="1" data-bind="checked: value">男</label>' +
                        '<label class="k-radio-label"><input class="k-radio" name="' + options.field + '" type="radio" value="2" data-bind="checked: value">女</label>')
                        .appendTo(container);
                }
            },
            { name: 'age', type: 'number', label: '年龄',
                editorTemplate: function (container, options) {
                    $('<input name="' + options.field + '" type="number" data-bind="value: value" style="width: 200px;">')
                        .appendTo(container)
                        .kendoNumericTextBox({
                            format: 'n0',
                            decimals: 0,
                            min: 1,
                            max: 120
                        });
                }
            },
            { name: 'height', type: 'number', label: '身高',
                editorTemplate: function (container, options) {
                    $('<input name="' + options.field + '" type="number" data-bind="value: value" style="width: 200px;">')
                        .appendTo(container)
                        .kendoNumericTextBox({
                            format: '0.00 m',
                            decimals: 2,
                            step: 0.01,
                            min: 1.01,
                            max: 3.00
                        });
                }
            },
            { name: 'bloodType', type: 'string', label: '血型',
                editorTemplate: function (container, options) {
                    $('<select name="' + options.field + '" data-bind="value: value" style="width: 200px;"></select>')
                        .appendTo(container)
                        .kendoDropDownList({
                            dataSource: {
                                data: [
                                    { text: 'A 型', value: '1' },
                                    { text: 'B 型', value: '2' },
                                    { text: 'O 型', value: '3' },
                                    { text: 'AB 型', value: '4' },
                                    { text: '其他', value: '5' }
                                ]
                            },
                            optionLabel: '-= 请选择 =-',
                            dataValueField: 'value',
                            dataTextField: 'text'
                        });
                }
            },
            { name: 'birthday', type: 'date', label: '生日',
                editorTemplate: function (container, options) {
                    $('<input name="' + options.field + '" type="date" data-bind="value: value" style="width: 200px;">')
                        .appendTo(container)
                        .kendoDatePicker({
                            format: 'yyyy-MM-dd',
                            footer: '今天：#= kendo.toString(data, "yyyy年MM月dd日") #',
                            min: new Date(1920, 0, 1),
                            max: new Date()
                        });
                }
            },
            { name: 'mateBirthday', type: 'date', label: '配偶生日',
                editorTemplate: function (container, options) {
                    $('<input name="' + options.field + '" type="date" data-bind="value: value" style="width: 200px;">')
                        .appendTo(container)
                        .kendoDateInput({
                            format: 'yyyy-MM-dd',
                            min: new Date(1920, 0, 1),
                            max: new Date()
                        });
                }
            },
            { name: 'creditCard', type: 'string', label: '银行卡',
                editorTemplate: function (container, options) {
                    $('<input name="' + options.field + '" data-bind="value: value" style="width: 200px;">')
                        .appendTo(container)
                        .kendoAutoComplete({
                            dataSource: dataSource,
                            dataTextField: options.field
                        });
                }
            },
            { name: 'asset', type: 'number', label: '资产',
                editorTemplate: function (container, options) {
                    $('<input name="' + options.field + '" type="number" data-bind="value: value" style="width: 200px;">')
                        .appendTo(container)
                        .kendoNumericTextBox({
                            format: 'c',
                            decimals: 2,
                            step: 10000
                        });
                }
            },
            { name: 'nativePlace', type: 'string', label: '籍贯',
                editorTemplate: function (container, options) {
                    $('<input name="' + options.field + '" data-bind="value: value" style="width: 200px;">')
                        .appendTo(container)
                        .kendoAutoComplete({
                            dataSource: dataSource,
                            dataTextField: options.field
                        });
                }
            },
            { name: 'domicile', type: 'string', label: '居住地',
                editorTemplate: function (container, options) {
                    $('<input name="' + options.field + '" data-bind="value: value" style="width: 200px;">')
                        .appendTo(container)
                        .kendoAutoComplete({
                            dataSource: dataSource,
                            dataTextField: options.field
                        });
                }
            },
            { name: 'nation', type: 'string', label: '民族',
                editorTemplate: function (container, options) {
                    $('<input name="' + options.field + '" data-bind="value: value" style="width: 200px;">')
                        .appendTo(container)
                        .kendoAutoComplete({
                            dataSource: dataSource,
                            dataTextField: options.field
                        });
                }
            },
            { name: 'zodiac', type: 'string', label: '生肖',
                editorTemplate: function (container, options) {
                    $('<input name="' + options.field + '" data-bind="value: value" style="width: 200px;">')
                        .appendTo(container)
                        .kendoAutoComplete({
                            dataSource: dataSource,
                            dataTextField: options.field
                        });
                }
            },
            { name: 'language', type: 'string', label: '语言',
                editorTemplate: function (container, options) {
                    $('<input name="' + options.field + '" data-bind="value: value" style="width: 200px;">')
                        .appendTo(container)
                        .kendoAutoComplete({
                            dataSource: dataSource,
                            dataTextField: options.field
                        });
                }
            },
            { name: 'education', type: 'string', label: '教育程度',
                editorTemplate: function (container, options) {
                    $('<select name="' + options.field + '" data-bind="value: value" style="width: 200px;"></select>')
                        .appendTo(container)
                        .kendoDropDownList({
                            dataSource: {
                                data: [
                                    { text: '小学', value: '小学' },
                                    { text: '初中', value: '初中' },
                                    { text: '高中', value: '高中' },
                                    { text: '中专', value: '中专' },
                                    { text: '大专', value: '大专' },
                                    { text: '本科', value: '本科' },
                                    { text: '硕士', value: '硕士' },
                                    { text: '博士', value: '博士' },
                                    { text: '其他', value: '其他' }
                                ]
                            },
                            optionLabel: '-= 请选择 =-',
                            dataValueField: 'value',
                            dataTextField: 'text'
                        });
                }
            },
            { name: 'graduation', type: 'date', label: '毕业年份',
                editorTemplate: function (container, options) {
                    $('<input name="' + options.field + '" data-bind="value: value" style="width: 200px;">')
                        .appendTo(container)
                        .kendoDatePicker({
                            start: 'decade',
                            depth: 'decade',
                            format: 'yyyy',
                            footer: '今年：#= kendo.toString(data, "yyyy年") #'
                        });
                }
            },
            { name: 'firstJob', type: 'date', label: '参加工作年月',
                editorTemplate: function (container, options) {
                    $('<input name="' + options.field + '" type="month" data-bind="value: value" style="width: 200px;">')
                        .appendTo(container)
                        .kendoDatePicker({
                            start: 'year',
                            depth: 'year',
                            format: 'yyyy-MM',
                            footer: '当月：#= kendo.toString(data, "yyyy年MM月") #'
                        });
                }
            },
            { name: 'mobile', type: 'string', label: '手机',
                editorTemplate: function (container, options) {
                    $('<input name="' + options.field + '" data-bind="value: value" style="width: 200px;">')
                        .appendTo(container)
                        .kendoAutoComplete({
                            dataSource: dataSource,
                            dataTextField: options.field
                        });
                }
            },
            { name: 'email', type: 'string', label: '电子邮件',
                editorTemplate: function (container, options) {
                    $('<input name="' + options.field + '" data-bind="value: value" style="width: 200px;">')
                        .appendTo(container)
                        .kendoAutoComplete({
                            dataSource: dataSource,
                            dataTextField: options.field
                        });
                }
            },
            { name: 'homepage', type: 'string', label: '个人主页',
                editorTemplate: function (container, options) {
                    $('<input name="' + options.field + '" data-bind="value: value" style="width: 200px;">')
                        .appendTo(container)
                        .kendoAutoComplete({
                            dataSource: dataSource,
                            dataTextField: options.field
                        });
                }
            },
            { name: 'getUp', type: 'date', label: '起床时间',
                editorTemplate: function (container, options) {
                    $('<input name="' + options.field + '" type="time" data-bind="value: value" style="width: 200px;">')
                        .appendTo(container)
                        .kendoTimePicker({
                            format: 'HH:mm'
                        });
                }
            },
            { name: 'importantMoment', type: 'date', label: '最有意义的时刻',
                editorTemplate: function (container, options) {
                    $('<input name="' + options.field + '" type="datetime" data-bind="value: value" style="width: 200px;">')
                        .appendTo(container)
                        .kendoDateTimePicker({
                            format: 'yyyy-MM-dd HH:mm',
                            footer: '现在：#= kendo.toString(data, "yyyy年MM月dd日 HH:mm") #'
                        });
                }
            },
            { name: 'character', type: 'number', label: '性格',
                editorTemplate: function (container, options) {
                    $('<input name="' + options.field + '" type="range" data-bind="value: value">')
                        .appendTo(container)
                        .kendoSlider({
                            decreaseButtonTitle: '内向',
                            increaseButtonTitle: '开朗',
                            min: -10,
                            max: 10,
                            smallStep: 2,
                            largeStep: 0,
                            tooltip: {
                                template:
                                    '# if (value === 10) { #' +
                                        '超级开朗' +
                                    '# } else if (value === 8) { #' +
                                        '非常开朗' +
                                    '# } else if (value === 6) { #' +
                                        '很开朗' +
                                    '# } else if (value === 4) { #' +
                                        '比较开朗' +
                                    '# } else if (value === 2) { #' +
                                        '有点开朗' +
                                    '# } else if (value === 0) { #' +
                                        '普通' +
                                    '# } else if (value === -2) { #' +
                                        '有点内向' +
                                    '# } else if (value === -4) { #' +
                                        '比较内向' +
                                    '# } else if (value === -6) { #' +
                                        '很内向' +
                                    '# } else if (value === -8) { #' +
                                        '非常内向' +
                                    '# } else if (value === -10) { #' +
                                        '超级内向' +
                                    '# } #'
                            }
                        });
                }
            },
            { name: 'color', type: 'string', label: '颜色喜好',
                editorTemplate: function (container, options) {
                    $('<select name="' + options.field + '" data-bind="value: value" style="width: 200px;"></select>')
                        .appendTo(container)
                        .kendoDropDownList({
                            dataSource: {
                                data: [
                                    { text: '棕色', value: 'rgba(195, 155, 143, 1)' },
                                    { text: '桃色', value: 'rgba(215, 112, 173, 1)' },
                                    { text: '红色', value: 'rgba(218, 68, 83, 1)' },
                                    { text: '橙色', value: 'rgba(255, 152, 0, 1)' },
                                    { text: '黄色', value: 'rgba(246, 187, 66, 1)' },
                                    { text: '翠色', value: 'rgba(140, 193, 82, 1)' },
                                    { text: '绿色', value: 'rgba(55, 188, 155, 1)' },
                                    { text: '青色', value: 'rgba(59, 175, 218, 1)' },
                                    { text: '蓝色', value: 'rgba(74, 137, 220, 1)' },
                                    { text: '紫色', value: 'rgba(150, 122, 220, 1)' },
                                    { text: '黑色', value: 'rgba(67, 74, 84, 1)' },
                                    { text: '灰色', value: 'rgba(170, 178, 189, 1)' }
                                ]
                            },
                            optionLabel: '-= 请选择 =-',
                            dataValueField: 'value',
                            dataTextField: 'text',
                            template: '<span style="display: inline-block; width: 100%; height: 24px; background: #= value #; border: 1px solid \\#c5c5c5; border-radius: 4px; vertical-align: middle;"></span>',
                            valueTemplate: '<span style="display: inline-block; width: 100%; height: 24px; background: #= value #; border: 1px solid \\#c5c5c5; border-radius: 4px; vertical-align: middle;"></span>'
                        });
                }
            },
            { name: 'constellation', type: 'string', label: '相配的星座',
                editorTemplate: function (container, options) {
                    $('<select name="' + options.field + '" data-bind="value: value" style="width: 200px;"></select>')
                        .appendTo(container)
                        .kendoDropDownList({
                            dataSource: {
                                data: [
                                    { text: '白羊座', value: '白羊座' },
                                    { text: '金牛座', value: '金牛座' },
                                    { text: '双子座', value: '双子座' },
                                    { text: '巨蟹座', value: '巨蟹座' },
                                    { text: '狮子座', value: '狮子座' },
                                    { text: '处女座', value: '处女座' },
                                    { text: '天秤座', value: '天秤座' },
                                    { text: '天蝎座', value: '天蝎座' },
                                    { text: '射手座', value: '射手座' },
                                    { text: '山羊座', value: '山羊座' },
                                    { text: '水瓶座', value: '水瓶座' },
                                    { text: '双鱼座', value: '双鱼座' }
                                ]
                            },
                            optionLabel: '-= 请选择 =-',
                            dataValueField: 'value',
                            dataTextField: 'text'
                        });
                }
            },
            { name: 'tourism', type: 'string', label: '旅游足迹',
                editorTemplate: function (container, options) {
                    $('<input name="' + options.field + '" data-bind="value: value" style="width: 200px;">')
                        .appendTo(container)
                        .kendoAutoComplete({
                            dataSource: dataSource,
                            dataTextField: options.field
                        });
                }
            },
            { name: 'evaluation', type: 'number', label: '自我评价',
                editorTemplate: function (container, options) {
                    $('<input name="' + options.field + '" data-bind="value: value">')
                        .appendTo(container)
                        .kendoRating({
                            max: 6,
                            label: {
                                template:
                                    '# if (value === 1) { #' +
                                        '不合格' +
                                    '# } else if (value === 2) { #' +
                                        '待提升' +
                                    '# } else if (value === 3) { #' +
                                        '合格' +
                                    '# } else if (value === 4) { #' +
                                        '良好' +
                                    '# } else if (value === 5) { #' +
                                        '优秀' +
                                    '# } else if (value === 6) { #' +
                                        '完美' +
                                    '# } #'
                            }
                        }).data('kendoRating').wrapper.kendoTooltip({
                        filter: '.k-rating-item',
                        content: function (e) {
                            return e.target.data('value') + '分';
                        }
                    });
                }
            },
            { name: 'summary', type: 'string', label: '自我介绍',
                editorTemplate: function (container, options) {
                    $('<input name="' + options.field + '" data-bind="value: value" style="width: 320px;">')
                        .appendTo(container)
                        .kendoAutoComplete({
                            dataSource: dataSource,
                            dataTextField: options.field
                        });
                }
            },
            { name: 'photo', type: 'string', label: '头像',
                editorTemplate: function (container, options) {
                    $('<select name="' + options.field + '" data-bind="value: value" style="width: 200px;"></select>')
                        .appendTo(container)
                        .kendoDropDownList({
                            dataSource: {
                                data: [
                                    { text: '穆', value: 'Aries' },
                                    { text: '阿鲁迪巴', value: 'Taurus' },
                                    { text: '撒加', value: 'Gemini' },
                                    { text: '迪斯马斯克', value: 'Cancer' },
                                    { text: '艾欧里亚', value: 'Leo' },
                                    { text: '沙加', value: 'Virgo' },
                                    { text: '童虎', value: 'Libra' },
                                    { text: '米罗', value: 'Scorpion' },
                                    { text: '艾俄洛斯', value: 'Sagittarius' },
                                    { text: '修罗', value: 'Capricorn' },
                                    { text: '卡妙', value: 'Aquarius' },
                                    { text: '阿布罗狄', value: 'Picses' }
                                ]
                            },
                            optionLabel: '-= 请选择 =-',
                            dataValueField: 'value',
                            dataTextField: 'text',
                            template: '<img class="w-25 rounded-circle mr-2" src="img/temp/#: value #.png" alt="#: value #.png">#: text #',
                            valueTemplate: '<img class="w-15 border rounded-circle mr-2" src="img/temp/#: value #.png" alt="#: text #">#: text #'
                        });
                }
            },
            { name: 'sign', type: 'string', label: '签名',
                editorTemplate: function (container, options) {
                    $('<input name="' + options.field + '" data-bind="value: value" style="width: 320px;">')
                        .appendTo(container)
                        .kendoAutoComplete({
                            dataSource: dataSource,
                            dataTextField: options.field,
                            template: function (dataItem) {
                                return dataItem.sign;
                            }
                        });
                }
            }
        ],
        expressionPreview: true,
        expression: {
            logic: 'and',
            filters: [
                { field: 'userName', operator: 'contains', value: '' },
                { field: 'realName', operator: 'contains', value: '' },
                { field: 'nickName', operator: 'contains', value: '' },
                { field: 'online', operator: 'none', value: '' },
                { field: 'gender', operator: 'contains', value: '' },
                { field: 'age', operator: 'gte', value: '' },
                { field: 'height', operator: 'gte', value: '' },
                { field: 'bloodType', operator: 'contains', value: '' },
                { field: 'birthday', operator: 'gte', value: '' },
                { field: 'mateBirthday', operator: 'gt', value: '' },
                { field: 'creditCard', operator: 'contains', value: '' },
                { field: 'asset', operator: 'gte', value: '' },
                { field: 'nativePlace', operator: 'contains', value: '' },
                { field: 'domicile', operator: 'contains', value: '' },
                { field: 'nation', operator: 'contains', value: '' },
                { field: 'zodiac', operator: 'contains', value: '' },
                { field: 'language', operator: 'contains', value: '' },
                { field: 'education', operator: 'contains', value: '' },
                { field: 'graduation', operator: 'gt', value: '' },
                { field: 'firstJob', operator: 'gt', value: '' },
                { field: 'mobile', operator: 'contains', value: '' },
                { field: 'email', operator: 'contains', value: '' },
                { field: 'homepage', operator: 'contains', value: '' },
                { field: 'getUp', operator: 'gt', value: '' },
                { field: 'importantMoment', operator: 'gt', value: '' },
                { field: 'character', operator: 'neq', value: null },
                { field: 'color', operator: 'contains', value: '' },
                { field: 'constellation', operator: 'contains', value: '' },
                { field: 'tourism', operator: 'contains', value: '' },
                { field: 'evaluation', operator: 'neq', value: null },
                { field: 'summary', operator: 'contains', value: '' },
                { field: 'photo', operator: 'contains', value: '' },
                { field: 'sign', operator: 'contains', value: '' }
            ]
        },
        change: function () {
            this.applyFilter();
        }
    }).data('kendoFilter').applyFilter();
    // 获取数据源生成表格
    $('#generalFilter').kendoGrid({
        dataSource: dataSource,
        columns: [
            { field: 'userName', title: '用户名', width: '80px' },
            { field: 'realName', title: '姓名', width: '100px' },
            { field: 'nickName', title: '昵称', width: '110px' },
            { hidden: true, field: 'password', title: '密码', width: '70px',
                template: function (dataItem) {
                    return dataItem.password.replace(dataItem.password.substr(0), '******');
                }
            },
            { field: 'online', title: '状态', width: '70px',
                template:
                    '# if (online) { #' +
                        '<span class="dot-color k-notification-success"></span><span class="k-notification-success bg-transparent ml-2">在线</span>' +
                    '# } else { #' +
                        '<span class="dot-color k-notification-error"></span><span class="k-notification-error bg-transparent ml-2">离线</span>' +
                    '# } #'
            },
            { field: 'gender', title: '性别', width: '60px',
                values: [
                    { text: '男', value: '1' },
                    { text: '女', value: '2' }
                ]
            },
            { field: 'age', title: '年龄', width: '70px',
                template: '#= age # 岁'
            },
            { field: 'height', title: '身高', width: '80px',
                template: '#= kendo.toString(height, "0.00") # m'
            },
            { field: 'bloodType', title: '血型', width: '70px',
                values: [
                    { text: 'A 型', value: '1' },
                    { text: 'B 型', value: '2' },
                    { text: 'O 型', value: '3' },
                    { text: 'AB 型', value: '4' },
                    { text: '其他', value: '5' }
                ]
            },
            { field: 'birthday', title: '生日', width: '110px',
                template: '#= kendo.toString(birthday, "yyyy-MM-dd") #'
            },
            { field: 'mateBirthday', title: '配偶生日', width: '110px',
                template: '#= kendo.toString(mateBirthday, "yyyy-MM-dd") #'
            },
            { field: 'creditCard', title: '银行卡', width: '150px',
                template: function (dataItem) {
                    return dataItem.creditCard.replace(dataItem.creditCard.substr(2, 12), '** **** **** **');
                }
            },
            { field: 'asset', title: '资产', width: '140px',
                format: '{0:c}'
            },
            { field: 'nativePlace', title: '籍贯', width: '250px' },
            { field: 'domicile', title: '居住地', width: '100px' },
            { field: 'nation', title: '民族', width: '100px' },
            { field: 'zodiac', title: '生肖', width: '60px' },
            { field: 'language', title: '语言', width: '210px' },
            { field: 'education', title: '教育程度', width: '130px' },
            { field: 'graduation', title: '毕业年份', width: '90px',
                template: '#= kendo.toString(graduation, "yyyy") #'
            },
            { field: 'firstJob', title: '参加工作年月', width: '110px',
                template: '#= kendo.toString(firstJob, "yyyy-MM") #'
            },
            { field: 'mobile', title: '手机', width: '120px' },
            { field: 'email', title: '电子邮件', width: '180px' },
            { field: 'homepage', title: '个人主页', width: '190px' },
            { field: 'getUp', title: '起床时间', width: '90px',
                template: '#= kendo.toString(getUp, "HH:mm") #'
            },
            { field: 'importantMoment', title: '最有意义的时刻', width: '150px',
                template: '#= kendo.toString(importantMoment, "yyyy-MM-dd HH:mm") #'
            },
            { field: 'character', title: '性格', width: '90px',
                values: [
                    { text: '超级开朗', value: 10 },
                    { text: '非常开朗', value: 8 },
                    { text: '很开朗', value: 6 },
                    { text: '比较开朗', value: 4 },
                    { text: '有点开朗', value: 2 },
                    { text: '普通', value: 0 },
                    { text: '有点内向', value: -2 },
                    { text: '比较内向', value: -4 },
                    { text: '很内向', value: -6 },
                    { text: '非常内向', value: -8 },
                    { text: '超级内向', value: -10 }
                ]
            },
            { field: 'color', title: '颜色喜好', width: '90px',
                template: '<span style="display: inline-block; width: 100%; height: 24px; background: #= color #; border: 1px solid \\#c5c5c5; border-radius: 4px; vertical-align: middle;"></span>'
            },
            { field: 'constellation', title: '相配的星座', width: '170px' },
            { field: 'tourism', title: '旅游足迹', width: '200px' },
            { field: 'evaluation', title: '自我评价', width: '90px',
                values: [
                    { text: '不合格', value: 1 },
                    { text: '待提升', value: 2 },
                    { text: '合格', value: 3 },
                    { text: '良好', value: 4 },
                    { text: '优秀', value: 5 },
                    { text: '完美', value: 6 }
                ]
            },
            { field: 'summary', title: '自我介绍', width: '310px' },
            { field: 'photo', title: '头像', width: '120px',
                template: '#= photo #'
            },
            { field: 'sign', title: '签名', width: '310px',
                template: '#= sign #'
            }
        ],
        reorderable: true,
        resizable: true
    });
    // 保存过滤条件
    $('#saveFilter').click(function (e) {
        e.preventDefault();
        sessionStorage.setItem('filter', kendo.stringify($('#filter').data('kendoFilter').getOptions()));
        alertMsgNoBtn('保存成功~', 'success');
    });
    // 加载过滤条件
    $('#loadFilter').click(function (e) {
        e.preventDefault();
        var options = sessionStorage.getItem('filter');
        if (options) {
            options = JSON.parse(options);
            options.dataSource = dataSource;
            $('#filter').data('kendoFilter').setOptions(options);
            $('#filter').data('kendoFilter').applyFilter();
        }
    });
});