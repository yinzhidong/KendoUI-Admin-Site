$(function () {
    // 自动完成框本地
    $('#autoCompleteLocal').kendoAutoComplete({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/select_data.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data'
            }
        },
        placeholder: '-= 请输入省市区关键字 =-',
        dataTextField: 'name',
        filter: 'contains'
    });
    // 自动完成框远程
    $('#autoCompleteRemote').kendoAutoComplete({
        placeholder: '-= 请输入省市区关键字 =-',
        dataTextField: 'name',
        filter: 'contains',
        delay: 1000,
        minLength: 1,
        enforceMinLength: true,
        filtering: function (e) {
            var that = this;
            if (!e.filter.value) {
                e.preventDefault();
            } else {
                $.fn.ajaxPost({
                    ajaxData: { keywords: e.filter.value},
                    ajaxUrl: 'json/select_data.json',
                    succeed: function (res) {
                        that.setDataSource(res.data);
                        that.dataSource.read();
                    }
                });
            }
        }
    });
    // 单选下拉框本地
    $('#dropDownListLocal').kendoDropDownList({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/select_data.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data'
            }
        },
        optionLabel: '-= 请输入省市区关键字 =-',
        dataValueField: 'code',
        dataTextField: 'name',
        filter: 'contains'
    });
    // 单选下拉框远程
    $('#dropDownListRemote').kendoDropDownList({
        optionLabel: '-= 请输入省市区关键字 =-',
        dataValueField: 'code',
        dataTextField: 'name',
        filter: 'contains',
        delay: 1000,
        minLength: 1,
        enforceMinLength: true,
        filtering: function (e) {
            var that = this;
            if (!e.filter.value) {
                e.preventDefault();
            } else {
                $.fn.ajaxPost({
                    ajaxData: { keywords: e.filter.value},
                    ajaxUrl: 'json/select_data.json',
                    succeed: function (res) {
                        that.setDataSource(res.data);
                        that.dataSource.read();
                    }
                });
            }
        }
    });
    // 输入下拉框本地
    $('#comboBoxLocal').kendoComboBox({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/select_data.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data'
            }
        },
        placeholder: '-= 请输入省市区关键字 =-',
        dataValueField: 'code',
        dataTextField: 'name',
        filter: 'contains'
    });
    // 输入下拉框远程
    $('#comboBoxRemote').kendoComboBox({
        placeholder: '-= 请输入省市区关键字 =-',
        dataValueField: 'code',
        dataTextField: 'name',
        filter: 'contains',
        delay: 1000,
        minLength: 1,
        enforceMinLength: true,
        filtering: function (e) {
            var that = this;
            if (!e.filter.value) {
                e.preventDefault();
            } else {
                $.fn.ajaxPost({
                    ajaxData: { keywords: e.filter.value},
                    ajaxUrl: 'json/select_data.json',
                    succeed: function (res) {
                        that.setDataSource(res.data);
                        that.dataSource.read();
                    }
                });
            }
        }
    });
    // 表格下拉框本地
    $('#multiColumnComboBoxLocal').kendoMultiColumnComboBox({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/select_data.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data'
            }
        },
        placeholder: '-= 请输入省市区关键字 =-',
        dataValueField: 'code',
        dataTextField: 'name',
        columns: [
            { field: 'code', title: '编码' },
            { field: 'name', title: '名称' }
        ],
        filter: 'contains',
        filterFields: ['code', 'name']
    });
    // 表格下拉框远程
    $('#multiColumnComboBoxRemote').kendoMultiColumnComboBox({
        placeholder: '-= 请输入省市区关键字 =-',
        dataValueField: 'code',
        dataTextField: 'name',
        columns: [
            { field: 'code', title: '编码' },
            { field: 'name', title: '名称' }
        ],
        filter: 'contains',
        filterFields: ['code', 'name'],
        delay: 1000,
        minLength: 1,
        enforceMinLength: true,
        filtering: function (e) {
            var that = this;
            if (!e.filter.filters[0].value) {
                e.preventDefault();
            } else {
                $.fn.ajaxPost({
                    ajaxData: { keywords: e.filter.filters[0].value},
                    ajaxUrl: 'json/select_data.json',
                    succeed: function (res) {
                        that.setDataSource(res.data);
                        that.dataSource.read();
                    }
                });
            }
        }
    });
    // 多选下拉框本地
    $('#multiSelectLocal').kendoMultiSelect({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/select_data.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data'
            }
        },
        placeholder: '-= 请输入省市区关键字 =-',
        dataValueField: 'code',
        dataTextField: 'name',
        filter: 'contains'
    });
    // 多选下拉框远程
    $('#multiSelectRemote').kendoMultiSelect({
        placeholder: '-= 请输入省市区关键字 =-',
        dataValueField: 'code',
        dataTextField: 'name',
        filter: 'contains',
        delay: 1000,
        minLength: 1,
        enforceMinLength: true,
        filtering: function (e) {
            var that = this;
            if (!e.filter.value) {
                e.preventDefault();
            } else {
                $.fn.ajaxPost({
                    ajaxData: { keywords: e.filter.value},
                    ajaxUrl: 'json/select_data.json',
                    succeed: function (res) {
                        that.setDataSource(res.data);
                        that.dataSource.read();
                    }
                });
            }
        }
    });
    // 树形单选下拉框本地
    $('#dropDownTreeLocal').kendoDropDownTree({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/select_hierarchical_data.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data',
                model: {
                    children: 'items'
                }
            }
        },
        placeholder: '-= 请输入省市区关键字 =-',
        dataValueField: 'code',
        dataTextField: 'name',
        filter: 'contains'
    });
    // 树形单选下拉框远程
    $('#dropDownTreeRemote').kendoDropDownTree({
        placeholder: '-= 请输入省市区关键字 =-',
        dataValueField: 'code',
        dataTextField: 'name',
        filter: 'contains',
        delay: 1000,
        minLength: 1,
        enforceMinLength: true,
        filtering: function (e) {
            var that = this;
            if (!e.filter.value) {
                e.preventDefault();
            } else {
                $.fn.ajaxPost({
                    ajaxData: { keywords: e.filter.value},
                    ajaxUrl: 'json/select_hierarchical_data.json',
                    succeed: function (res) {
                        that.setDataSource(res.data);
                    }
                });
            }
        }
    });
    // 树形多选下拉框本地
    $('#multiDropDownTreeLocal').kendoDropDownTree({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/select_hierarchical_data.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data',
                model: {
                    children: 'items'
                }
            }
        },
        placeholder: '-= 请输入省市区关键字 =-',
        dataValueField: 'code',
        dataTextField: 'name',
        checkboxes: {
            checkChildren: true
        },
        autoClose: false,
        filter: 'contains'
    });
    // 树形多选下拉框远程
    $('#multiDropDownTreeRemote').kendoDropDownTree({
        placeholder: '-= 请输入省市区关键字 =-',
        dataValueField: 'code',
        dataTextField: 'name',
        checkboxes: {
            checkChildren: true
        },
        autoClose: false,
        filter: 'contains',
        delay: 1000,
        minLength: 1,
        enforceMinLength: true,
        filtering: function (e) {
            var that = this;
            if (!e.filter.value) {
                e.preventDefault();
            } else {
                $.fn.ajaxPost({
                    ajaxData: { keywords: e.filter.value},
                    ajaxUrl: 'json/select_hierarchical_data.json',
                    succeed: function (res) {
                        that.setDataSource(res.data);
                    }
                });
            }
        }
    });
});