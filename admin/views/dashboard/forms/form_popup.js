$(function () {
    // 表格对话框单选
    $('#gridDialogSingle').kendoDropDownList({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/grid.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data'
            }
        },
        optionLabel: '-= 请点击右侧按钮选择 =-',
        dataValueField: 'id',
        dataTextField: 'realName'
    });
    $('#gridDialogSingleBtn').kendoButton({
        iconClass: 'gridIcon',
        click: function () {
            var confirmDialog = $('<div class="p-0"></div>').kendoDialog({
                animation: {open: {effects: 'fade:in'}, close: {effects: 'fade:out'}},
                maxWidth: '60%',
                maxHeight: '60%',
                minWidth: 375,
                minHeight: 585,
                title: '表格对话框单选',
                content: '<div class="border-0" id="grid"></div>',
                actions: [
                    {
                        text: '确定',
                        primary: true,
                        action: function () {
                            selectRender('grid', 'gridDialogSingle', 'kendoDropDownList');
                        }
                    },
                    {
                        text: '重置',
                        action: function () {
                            $('#grid').data('kendoGrid').clearSelection();
                            renderSelect('grid', 'gridDialogSingle', 'kendoDropDownList');
                            return false;
                        }
                    },
                    {
                        text: '取消',
                        action: function () {
                            confirmDialog.close();
                        }
                    }
                ],
                initOpen: function () {
                    var gridDataSource = getGridDataSource(),
                        gridColumns = getGridColumns();
                    gridColumns.unshift({ field: 'id', width: '45px',
                        headerTemplate: '&nbsp;',
                        template:
                            '<label class="k-radio-label">' +
                                '<input class="k-radio ids" type="radio" value="#= id #" onclick="selectSingle(\'grid\', \'kendoGrid\', this);">' +
                            '</label>'
                    });
                    $('#grid').kendoGrid({
                        dataSource: gridDataSource,
                        toolbar: [{
                            template:
                                '<span class="k-searchbox w-100">' +
                                    '<span class="k-input-prefix">' +
                                        '<span class="k-icon k-i-search"></span>' +
                                    '</span>' +
                                    '<input class="k-input" name="keywords" type="text" placeholder="关键字搜索" onkeyup="keywordsSearch(\'grid\', \'kendoGrid\', this);">' +
                                '</span>'
                        }],
                        columns: gridColumns,
                        height: 496,
                        scrollable: {
                            endless: true
                        },
                        reorderable: true,
                        resizable: true,
                        selectable: 'row',
                        change: function () {
                            $('#grid .k-grid-content .ids').prop('checked', false);
                            this.select().find('.ids').prop('checked', true);
                        },
                        dataBound: function () {
                            renderSelect('grid', 'gridDialogSingle', 'kendoDropDownList');
                        }
                    });
                },
                close: function () {
                    confirmDialog.destroy();
                }
            }).data('kendoDialog');
            confirmDialog.open();
        }
    });
    // 表格对话框多选
    $('#gridDialogMultiple').kendoMultiSelect({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/grid.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data'
            }
        },
        placeholder: '-= 请点击右侧按钮选择 =-',
        dataValueField: 'id',
        dataTextField: 'realName'
    });
    $('#gridDialogMultipleBtn').kendoButton({
        iconClass: 'gridIcon',
        click: function () {
            var confirmDialog = $('<div class="p-0"></div>').kendoDialog({
                animation: {open: {effects: 'fade:in'}, close: {effects: 'fade:out'}},
                maxWidth: '60%',
                maxHeight: '60%',
                minWidth: 375,
                minHeight: 585,
                title: '表格对话框多选',
                content: '<div class="border-0" id="grid"></div>',
                actions: [
                    {
                        text: '确定',
                        primary: true,
                        action: function () {
                            selectRender('grid', 'gridDialogMultiple', 'kendoMultiSelect');
                        }
                    },
                    {
                        text: '重置',
                        action: function () {
                            $('#grid').data('kendoGrid').clearSelection();
                            renderSelect('grid', 'gridDialogMultiple', 'kendoMultiSelect');
                            return false;
                        }
                    },
                    {
                        text: '取消',
                        action: function () {
                            confirmDialog.close();
                        }
                    }
                ],
                initOpen: function () {
                    var gridDataSource = getGridDataSource(),
                        gridColumns = getGridColumns();
                    gridColumns.unshift({ field: 'id', width: '45px',
                        headerTemplate:
                            '<label class="k-checkbox-label" title="全选">' +
                                '<input class="k-checkbox" id="selectAll" type="checkbox" onclick="selectAll(\'grid\', \'kendoGrid\', this);">' +
                            '</label>',
                        template:
                            '<label class="k-checkbox-label">' +
                                '<input class="k-checkbox ids" type="checkbox" value="#= id #" onclick="selectSingle(\'grid\', \'kendoGrid\', this);">' +
                            '</label>'
                    });
                    $('#grid').kendoGrid({
                        dataSource: gridDataSource,
                        toolbar: [{
                            template:
                                '<span class="k-searchbox w-100">' +
                                    '<span class="k-input-prefix">' +
                                        '<span class="k-icon k-i-search"></span>' +
                                    '</span>' +
                                    '<input class="k-input" name="keywords" type="text" placeholder="关键字搜索" onkeyup="keywordsSearch(\'grid\', \'kendoGrid\', this);">' +
                                '</span>'
                        }],
                        columns: gridColumns,
                        height: 496,
                        scrollable: {
                            endless: true
                        },
                        reorderable: true,
                        resizable: true,
                        selectable: 'multiple, row',
                        change: function () {
                            $('#grid .k-grid-content .ids').prop('checked', false);
                            this.select().find('.ids').prop('checked', true);
                            selectHalf('grid');
                        },
                        dataBound: function () {
                            selectHalf('grid');
                            renderSelect('grid', 'gridDialogMultiple', 'kendoMultiSelect');
                        }
                    });
                },
                close: function () {
                    confirmDialog.destroy();
                }
            }).data('kendoDialog');
            confirmDialog.open();
        }
    });
    // 表格模态框单选
    $('#gridWindowSingle').kendoDropDownList({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/grid.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data'
            }
        },
        optionLabel: '-= 请点击右侧按钮选择 =-',
        dataValueField: 'id',
        dataTextField: 'realName'
    });
    $('#gridWindowSingleBtn').kendoButton({
        iconClass: 'gridIcon',
        click: function () {
            var confirmWindow = $('<div></div>').kendoWindow({
                animation: {open: {effects: 'fade:in'}, close: {effects: 'fade:out'}},
                maxWidth: '60%',
                maxHeight: '60%',
                minWidth: 375,
                minHeight: 628,
                width: '60%',
                title: {
                    text: '<i class="mr-2 theme-m gridIcon"></i>表格模态框单选',
                    encoded: false
                },
                modal: true,
                pinned: true,
                resizable: false,
                open: function () {
                    var gridDataSource = getGridDataSource(),
                        gridColumns = getGridColumns();
                    gridColumns.unshift({ field: 'id', width: '45px',
                        headerTemplate: '&nbsp;',
                        template:
                            '<label class="k-radio-label">' +
                                '<input class="k-radio ids" type="radio" value="#= id #" onclick="selectSingle(\'grid\', \'kendoGrid\', this);">' +
                            '</label>'
                    });
                    $('#grid').kendoGrid({
                        dataSource: gridDataSource,
                        toolbar: [{
                            template:
                                '<span class="k-searchbox w-100">' +
                                    '<span class="k-input-prefix">' +
                                        '<span class="k-icon k-i-search"></span>' +
                                    '</span>' +
                                    '<input class="k-input" name="keywords" type="text" placeholder="关键字搜索" onkeyup="keywordsSearch(\'grid\', \'kendoGrid\', this);">' +
                                '</span>'
                        }],
                        columns: gridColumns,
                        height: 500,
                        scrollable: {
                            endless: true
                        },
                        reorderable: true,
                        resizable: true,
                        selectable: 'row',
                        change: function () {
                            $('#grid .k-grid-content .ids').prop('checked', false);
                            this.select().find('.ids').prop('checked', true);
                        },
                        dataBound: function () {
                            renderSelect('grid', 'gridWindowSingle', 'kendoDropDownList');
                        }
                    });
                },
                close: function () {
                    confirmWindow.destroy();
                }
            }).data('kendoWindow');
            confirmWindow.content(
                '<div id="grid"></div>' +
                '<div class="k-window-buttongroup pt-3">' +
                    '<button class="k-button k-button-lg k-primary confirm" type="button">确 定</button>' +
                    '<button class="k-button k-button-lg theme-m-box reset" type="button">重 置</button>' +
                    '<button class="k-button k-button-lg cancel" type="button">取 消</button>' +
                '</div>'
            ).center().open();
            confirmWindow.wrapper.find('.k-window-buttongroup .confirm').click(function () {
                selectRender('grid', 'gridWindowSingle', 'kendoDropDownList');
                confirmWindow.close();
            });
            confirmWindow.wrapper.find('.k-window-buttongroup .reset').click(function () {
                $('#grid').data('kendoGrid').clearSelection();
                renderSelect('grid', 'gridWindowSingle', 'kendoDropDownList');
            });
            confirmWindow.wrapper.find('.k-window-buttongroup .cancel').click(function () {
                confirmWindow.close();
            });
        }
    });
    // 表格模态框多选
    $('#gridWindowMultiple').kendoMultiSelect({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/grid.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data'
            }
        },
        placeholder: '-= 请点击右侧按钮选择 =-',
        dataValueField: 'id',
        dataTextField: 'realName'
    });
    $('#gridWindowMultipleBtn').kendoButton({
        iconClass: 'gridIcon',
        click: function () {
            var confirmWindow = $('<div></div>').kendoWindow({
                animation: {open: {effects: 'fade:in'}, close: {effects: 'fade:out'}},
                maxWidth: '60%',
                maxHeight: '60%',
                minWidth: 375,
                minHeight: 628,
                width: '60%',
                title: {
                    text: '<i class="mr-2 theme-m gridIcon"></i>表格模态框多选',
                    encoded: false
                },
                modal: true,
                pinned: true,
                resizable: false,
                open: function () {
                    var gridDataSource = getGridDataSource(),
                        gridColumns = getGridColumns();
                    gridColumns.unshift({ field: 'id', width: '45px',
                        headerTemplate:
                            '<label class="k-checkbox-label" title="全选">' +
                                '<input class="k-checkbox" id="selectAll" type="checkbox" onclick="selectAll(\'grid\', \'kendoGrid\', this);">' +
                            '</label>',
                        template:
                            '<label class="k-checkbox-label">' +
                                '<input class="k-checkbox ids" type="checkbox" value="#= id #" onclick="selectSingle(\'grid\', \'kendoGrid\', this);">' +
                            '</label>'
                    });
                    $('#grid').kendoGrid({
                        dataSource: gridDataSource,
                        toolbar: [{
                            template:
                                '<span class="k-searchbox w-100">' +
                                    '<span class="k-input-prefix">' +
                                        '<span class="k-icon k-i-search"></span>' +
                                    '</span>' +
                                    '<input class="k-input" name="keywords" type="text" placeholder="关键字搜索" onkeyup="keywordsSearch(\'grid\', \'kendoGrid\', this);">' +
                                '</span>'
                        }],
                        columns: gridColumns,
                        height: 500,
                        scrollable: {
                            endless: true
                        },
                        reorderable: true,
                        resizable: true,
                        selectable: 'multiple, row',
                        change: function () {
                            $('#grid .k-grid-content .ids').prop('checked', false);
                            this.select().find('.ids').prop('checked', true);
                            selectHalf('grid');
                        },
                        dataBound: function () {
                            selectHalf('grid');
                            renderSelect('grid', 'gridWindowMultiple', 'kendoMultiSelect');
                        }
                    });
                },
                close: function () {
                    confirmWindow.destroy();
                }
            }).data('kendoWindow');
            confirmWindow.content(
                '<div id="grid"></div>' +
                '<div class="k-window-buttongroup pt-3">' +
                    '<button class="k-button k-button-lg k-primary confirm" type="button">确 定</button>' +
                    '<button class="k-button k-button-lg theme-m-box reset" type="button">重 置</button>' +
                    '<button class="k-button k-button-lg cancel" type="button">取 消</button>' +
                '</div>'
            ).center().open();
            confirmWindow.wrapper.find('.k-window-buttongroup .confirm').click(function () {
                selectRender('grid', 'gridWindowMultiple', 'kendoMultiSelect');
                confirmWindow.close();
            });
            confirmWindow.wrapper.find('.k-window-buttongroup .reset').click(function () {
                $('#grid').data('kendoGrid').clearSelection();
                renderSelect('grid', 'gridWindowMultiple', 'kendoMultiSelect');
            });
            confirmWindow.wrapper.find('.k-window-buttongroup .cancel').click(function () {
                confirmWindow.close();
            });
        }
    });
    // 树形列表对话框单选
    $('#treeListDialogSingle').kendoDropDownList({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/treelist.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data'
            }
        },
        optionLabel: '-= 请点击右侧按钮选择 =-',
        dataValueField: 'id',
        dataTextField: 'realName'
    });
    $('#treeListDialogSingleBtn').kendoButton({
        iconClass: 'treeListIcon',
        click: function () {
            var confirmDialog = $('<div class="p-0"></div>').kendoDialog({
                animation: {open: {effects: 'fade:in'}, close: {effects: 'fade:out'}},
                maxWidth: '60%',
                maxHeight: '60%',
                minWidth: 375,
                minHeight: 635,
                title: '树形列表对话框单选',
                content: '<div class="border-0" id="treeList"></div>',
                actions: [
                    {
                        text: '确定',
                        primary: true,
                        action: function () {
                            selectRender('treeList', 'treeListDialogSingle', 'kendoDropDownList');
                        }
                    },
                    {
                        text: '重置',
                        action: function () {
                            $('#treeList').data('kendoTreeList').clearSelection();
                            renderSelect('treeList', 'treeListDialogSingle', 'kendoDropDownList');
                            return false;
                        }
                    },
                    {
                        text: '取消',
                        action: function () {
                            confirmDialog.close();
                        }
                    }
                ],
                initOpen: function () {
                    var treeListDataSource = getTreeListDataSource(),
                        treeListColumns = getTreeListColumns();
                    treeListColumns.unshift({ field: 'id', width: '45px',
                        headerTemplate: '&nbsp;',
                        template:
                            '<label class="k-radio-label">' +
                                '<input class="k-radio ids" type="radio" value="#= id #" onclick="selectSingle(\'treeList\', \'kendoTreeList\', this);">' +
                            '</label>'
                    });
                    $('#treeList').kendoTreeList({
                        dataSource: treeListDataSource,
                        toolbar: kendo.template(
                            '<span class="k-searchbox w-100">' +
                                '<span class="k-input-prefix">' +
                                    '<span class="k-icon k-i-search"></span>' +
                                '</span>' +
                                '<input class="k-input" name="keywords" type="text" placeholder="关键字搜索" onkeyup="keywordsSearch(\'treeList\', \'kendoTreeList\', this);">' +
                            '</span>'
                        ),
                        columns: treeListColumns,
                        pageable: {
                            buttonCount: 5,
                            input: true,
                            pageSizes: [5, 10, 15, 20, 25, 30, 50, 100, 'all'],
                            refresh: true
                        },
                        height: 546,
                        reorderable: true,
                        resizable: true,
                        selectable: 'row',
                        change: function () {
                            $('#treeList .k-grid-content .ids').prop('checked', false);
                            this.select().find('.ids').prop('checked', true);
                        },
                        dataBound: function () {
                            renderSelect('treeList', 'treeListDialogSingle', 'kendoDropDownList');
                        }
                    });
                },
                close: function () {
                    confirmDialog.destroy();
                }
            }).data('kendoDialog');
            confirmDialog.open();
        }
    });
    // 树形列表对话框多选
    $('#treeListDialogMultiple').kendoMultiSelect({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/treelist.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data'
            }
        },
        placeholder: '-= 请点击右侧按钮选择 =-',
        dataValueField: 'id',
        dataTextField: 'realName'
    });
    $('#treeListDialogMultipleBtn').kendoButton({
        iconClass: 'treeListIcon',
        click: function () {
            var confirmDialog = $('<div class="p-0"></div>').kendoDialog({
                animation: {open: {effects: 'fade:in'}, close: {effects: 'fade:out'}},
                maxWidth: '60%',
                maxHeight: '60%',
                minWidth: 375,
                minHeight: 635,
                title: '树形列表对话框多选',
                content: '<div class="border-0" id="treeList"></div>',
                actions: [
                    {
                        text: '确定',
                        primary: true,
                        action: function () {
                            selectRender('treeList', 'treeListDialogMultiple', 'kendoMultiSelect');
                        }
                    },
                    {
                        text: '重置',
                        action: function () {
                            $('#treeList').data('kendoTreeList').clearSelection();
                            renderSelect('treeList', 'treeListDialogMultiple', 'kendoMultiSelect');
                            return false;
                        }
                    },
                    {
                        text: '取消',
                        action: function () {
                            confirmDialog.close();
                        }
                    }
                ],
                initOpen: function () {
                    var treeListDataSource = getTreeListDataSource(),
                        treeListColumns = getTreeListColumns();
                    treeListColumns.unshift({ field: 'id', width: '45px',
                        headerTemplate:
                            '<label class="k-checkbox-label" title="全选">' +
                                '<input class="k-checkbox" id="selectAll" type="checkbox" onclick="selectAll(\'treeList\', \'kendoTreeList\', this);">' +
                            '</label>',
                        template:
                            '<label class="k-checkbox-label">' +
                                '<input class="k-checkbox ids" type="checkbox" value="#= id #" onclick="selectSingle(\'treeList\', \'kendoTreeList\', this);">' +
                            '</label>'
                    });
                    $('#treeList').kendoTreeList({
                        dataSource: treeListDataSource,
                        toolbar: kendo.template(
                            '<span class="k-searchbox w-100">' +
                                '<span class="k-input-prefix">' +
                                    '<span class="k-icon k-i-search"></span>' +
                                '</span>' +
                                '<input class="k-input" name="keywords" type="text" placeholder="关键字搜索" onkeyup="keywordsSearch(\'treeList\', \'kendoTreeList\', this);">' +
                            '</span>'
                        ),
                        columns: treeListColumns,
                        pageable: {
                            buttonCount: 5,
                            input: true,
                            pageSizes: [5, 10, 15, 20, 25, 30, 50, 100, 'all'],
                            refresh: true
                        },
                        height: 546,
                        reorderable: true,
                        resizable: true,
                        selectable: 'multiple, row',
                        change: function () {
                            $('#treeList .k-grid-content .ids').prop('checked', false);
                            this.select().find('.ids').prop('checked', true);
                            selectHalf('treeList');
                        },
                        dataBound: function () {
                            selectHalf('treeList');
                            renderSelect('treeList', 'treeListDialogMultiple', 'kendoMultiSelect');
                        }
                    });
                },
                close: function () {
                    confirmDialog.destroy();
                }
            }).data('kendoDialog');
            confirmDialog.open();
        }
    });
    // 树形列表模态框单选
    $('#treeListWindowSingle').kendoDropDownList({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/treelist.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data'
            }
        },
        optionLabel: '-= 请点击右侧按钮选择 =-',
        dataValueField: 'id',
        dataTextField: 'realName'
    });
    $('#treeListWindowSingleBtn').kendoButton({
        iconClass: 'treeListIcon',
        click: function () {
            var confirmWindow = $('<div></div>').kendoWindow({
                animation: {open: {effects: 'fade:in'}, close: {effects: 'fade:out'}},
                maxWidth: '60%',
                maxHeight: '60%',
                minWidth: 375,
                minHeight: 678,
                width: '60%',
                title: {
                    text: '<i class="mr-2 theme-m treeListIcon"></i>树形列表模态框单选',
                    encoded: false
                },
                modal: true,
                pinned: true,
                resizable: false,
                open: function () {
                    var treeListDataSource = getTreeListDataSource(),
                        treeListColumns = getTreeListColumns();
                    treeListColumns.unshift({ field: 'id', width: '45px',
                        headerTemplate: '&nbsp;',
                        template:
                            '<label class="k-radio-label">' +
                                '<input class="k-radio ids" type="radio" value="#= id #" onclick="selectSingle(\'treeList\', \'kendoTreeList\', this);">' +
                            '</label>'
                    });
                    $('#treeList').kendoTreeList({
                        dataSource: treeListDataSource,
                        toolbar: kendo.template(
                            '<span class="k-searchbox w-100">' +
                                '<span class="k-input-prefix">' +
                                    '<span class="k-icon k-i-search"></span>' +
                                '</span>' +
                                '<input class="k-input" name="keywords" type="text" placeholder="关键字搜索" onkeyup="keywordsSearch(\'treeList\', \'kendoTreeList\', this);">' +
                            '</span>'
                        ),
                        columns: treeListColumns,
                        pageable: {
                            buttonCount: 5,
                            input: true,
                            pageSizes: [5, 10, 15, 20, 25, 30, 50, 100, 'all'],
                            refresh: true
                        },
                        height: 550,
                        reorderable: true,
                        resizable: true,
                        selectable: 'row',
                        change: function () {
                            $('#treeList .k-grid-content .ids').prop('checked', false);
                            this.select().find('.ids').prop('checked', true);
                        },
                        dataBound: function () {
                            renderSelect('treeList', 'treeListWindowSingle', 'kendoDropDownList');
                        }
                    });
                },
                close: function () {
                    confirmWindow.destroy();
                }
            }).data('kendoWindow');
            confirmWindow.content(
                '<div id="treeList"></div>' +
                '<div class="k-window-buttongroup pt-3">' +
                    '<button class="k-button k-button-lg k-primary confirm" type="button">确 定</button>' +
                    '<button class="k-button k-button-lg theme-m-box reset" type="button">重 置</button>' +
                    '<button class="k-button k-button-lg cancel" type="button">取 消</button>' +
                '</div>'
            ).center().open();
            confirmWindow.wrapper.find('.k-window-buttongroup .confirm').click(function () {
                selectRender('treeList', 'treeListWindowSingle', 'kendoDropDownList');
                confirmWindow.close();
            });
            confirmWindow.wrapper.find('.k-window-buttongroup .reset').click(function () {
                $('#treeList').data('kendoTreeList').clearSelection();
                renderSelect('treeList', 'treeListWindowSingle', 'kendoDropDownList');
            });
            confirmWindow.wrapper.find('.k-window-buttongroup .cancel').click(function () {
                confirmWindow.close();
            });
        }
    });
    // 树形列表模态框多选
    $('#treeListWindowMultiple').kendoMultiSelect({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/treelist.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data'
            }
        },
        placeholder: '-= 请点击右侧按钮选择 =-',
        dataValueField: 'id',
        dataTextField: 'realName'
    });
    $('#treeListWindowMultipleBtn').kendoButton({
        iconClass: 'treeListIcon',
        click: function () {
            var confirmWindow = $('<div></div>').kendoWindow({
                animation: {open: {effects: 'fade:in'}, close: {effects: 'fade:out'}},
                maxWidth: '60%',
                maxHeight: '60%',
                minWidth: 375,
                minHeight: 678,
                width: '60%',
                title: {
                    text: '<i class="mr-2 theme-m treeListIcon"></i>树形列表模态框多选',
                    encoded: false
                },
                modal: true,
                pinned: true,
                resizable: false,
                open: function () {
                    var treeListDataSource = getTreeListDataSource(),
                        treeListColumns = getTreeListColumns();
                    treeListColumns.unshift({ field: 'id', width: '45px',
                        headerTemplate:
                            '<label class="k-checkbox-label" title="全选">' +
                                '<input class="k-checkbox" id="selectAll" type="checkbox" onclick="selectAll(\'treeList\', \'kendoTreeList\', this);">' +
                            '</label>',
                        template:
                            '<label class="k-checkbox-label">' +
                                '<input class="k-checkbox ids" type="checkbox" value="#= id #" onclick="selectSingle(\'treeList\', \'kendoTreeList\', this);">' +
                            '</label>'
                    });
                    $('#treeList').kendoTreeList({
                        dataSource: treeListDataSource,
                        toolbar: kendo.template(
                            '<span class="k-searchbox w-100">' +
                                '<span class="k-input-prefix">' +
                                    '<span class="k-icon k-i-search"></span>' +
                                '</span>' +
                                '<input class="k-input" name="keywords" type="text" placeholder="关键字搜索" onkeyup="keywordsSearch(\'treeList\', \'kendoTreeList\', this);">' +
                            '</span>'
                        ),
                        columns: treeListColumns,
                        pageable: {
                            buttonCount: 5,
                            input: true,
                            pageSizes: [5, 10, 15, 20, 25, 30, 50, 100, 'all'],
                            refresh: true
                        },
                        height: 550,
                        reorderable: true,
                        resizable: true,
                        selectable: 'multiple, row',
                        change: function () {
                            $('#treeList .k-grid-content .ids').prop('checked', false);
                            this.select().find('.ids').prop('checked', true);
                            selectHalf('treeList');
                        },
                        dataBound: function () {
                            selectHalf('treeList');
                            renderSelect('treeList', 'treeListWindowMultiple', 'kendoMultiSelect');
                        }
                    });
                },
                close: function () {
                    confirmWindow.destroy();
                }
            }).data('kendoWindow');
            confirmWindow.content(
                '<div id="treeList"></div>' +
                '<div class="k-window-buttongroup pt-3">' +
                    '<button class="k-button k-button-lg k-primary confirm" type="button">确 定</button>' +
                    '<button class="k-button k-button-lg theme-m-box reset" type="button">重 置</button>' +
                    '<button class="k-button k-button-lg cancel" type="button">取 消</button>' +
                '</div>'
            ).center().open();
            confirmWindow.wrapper.find('.k-window-buttongroup .confirm').click(function () {
                selectRender('treeList', 'treeListWindowMultiple', 'kendoMultiSelect');
                confirmWindow.close();
            });
            confirmWindow.wrapper.find('.k-window-buttongroup .reset').click(function () {
                $('#treeList').data('kendoTreeList').clearSelection();
                renderSelect('treeList', 'treeListWindowMultiple', 'kendoMultiSelect');
            });
            confirmWindow.wrapper.find('.k-window-buttongroup .cancel').click(function () {
                confirmWindow.close();
            });
        }
    });
    // 列表视图对话框单选
    $('#listDialogSingle').kendoDropDownList({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/list.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data'
            }
        },
        optionLabel: '-= 请点击右侧按钮选择 =-',
        dataValueField: 'id',
        dataTextField: 'realName'
    });
    $('#listDialogSingleBtn').kendoButton({
        iconClass: 'listViewIcon',
        click: function () {
            var confirmDialog = $('<div class="p-0"></div>').kendoDialog({
                animation: {open: {effects: 'fade:in'}, close: {effects: 'fade:out'}},
                maxWidth: '40%',
                maxHeight: '60%',
                minWidth: 375,
                minHeight: 575,
                width: '40%',
                title: '列表视图对话框单选',
                content:
                    '<div class="k-toolbar border-top-0 border-left-0 border-right-0">' +
                        '<span class="k-searchbox flex-fill">' +
                            '<span class="k-input-prefix">' +
                                '<span class="k-icon k-i-search"></span>' +
                            '</span>' +
                            '<input class="k-input" name="keywords" type="text" placeholder="关键字搜索" onkeyup="keywordsSearch(\'listView\', \'kendoListView\', this);">' +
                        '</span>' +
                    '</div>' +
                    '<div class="row mx-0 d-flex border-0" id="listView"></div>',
                actions: [
                    {
                        text: '确定',
                        primary: true,
                        action: function () {
                            selectRender('listView', 'listDialogSingle', 'kendoDropDownList');
                        }
                    },
                    {
                        text: '重置',
                        action: function () {
                            $('#listView').data('kendoListView').clearSelection();
                            renderSelect('listView', 'listDialogSingle', 'kendoDropDownList');
                            return false;
                        }
                    },
                    {
                        text: '取消',
                        action: function () {
                            confirmDialog.close();
                        }
                    }
                ],
                initOpen: function () {
                    var listDataSource = getListDataSource();
                    $('#listView').kendoListView({
                        dataSource: listDataSource,
                        layout: 'flex',
                        flex: {
                            wrap: 'wrap'
                        },
                        template: kendo.template(
                            '<div class="border-0 p-3 col-md-6 col-lg-4 col-xl-3">' +
                                '<div class="card">' +
                                    '<img class="card-img-top" src="#= photo.url #" alt="#= nickName #" title="#= realName #">' +
                                    '<div class="card-footer text-center p-2">' +
                                        '<label class="k-radio-label">' +
                                            '<input class="k-radio ids" type="radio" value="#= id #" onclick="selectSingle(\'listView\', \'kendoListView\', this);">' +
                                        '</label>' +
                                        '<span class="text-dark">#= realName #</span>' +
                                    '</div>' +
                                '</div>' +
                            '</div>'
                        ),
                        height: 440,
                        scrollable: 'endless',
                        selectable: true,
                        change: function () {
                            $('#listView .ids').prop('checked', false);
                            this.select().find('.ids').prop('checked', true);
                        },
                        dataBound: function () {
                            renderSelect('listView', 'listDialogSingle', 'kendoDropDownList');
                        }
                    });
                },
                close: function () {
                    confirmDialog.destroy();
                }
            }).data('kendoDialog');
            confirmDialog.open();
        }
    });
    // 列表视图对话框多选
    $('#listDialogMultiple').kendoMultiSelect({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/list.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data'
            }
        },
        placeholder: '-= 请点击右侧按钮选择 =-',
        dataValueField: 'id',
        dataTextField: 'realName'
    });
    $('#listDialogMultipleBtn').kendoButton({
        iconClass: 'listViewIcon',
        click: function () {
            var confirmDialog = $('<div class="p-0"></div>').kendoDialog({
                animation: {open: {effects: 'fade:in'}, close: {effects: 'fade:out'}},
                maxWidth: '40%',
                maxHeight: '60%',
                minWidth: 375,
                minHeight: 575,
                width: '40%',
                title: '列表视图对话框多选',
                content:
                    '<div class="k-toolbar border-top-0 border-left-0 border-right-0">' +
                        '<label class="k-checkbox-label mr-3">' +
                            '<input class="k-checkbox" id="selectAll" type="checkbox" onclick="selectAll(\'listView\', \'kendoListView\', this);">全选' +
                        '</label>' +
                        '<span class="k-searchbox flex-fill">' +
                            '<span class="k-input-prefix">' +
                                '<span class="k-icon k-i-search"></span>' +
                            '</span>' +
                            '<input class="k-input" name="keywords" type="text" placeholder="关键字搜索" onkeyup="keywordsSearch(\'listView\', \'kendoListView\', this);">' +
                        '</span>' +
                    '</div>' +
                    '<div class="row mx-0 d-flex border-0" id="listView"></div>',
                actions: [
                    {
                        text: '确定',
                        primary: true,
                        action: function () {
                            selectRender('listView', 'listDialogMultiple', 'kendoMultiSelect');
                        }
                    },
                    {
                        text: '重置',
                        action: function () {
                            $('#listView').data('kendoListView').clearSelection();
                            renderSelect('listView', 'listDialogMultiple', 'kendoMultiSelect');
                            return false;
                        }
                    },
                    {
                        text: '取消',
                        action: function () {
                            confirmDialog.close();
                        }
                    }
                ],
                initOpen: function () {
                    var listDataSource = getListDataSource();
                    $('#listView').kendoListView({
                        dataSource: listDataSource,
                        layout: 'flex',
                        flex: {
                            wrap: 'wrap'
                        },
                        template: kendo.template(
                            '<div class="border-0 p-3 col-md-6 col-lg-4 col-xl-3">' +
                                '<div class="card">' +
                                    '<img class="card-img-top" src="#= photo.url #" alt="#= nickName #" title="#= realName #">' +
                                    '<div class="card-footer text-center p-2">' +
                                        '<label class="k-checkbox-label">' +
                                            '<input class="k-checkbox ids" type="checkbox" value="#= id #" onclick="selectSingle(\'listView\', \'kendoListView\', this);">' +
                                        '</label>' +
                                        '<span class="text-dark">#= realName #</span>' +
                                    '</div>' +
                                '</div>' +
                            '</div>'
                        ),
                        height: 440,
                        scrollable: 'endless',
                        selectable: 'multiple',
                        change: function () {
                            $('#listView .ids').prop('checked', false);
                            this.select().find('.ids').prop('checked', true);
                            selectHalf('listView');
                        },
                        dataBound: function () {
                            selectHalf('listView');
                            renderSelect('listView', 'listDialogMultiple', 'kendoMultiSelect');
                        }
                    });
                },
                close: function () {
                    confirmDialog.destroy();
                }
            }).data('kendoDialog');
            confirmDialog.open();
        }
    });
    // 列表视图模态框单选
    $('#listWindowSingle').kendoDropDownList({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/list.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data'
            }
        },
        optionLabel: '-= 请点击右侧按钮选择 =-',
        dataValueField: 'id',
        dataTextField: 'realName'
    });
    $('#listWindowSingleBtn').kendoButton({
        iconClass: 'listViewIcon',
        click: function () {
            var confirmWindow = $('<div></div>').kendoWindow({
                animation: {open: {effects: 'fade:in'}, close: {effects: 'fade:out'}},
                maxWidth: '40%',
                maxHeight: '60%',
                minWidth: 375,
                minHeight: 600,
                width: '40%',
                title: {
                    text: '<i class="mr-2 theme-m listViewIcon"></i>列表视图模态框单选',
                    encoded: false
                },
                modal: true,
                pinned: true,
                resizable: false,
                open: function () {
                    var listDataSource = getListDataSource();
                    $('#listView').kendoListView({
                        dataSource: listDataSource,
                        layout: 'flex',
                        flex: {
                            wrap: 'wrap'
                        },
                        template: kendo.template(
                            '<div class="border-0 p-3 col-md-6 col-lg-4 col-xl-3">' +
                                '<div class="card">' +
                                    '<img class="card-img-top" src="#= photo.url #" alt="#= nickName #" title="#= realName #">' +
                                    '<div class="card-footer text-center p-2">' +
                                        '<label class="k-radio-label">' +
                                            '<input class="k-radio ids" type="radio" value="#= id #" onclick="selectSingle(\'listView\', \'kendoListView\', this);">' +
                                        '</label>' +
                                        '<span class="text-dark">#= realName #</span>' +
                                    '</div>' +
                                '</div>' +
                            '</div>'
                        ),
                        height: 426,
                        scrollable: 'endless',
                        selectable: true,
                        change: function () {
                            $('#listView .ids').prop('checked', false);
                            this.select().find('.ids').prop('checked', true);
                        },
                        dataBound: function () {
                            renderSelect('listView', 'listWindowSingle', 'kendoDropDownList');
                        }
                    });
                },
                close: function () {
                    confirmWindow.destroy();
                }
            }).data('kendoWindow');
            confirmWindow.content(
                '<div class="k-toolbar">' +
                    '<span class="k-searchbox flex-fill">' +
                        '<span class="k-input-prefix">' +
                            '<span class="k-icon k-i-search"></span>' +
                        '</span>' +
                        '<input class="k-input" name="keywords" type="text" placeholder="关键字搜索" onkeyup="keywordsSearch(\'listView\', \'kendoListView\', this);">' +
                    '</span>' +
                '</div>' +
                '<div class="row mx-0 d-flex border-top-0" id="listView"></div>' +
                '<div class="k-window-buttongroup pt-3">' +
                    '<button class="k-button k-button-lg k-primary confirm" type="button">确 定</button>' +
                    '<button class="k-button k-button-lg theme-m-box reset" type="button">重 置</button>' +
                    '<button class="k-button k-button-lg cancel" type="button">取 消</button>' +
                '</div>'
            ).center().open();
            confirmWindow.wrapper.find('.k-window-buttongroup .confirm').click(function () {
                selectRender('listView', 'listWindowSingle', 'kendoDropDownList');
                confirmWindow.close();
            });
            confirmWindow.wrapper.find('.k-window-buttongroup .reset').click(function () {
                $('#listView').data('kendoListView').clearSelection();
                renderSelect('listView', 'listWindowSingle', 'kendoDropDownList');
            });
            confirmWindow.wrapper.find('.k-window-buttongroup .cancel').click(function () {
                confirmWindow.close();
            });
        }
    });
    // 列表视图模态框多选
    $('#listWindowMultiple').kendoMultiSelect({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/list.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data'
            }
        },
        placeholder: '-= 请点击右侧按钮选择 =-',
        dataValueField: 'id',
        dataTextField: 'realName'
    });
    $('#listWindowMultipleBtn').kendoButton({
        iconClass: 'listViewIcon',
        click: function () {
            var confirmWindow = $('<div></div>').kendoWindow({
                animation: {open: {effects: 'fade:in'}, close: {effects: 'fade:out'}},
                maxWidth: '40%',
                maxHeight: '60%',
                minWidth: 375,
                minHeight: 600,
                width: '40%',
                title: {
                    text: '<i class="mr-2 theme-m listViewIcon"></i>列表视图模态框多选',
                    encoded: false
                },
                modal: true,
                pinned: true,
                resizable: false,
                open: function () {
                    var listDataSource = getListDataSource();
                    $('#listView').kendoListView({
                        dataSource: listDataSource,
                        layout: 'flex',
                        flex: {
                            wrap: 'wrap'
                        },
                        template: kendo.template(
                            '<div class="border-0 p-3 col-md-6 col-lg-4 col-xl-3">' +
                                '<div class="card">' +
                                    '<img class="card-img-top" src="#= photo.url #" alt="#= nickName #" title="#= realName #">' +
                                    '<div class="card-footer text-center p-2">' +
                                        '<label class="k-checkbox-label">' +
                                            '<input class="k-checkbox ids" type="checkbox" value="#= id #" onclick="selectSingle(\'listView\', \'kendoListView\', this);">' +
                                        '</label>' +
                                        '<span class="text-dark">#= realName #</span>' +
                                    '</div>' +
                                '</div>' +
                            '</div>'
                        ),
                        height: 426,
                        scrollable: 'endless',
                        selectable: 'multiple',
                        change: function () {
                            $('#listView .ids').prop('checked', false);
                            this.select().find('.ids').prop('checked', true);
                            selectHalf('listView');
                        },
                        dataBound: function () {
                            selectHalf('listView');
                            renderSelect('listView', 'listWindowMultiple', 'kendoMultiSelect');
                        }
                    });
                },
                close: function () {
                    confirmWindow.destroy();
                }
            }).data('kendoWindow');
            confirmWindow.content(
                '<div class="k-toolbar">' +
                    '<label class="k-checkbox-label mr-3">' +
                        '<input class="k-checkbox" id="selectAll" type="checkbox" onclick="selectAll(\'listView\', \'kendoListView\', this);">全选' +
                    '</label>' +
                    '<span class="k-searchbox flex-fill">' +
                        '<span class="k-input-prefix">' +
                            '<span class="k-icon k-i-search"></span>' +
                        '</span>' +
                        '<input class="k-input" name="keywords" type="text" placeholder="关键字搜索" onkeyup="keywordsSearch(\'listView\', \'kendoListView\', this);">' +
                    '</span>' +
                '</div>' +
                '<div class="row mx-0 d-flex border-top-0" id="listView"></div>' +
                '<div class="k-window-buttongroup pt-3">' +
                    '<button class="k-button k-button-lg k-primary confirm" type="button">确 定</button>' +
                    '<button class="k-button k-button-lg theme-m-box reset" type="button">重 置</button>' +
                    '<button class="k-button k-button-lg cancel" type="button">取 消</button>' +
                '</div>'
            ).center().open();
            confirmWindow.wrapper.find('.k-window-buttongroup .confirm').click(function () {
                selectRender('listView', 'listWindowMultiple', 'kendoMultiSelect');
                confirmWindow.close();
            });
            confirmWindow.wrapper.find('.k-window-buttongroup .reset').click(function () {
                $('#listView').data('kendoListView').clearSelection();
                renderSelect('listView', 'listWindowMultiple', 'kendoMultiSelect');
            });
            confirmWindow.wrapper.find('.k-window-buttongroup .cancel').click(function () {
                confirmWindow.close();
            });
        }
    });
    // 树形视图对话框单选
    $('#treeDialogSingle').kendoDropDownTree({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/tree.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data',
                model: {
                    id: 'id',
                    children: 'items'
                }
            }
        },
        placeholder: '-= 请点击右侧按钮选择 =-',
        dataValueField: 'id',
        dataTextField: 'text',
        dataBound: function () {
            this.value('');
        }
    });
    $('#treeDialogSingleBtn').kendoButton({
        iconClass: 'treeViewIcon',
        click: function () {
            var confirmDialog = $('<div class="p-0"></div>').kendoDialog({
                animation: {open: {effects: 'fade:in'}, close: {effects: 'fade:out'}},
                maxWidth: '25%',
                maxHeight: '60%',
                minWidth: 375,
                minHeight: 601,
                width: '25%',
                title: '树形视图对话框单选',
                content:
                    '<div class="k-toolbar border-top-0 border-left-0 border-right-0">' +
                        '<span class="k-searchbox flex-fill">' +
                            '<span class="k-input-prefix">' +
                                '<span class="k-icon k-i-search"></span>' +
                            '</span>' +
                            '<input class="k-input" name="keywords" type="text" placeholder="关键字搜索" onkeyup="keywordsSearch(\'treeView\', \'kendoTreeView\', this);">' +
                        '</span>' +
                    '</div>' +
                    '<div class="border-0 p-2" id="treeView"></div>',
                actions: [
                    {
                        text: '确定',
                        primary: true,
                        action: function () {
                            selectRender('treeView', 'treeDialogSingle', 'kendoDropDownTree');
                        }
                    },
                    {
                        text: '重置',
                        action: function () {
                            $('#treeView .ids').prop('checked', false);
                            renderSelect('treeView', 'treeDialogSingle', 'kendoDropDownTree');
                            return false;
                        }
                    },
                    {
                        text: '取消',
                        action: function () {
                            confirmDialog.close();
                        }
                    }
                ],
                initOpen: function () {
                    var treeDataSource = getTreeDataSource();
                    $('#treeView').height(451).kendoTreeView({
                        dataSource: treeDataSource,
                        checkboxes: {
                            template:
                                '<label class="k-radio-label">' +
                                    '<input class="k-radio ids" name="radio" type="radio" value="#= item.id #" onclick="selectSingle(\'treeView\', \'kendoTreeView\', this);">' +
                                    '# if (item.spriteCssClass) { #' +
                                        '<i class="k-sprite #= item.spriteCssClass #"></i>' +
                                    '# } else if (item.imageUrl) { #' +
                                        '<img class="k-image" src="#= item.imageUrl #">' +
                                    '# } #' +
                                    '#= item.text #' +
                                '</label>'
                        },
                        template: function () {
                            return ''
                        },
                        loadOnDemand: false,
                        dataBound: function () {
                            this.expand($('#treeView li'));
                            $('#treeView').find('.k-in').remove();
                            setTimeout(function () {
                                renderSelect('treeView', 'treeDialogSingle', 'kendoDropDownTree');
                            });
                        }
                    });
                },
                close: function () {
                    confirmDialog.destroy();
                }
            }).data('kendoDialog');
            confirmDialog.open();
        }
    });
    // 树形视图对话框多选
    $('#treeDialogMultiple').kendoDropDownTree({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/tree.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data',
                model: {
                    id: 'id',
                    children: 'items'
                }
            }
        },
        placeholder: '-= 请点击右侧按钮选择 =-',
        dataValueField: 'id',
        dataTextField: 'text',
        checkboxes: {
            checkChildren: true
        },
        dataBound: function () {
            this.value('');
        }
    });
    $('#treeDialogMultipleBtn').kendoButton({
        iconClass: 'treeViewIcon',
        click: function () {
            var confirmDialog = $('<div class="p-0"></div>').kendoDialog({
                animation: {open: {effects: 'fade:in'}, close: {effects: 'fade:out'}},
                maxWidth: '25%',
                maxHeight: '60%',
                minWidth: 375,
                minHeight: 601,
                width: '25%',
                title: '树形视图对话框多选',
                content:
                    '<div class="k-toolbar border-top-0 border-left-0 border-right-0">' +
                        '<label class="k-checkbox-label mr-3">' +
                            '<input class="k-checkbox" id="selectAll" type="checkbox" onclick="selectAll(\'treeView\', \'kendoTreeView\', this);">全选' +
                        '</label>' +
                        '<span class="k-searchbox flex-fill">' +
                            '<span class="k-input-prefix">' +
                                '<span class="k-icon k-i-search"></span>' +
                            '</span>' +
                            '<input class="k-input" name="keywords" type="text" placeholder="关键字搜索" onkeyup="keywordsSearch(\'treeView\', \'kendoTreeView\', this);">' +
                        '</span>' +
                    '</div>' +
                    '<div class="border-0 p-2" id="treeView"></div>',
                actions: [
                    {
                        text: '确定',
                        primary: true,
                        action: function () {
                            selectRender('treeView', 'treeDialogMultiple', 'kendoDropDownTree');
                        }
                    },
                    {
                        text: '重置',
                        action: function () {
                            $('#treeView .ids').prop('checked', false);
                            renderSelect('treeView', 'treeDialogMultiple', 'kendoDropDownTree');
                            return false;
                        }
                    },
                    {
                        text: '取消',
                        action: function () {
                            confirmDialog.close();
                        }
                    }
                ],
                initOpen: function () {
                    var treeDataSource = getTreeDataSource();
                    $('#treeView').height(451).kendoTreeView({
                        dataSource: treeDataSource,
                        checkboxes: {
                            checkChildren: true,
                            template:
                                '<label class="k-checkbox-label">' +
                                    '<input class="k-checkbox ids" name="checkbox" type="checkbox" value="#= item.id #" onclick="selectSingle(\'treeView\', \'kendoTreeView\', this);">' +
                                    '# if (item.spriteCssClass) { #' +
                                        '<i class="k-sprite #= item.spriteCssClass #"></i>' +
                                    '# } else if (item.imageUrl) { #' +
                                        '<img class="k-image" src="#= item.imageUrl #">' +
                                    '# } #' +
                                    '#= item.text #' +
                                '</label>'
                        },
                        template: function () {
                            return ''
                        },
                        loadOnDemand: false,
                        check: function () {
                            selectHalf('treeView');
                        },
                        dataBound: function () {
                            this.expand($('#treeView li'));
                            $('#treeView').find('.k-in').remove();
                            setTimeout(function () {
                                selectHalf('treeView');
                                renderSelect('treeView', 'treeDialogMultiple', 'kendoDropDownTree');
                            });
                        }
                    });
                },
                close: function () {
                    confirmDialog.destroy();
                }
            }).data('kendoDialog');
            confirmDialog.open();
        }
    });
    // 树形视图模态框单选
    $('#treeWindowSingle').kendoDropDownTree({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/tree.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data',
                model: {
                    id: 'id',
                    children: 'items'
                }
            }
        },
        placeholder: '-= 请点击右侧按钮选择 =-',
        dataValueField: 'id',
        dataTextField: 'text',
        dataBound: function () {
            this.value('');
        }
    });
    $('#treeWindowSingleBtn').kendoButton({
        iconClass: 'treeViewIcon',
        click: function () {
            var confirmWindow = $('<div></div>').kendoWindow({
                animation: {open: {effects: 'fade:in'}, close: {effects: 'fade:out'}},
                maxWidth: '25%',
                maxHeight: '60%',
                minWidth: 375,
                minHeight: 600,
                width: '25%',
                title: {
                    text: '<i class="mr-2 theme-m treeViewIcon"></i>树形视图模态框单选',
                    encoded: false
                },
                modal: true,
                pinned: true,
                resizable: false,
                open: function () {
                    var treeDataSource = getTreeDataSource();
                    $('#treeView').height(410).kendoTreeView({
                        dataSource: treeDataSource,
                        checkboxes: {
                            template:
                                '<label class="k-radio-label">' +
                                    '<input class="k-radio ids" name="radio" type="radio" value="#= item.id #" onclick="selectSingle(\'treeView\', \'kendoTreeView\', this);">' +
                                    '# if (item.spriteCssClass) { #' +
                                        '<i class="k-sprite #= item.spriteCssClass #"></i>' +
                                    '# } else if (item.imageUrl) { #' +
                                        '<img class="k-image" src="#= item.imageUrl #">' +
                                    '# } #' +
                                    '#= item.text #' +
                                '</label>'
                        },
                        template: function () {
                            return ''
                        },
                        loadOnDemand: false,
                        dataBound: function () {
                            this.expand($('#treeView li'));
                            $('#treeView').find('.k-in').remove();
                            setTimeout(function () {
                                renderSelect('treeView', 'treeWindowSingle', 'kendoDropDownTree');
                            });
                        }
                    });
                },
                close: function () {
                    confirmWindow.destroy();
                }
            }).data('kendoWindow');
            confirmWindow.content(
                '<div class="k-toolbar">' +
                    '<span class="k-searchbox flex-fill">' +
                        '<span class="k-input-prefix">' +
                            '<span class="k-icon k-i-search"></span>' +
                        '</span>' +
                        '<input class="k-input" name="keywords" type="text" placeholder="关键字搜索" onkeyup="keywordsSearch(\'treeView\', \'kendoTreeView\', this);">' +
                    '</span>' +
                '</div>' +
                '<div class="p-2 border border-top-0" id="treeView"></div>' +
                '<div class="k-window-buttongroup pt-3">' +
                    '<button class="k-button k-button-lg k-primary confirm" type="button">确 定</button>' +
                    '<button class="k-button k-button-lg theme-m-box reset" type="button">重 置</button>' +
                    '<button class="k-button k-button-lg cancel" type="button">取 消</button>' +
                '</div>'
            ).center().open();
            confirmWindow.wrapper.find('.k-window-buttongroup .confirm').click(function () {
                selectRender('treeView', 'treeWindowSingle', 'kendoDropDownTree');
                confirmWindow.close();
            });
            confirmWindow.wrapper.find('.k-window-buttongroup .reset').click(function () {
                $('#treeView .ids').prop('checked', false);
                renderSelect('treeView', 'treeWindowSingle', 'kendoDropDownTree');
            });
            confirmWindow.wrapper.find('.k-window-buttongroup .cancel').click(function () {
                confirmWindow.close();
            });
        }
    });
    // 树形视图模态框多选
    $('#treeWindowMultiple').kendoDropDownTree({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/tree.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data',
                model: {
                    id: 'id',
                    children: 'items'
                }
            }
        },
        placeholder: '-= 请点击右侧按钮选择 =-',
        dataValueField: 'id',
        dataTextField: 'text',
        checkboxes: {
            checkChildren: true
        },
        dataBound: function () {
            this.value('');
        }
    });
    $('#treeWindowMultipleBtn').kendoButton({
        iconClass: 'treeViewIcon',
        click: function () {
            var confirmWindow = $('<div></div>').kendoWindow({
                animation: {open: {effects: 'fade:in'}, close: {effects: 'fade:out'}},
                maxWidth: '25%',
                maxHeight: '60%',
                minWidth: 375,
                minHeight: 600,
                width: '25%',
                title: {
                    text: '<i class="mr-2 theme-m treeViewIcon"></i>树形视图模态框多选',
                    encoded: false
                },
                modal: true,
                pinned: true,
                resizable: false,
                open: function () {
                    var treeDataSource = getTreeDataSource();
                    $('#treeView').height(410).kendoTreeView({
                        dataSource: treeDataSource,
                        checkboxes: {
                            checkChildren: true,
                            template:
                                '<label class="k-checkbox-label">' +
                                    '<input class="k-checkbox ids" name="checkbox" type="checkbox" value="#= item.id #" onclick="selectSingle(\'treeView\', \'kendoTreeView\', this);">' +
                                    '# if (item.spriteCssClass) { #' +
                                        '<i class="k-sprite #= item.spriteCssClass #"></i>' +
                                    '# } else if (item.imageUrl) { #' +
                                        '<img class="k-image" src="#= item.imageUrl #">' +
                                    '# } #' +
                                    '#= item.text #' +
                                '</label>'
                        },
                        template: function () {
                            return ''
                        },
                        loadOnDemand: false,
                        check: function () {
                            selectHalf('treeView');
                        },
                        dataBound: function () {
                            this.expand($('#treeView li'));
                            $('#treeView').find('.k-in').remove();
                            setTimeout(function () {
                                selectHalf('treeView');
                                renderSelect('treeView', 'treeWindowMultiple', 'kendoDropDownTree');
                            });
                        }
                    });
                },
                close: function () {
                    confirmWindow.destroy();
                }
            }).data('kendoWindow');
            confirmWindow.content(
                '<div class="k-toolbar">' +
                    '<label class="k-checkbox-label mr-3">' +
                        '<input class="k-checkbox" id="selectAll" type="checkbox" onclick="selectAll(\'treeView\', \'kendoTreeView\', this);">全选' +
                    '</label>' +
                    '<span class="k-searchbox flex-fill">' +
                        '<span class="k-input-prefix">' +
                            '<span class="k-icon k-i-search"></span>' +
                        '</span>' +
                        '<input class="k-input" name="keywords" type="text" placeholder="关键字搜索" onkeyup="keywordsSearch(\'treeView\', \'kendoTreeView\', this);">' +
                    '</span>' +
                '</div>' +
                '<div class="p-2 border border-top-0" id="treeView"></div>' +
                '<div class="k-window-buttongroup pt-3">' +
                    '<button class="k-button k-button-lg k-primary confirm" type="button">确 定</button>' +
                    '<button class="k-button k-button-lg theme-m-box reset" type="button">重 置</button>' +
                    '<button class="k-button k-button-lg cancel" type="button">取 消</button>' +
                '</div>'
            ).center().open();
            confirmWindow.wrapper.find('.k-window-buttongroup .confirm').click(function () {
                selectRender('treeView', 'treeWindowMultiple', 'kendoDropDownTree');
                confirmWindow.close();
            });
            confirmWindow.wrapper.find('.k-window-buttongroup .reset').click(function () {
                $('#treeView .ids').prop('checked', false);
                renderSelect('treeView', 'treeWindowMultiple', 'kendoDropDownTree');
            });
            confirmWindow.wrapper.find('.k-window-buttongroup .cancel').click(function () {
                confirmWindow.close();
            });
        }
    });
    // 选项卡对话框单选
    $('#tabDialogSingle').kendoDropDownTree({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/group.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data',
                model: {
                    id: 'id',
                    children: 'items'
                }
            }
        },
        placeholder: '-= 请点击右侧按钮选择 =-',
        dataValueField: 'id',
        dataTextField: 'text'
    });
    $('#tabDialogSingleBtn').kendoButton({
        iconClass: 'tabStripIcon',
        click: function () {
            var confirmDialog = $('<div class="p-0"></div>').kendoDialog({
                animation: {open: {effects: 'fade:in'}, close: {effects: 'fade:out'}},
                maxWidth: '30%',
                maxHeight: '60%',
                minWidth: 375,
                minHeight: 299,
                width: '30%',
                title: '选项卡对话框单选',
                content:
                    '<div class="k-toolbar border-top-0 border-left-0 border-right-0">' +
                        '<span class="k-searchbox flex-fill">' +
                            '<span class="k-input-prefix">' +
                                '<span class="k-icon k-i-search"></span>' +
                            '</span>' +
                            '<input class="k-input" name="keywords" type="text" placeholder="关键字搜索" onkeyup="keywordsSearch(\'tabStrip\', \'kendoTabStrip\', this);">' +
                        '</span>' +
                    '</div>' +
                    '<div id="tabStrip"></div>',
                actions: [
                    {
                        text: '确定',
                        primary: true,
                        action: function () {
                            selectRender('tabStrip', 'tabDialogSingle', 'kendoDropDownTree');
                        }
                    },
                    {
                        text: '重置',
                        action: function () {
                            $('#tabStrip .ids').prop('checked', false);
                            renderSelect('tabStrip', 'tabDialogSingle', 'kendoDropDownTree');
                            return false;
                        }
                    },
                    {
                        text: '取消',
                        action: function () {
                            confirmDialog.close();
                        }
                    }
                ],
                initOpen: function () {
                    // 选项卡数据源
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/group.json',
                        succeed: function (res) {
                            var content =   '<ul class="bg-light">';
                            $.each(res.data, function (i, groups) {
                                content +=      '<li class="border-top-0 border-left-0 border-right-0 p-2">' + groups.text + '</li>';
                            });
                                content +=  '</ul>';
                            $.each(res.data, function (i, groups) {
                                content +=  '<div class="border-0">' +
                                                '<div class="row m-0">';
                                $.each(groups.items, function (k, group) {
                                    content +=      '<div class="col-sm-6 col-md-4 my-2 pl-2">' +
                                                        '<label class="k-radio-label">' +
                                                            '<input class="k-radio ids" name="radio" type="radio" value="' + group.id + '" onclick="selectSingle(\'tabStrip\', \'kendoTabStrip\', this);">' +
                                                            group.text +
                                                        '</label>' +
                                                    '</div>';
                                });
                                content +=      '</div>' +
                                            '</div>';
                            });
                            $('#tabStrip').html(content).height(200).kendoTabStrip({
                                animation: false,
                                select: function (e) {
                                    this.tabGroup.find('li').removeClass('theme-m-box');
                                    $(e.item).addClass('theme-m-box');
                                }
                            }).data('kendoTabStrip').select(0);
                            renderSelect('tabStrip', 'tabDialogSingle', 'kendoDropDownTree');
                        }
                    });
                },
                close: function () {
                    confirmDialog.destroy();
                }
            }).data('kendoDialog');
            confirmDialog.open();
        }
    });
    // 选项卡对话框多选
    $('#tabDialogMultiple').kendoDropDownTree({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/group.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data',
                model: {
                    id: 'id',
                    children: 'items'
                }
            }
        },
        placeholder: '-= 请点击右侧按钮选择 =-',
        dataValueField: 'id',
        dataTextField: 'text',
        checkboxes: {
            checkChildren: true
        }
    });
    $('#tabDialogMultipleBtn').kendoButton({
        iconClass: 'tabStripIcon',
        click: function () {
            var confirmDialog = $('<div class="p-0"></div>').kendoDialog({
                animation: {open: {effects: 'fade:in'}, close: {effects: 'fade:out'}},
                maxWidth: '30%',
                maxHeight: '60%',
                minWidth: 375,
                minHeight: 299,
                width: '30%',
                title: '选项卡对话框多选',
                content:
                    '<div class="k-toolbar border-top-0 border-left-0 border-right-0">' +
                        '<label class="k-checkbox-label mr-3">' +
                            '<input class="k-checkbox" id="selectAll" type="checkbox" onclick="selectAll(\'tabStrip\', \'kendoTabStrip\', this);">全选' +
                        '</label>' +
                        '<span class="k-searchbox flex-fill">' +
                            '<span class="k-input-prefix">' +
                                '<span class="k-icon k-i-search"></span>' +
                            '</span>' +
                            '<input class="k-input" name="keywords" type="text" placeholder="关键字搜索" onkeyup="keywordsSearch(\'tabStrip\', \'kendoTabStrip\', this);">' +
                        '</span>' +
                    '</div>' +
                    '<div id="tabStrip"></div>',
                actions: [
                    {
                        text: '确定',
                        primary: true,
                        action: function () {
                            selectRender('tabStrip', 'tabDialogMultiple', 'kendoDropDownTree');
                        }
                    },
                    {
                        text: '重置',
                        action: function () {
                            $('#tabStrip .ids, #tabStrip .k-tabstrip-items .k-checkbox').prop('indeterminate', false).prop('checked', false);
                            renderSelect('tabStrip', 'tabDialogMultiple', 'kendoDropDownTree');
                            return false;
                        }
                    },
                    {
                        text: '取消',
                        action: function () {
                            confirmDialog.close();
                        }
                    }
                ],
                initOpen: function () {
                    // 选项卡数据源
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/group.json',
                        succeed: function (res) {
                            var content =   '<ul class="bg-light">';
                            $.each(res.data, function (i, groups) {
                                content +=      '<li class="border-top-0 border-left-0 border-right-0 p-2">' +
                                                    '<label class="k-checkbox-label">' +
                                                        '<input class="k-checkbox" type="checkbox" onclick="selectTabAll(event, this);">' +
                                                    '</label>' +
                                                    groups.text +
                                                '</li>';
                            });
                                content +=  '</ul>';
                            $.each(res.data, function (i, groups) {
                                content +=  '<div class="border-0">' +
                                                '<div class="row m-0">';
                                $.each(groups.items, function (k, group) {
                                    content +=      '<div class="col-sm-6 col-md-4 my-2 pl-2">' +
                                                        '<label class="k-checkbox-label">' +
                                                            '<input class="k-checkbox ids" name="checkbox" type="checkbox" value="' + group.id + '" onclick="selectSingle(\'tabStrip\', \'kendoTabStrip\', this);">' +
                                                            group.text +
                                                        '</label>' +
                                                    '</div>';
                                });
                                content +=      '</div>' +
                                            '</div>';
                            });
                            $('#tabStrip').html(content).height(200).kendoTabStrip({
                                animation: false,
                                select: function (e) {
                                    this.tabGroup.find('li').removeClass('theme-m-box');
                                    $(e.item).addClass('theme-m-box');
                                }
                            }).data('kendoTabStrip').select(0);
                            renderSelect('tabStrip', 'tabDialogMultiple', 'kendoDropDownTree');
                        }
                    });
                },
                close: function () {
                    confirmDialog.destroy();
                }
            }).data('kendoDialog');
            confirmDialog.open();
        }
    });
    // 选项卡模态框单选
    $('#tabWindowSingle').kendoDropDownTree({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/group.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data',
                model: {
                    id: 'id',
                    children: 'items'
                }
            }
        },
        placeholder: '-= 请点击右侧按钮选择 =-',
        dataValueField: 'id',
        dataTextField: 'text'
    });
    $('#tabWindowSingleBtn').kendoButton({
        iconClass: 'tabStripIcon',
        click: function () {
            var confirmWindow = $('<div></div>').kendoWindow({
                animation: {open: {effects: 'fade:in'}, close: {effects: 'fade:out'}},
                maxWidth: '30%',
                maxHeight: '60%',
                minWidth: 375,
                minHeight: 380,
                width: '30%',
                title: {
                    text: '<i class="mr-2 theme-m tabStripIcon"></i>选项卡模态框单选',
                    encoded: false
                },
                modal: true,
                pinned: true,
                resizable: false,
                open: function () {
                    // 选项卡数据源
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/group.json',
                        succeed: function (res) {
                            var content =   '<ul>';
                            $.each(res.data, function (i, groups) {
                                content +=      '<li class="p-2">' + groups.text + '</li>';
                            });
                                content +=  '</ul>';
                            $.each(res.data, function (i, groups) {
                                content +=  '<div>' +
                                                '<div class="row m-0">';
                                $.each(groups.items, function (k, group) {
                                    content +=      '<div class="col-sm-6 col-md-4 my-2 pl-2">' +
                                                        '<label class="k-radio-label">' +
                                                            '<input class="k-radio ids" name="radio" type="radio" value="' + group.id + '" onclick="selectSingle(\'tabStrip\', \'kendoTabStrip\', this);">' +
                                                            group.text +
                                                        '</label>' +
                                                    '</div>';
                                });
                                content +=      '</div>' +
                                            '</div>';
                            });
                            $('#tabStrip').html(content).height(200).kendoTabStrip({
                                animation: false
                            }).data('kendoTabStrip').select(0);
                            renderSelect('tabStrip', 'tabWindowSingle', 'kendoDropDownTree');
                        }
                    });
                },
                close: function () {
                    confirmWindow.destroy();
                }
            }).data('kendoWindow');
            confirmWindow.content(
                '<div class="k-toolbar mb-2">' +
                    '<span class="k-searchbox flex-fill">' +
                        '<span class="k-input-prefix">' +
                            '<span class="k-icon k-i-search"></span>' +
                        '</span>' +
                        '<input class="k-input" name="keywords" type="text" placeholder="关键字搜索" onkeyup="keywordsSearch(\'tabStrip\', \'kendoTabStrip\', this);">' +
                    '</span>' +
                '</div>' +
                '<div id="tabStrip"></div>' +
                '<div class="k-window-buttongroup pt-3">' +
                    '<button class="k-button k-button-lg k-primary confirm" type="button">确 定</button>' +
                    '<button class="k-button k-button-lg theme-m-box reset" type="button">重 置</button>' +
                    '<button class="k-button k-button-lg cancel" type="button">取 消</button>' +
                '</div>'
            ).center().open();
            confirmWindow.wrapper.find('.k-window-buttongroup .confirm').click(function () {
                selectRender('tabStrip', 'tabWindowSingle', 'kendoDropDownTree');
                confirmWindow.close();
            });
            confirmWindow.wrapper.find('.k-window-buttongroup .reset').click(function () {
                $('#tabStrip .ids').prop('checked', false);
                renderSelect('tabStrip', 'tabWindowSingle', 'kendoDropDownTree');
            });
            confirmWindow.wrapper.find('.k-window-buttongroup .cancel').click(function () {
                confirmWindow.close();
            });
        }
    });
    // 选项卡模态框多选
    $('#tabWindowMultiple').kendoDropDownTree({
        dataSource: {
            transport: {
                read: function (options) {
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/group.json',
                        succeed: function (res) {
                            options.success(res);
                        },
                        failed: function (res) {
                            options.error(res);
                        }
                    });
                }
            },
            schema: {
                data: 'data',
                model: {
                    id: 'id',
                    children: 'items'
                }
            }
        },
        placeholder: '-= 请点击右侧按钮选择 =-',
        dataValueField: 'id',
        dataTextField: 'text',
        checkboxes: {
            checkChildren: true
        }
    });
    $('#tabWindowMultipleBtn').kendoButton({
        iconClass: 'tabStripIcon',
        click: function () {
            var confirmWindow = $('<div></div>').kendoWindow({
                animation: {open: {effects: 'fade:in'}, close: {effects: 'fade:out'}},
                maxWidth: '30%',
                maxHeight: '60%',
                minWidth: 375,
                minHeight: 380,
                width: '30%',
                title: {
                    text: '<i class="mr-2 theme-m tabStripIcon"></i>选项卡模态框多选',
                    encoded: false
                },
                modal: true,
                pinned: true,
                resizable: false,
                open: function () {
                    // 选项卡数据源
                    $.fn.ajaxPost({
                        ajaxUrl: 'json/group.json',
                        succeed: function (res) {
                            var content =   '<ul>';
                            $.each(res.data, function (i, groups) {
                                content +=      '<li class="p-2">' +
                                                    '<label class="k-checkbox-label">' +
                                                        '<input class="k-checkbox" type="checkbox" onclick="selectTabAll(event, this);">' +
                                                    '</label>' +
                                                    groups.text +
                                                '</li>';
                            });
                                content +=  '</ul>';
                            $.each(res.data, function (i, groups) {
                                content +=  '<div>' +
                                                '<div class="row m-0">';
                                $.each(groups.items, function (k, group) {
                                    content +=      '<div class="col-sm-6 col-md-4 my-2 pl-2">' +
                                                        '<label class="k-checkbox-label">' +
                                                            '<input class="k-checkbox ids" name="checkbox" type="checkbox" value="' + group.id + '" onclick="selectSingle(\'tabStrip\', \'kendoTabStrip\', this);">' +
                                                            group.text +
                                                        '</label>' +
                                                    '</div>';
                                });
                                content +=      '</div>' +
                                            '</div>';
                            });
                            $('#tabStrip').html(content).height(200).kendoTabStrip({
                                animation: false
                            }).data('kendoTabStrip').select(0);
                            renderSelect('tabStrip', 'tabWindowMultiple', 'kendoDropDownTree');
                        }
                    });
                },
                close: function () {
                    confirmWindow.destroy();
                }
            }).data('kendoWindow');
            confirmWindow.content(
                '<div class="k-toolbar mb-2">' +
                    '<label class="k-checkbox-label mr-3">' +
                        '<input class="k-checkbox" id="selectAll" type="checkbox" onclick="selectAll(\'tabStrip\', \'kendoTabStrip\', this);">全选' +
                    '</label>' +
                    '<span class="k-searchbox flex-fill">' +
                        '<span class="k-input-prefix">' +
                            '<span class="k-icon k-i-search"></span>' +
                        '</span>' +
                        '<input class="k-input" name="keywords" type="text" placeholder="关键字搜索" onkeyup="keywordsSearch(\'tabStrip\', \'kendoTabStrip\', this);">' +
                    '</span>' +
                '</div>' +
                '<div id="tabStrip"></div>' +
                '<div class="k-window-buttongroup pt-3">' +
                    '<button class="k-button k-button-lg k-primary confirm" type="button">确 定</button>' +
                    '<button class="k-button k-button-lg theme-m-box reset" type="button">重 置</button>' +
                    '<button class="k-button k-button-lg cancel" type="button">取 消</button>' +
                '</div>'
            ).center().open();
            confirmWindow.wrapper.find('.k-window-buttongroup .confirm').click(function () {
                selectRender('tabStrip', 'tabWindowMultiple', 'kendoDropDownTree');
                confirmWindow.close();
            });
            confirmWindow.wrapper.find('.k-window-buttongroup .reset').click(function () {
                $('#tabStrip .ids, #tabStrip .k-tabstrip-items .k-checkbox').prop('indeterminate', false).prop('checked', false);
                renderSelect('tabStrip', 'tabWindowMultiple', 'kendoDropDownTree');
            });
            confirmWindow.wrapper.find('.k-window-buttongroup .cancel').click(function () {
                confirmWindow.close();
            });
        }
    });
});

// 表格数据源
function getGridDataSource() {
    var gridDataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) { readItem(options, 'json/grid.json') }
        },
        schema: {
            total: 'total',
            data: 'data',
            model: {
                id: 'id',
                fields: {
                    userName: { type: 'string' },
                    realName: { type: 'string' },
                    nickName: { type: 'string' },
                    password: { type: 'string' },
                    confirmPassword: { type: 'string' },
                    online: { type: 'boolean' },
                    gender: { type: 'string' },
                    age: { type: 'number' },
                    height: { type: 'number' },
                    bloodType: { type: 'string' },
                    birthday: { type: 'date',
                        parse: function (e) {
                            return kendo.toString(kendo.parseDate(e), 'yyyy-MM-dd');
                        }
                    },
                    mateBirthday: { type: 'date',
                        parse: function (e) {
                            return kendo.toString(kendo.parseDate(e), 'yyyy-MM-dd');
                        }
                    },
                    creditCard: { type: 'string' },
                    asset: { type: 'number' },
                    nativePlace: { type: 'object' },
                    domicile: { type: 'object' },
                    nation: { type: 'object' },
                    zodiac: { type: 'object' },
                    language: { type: 'string' },
                    education: { type: 'object' },
                    graduation: { type: 'date',
                        parse: function (e) {
                            return kendo.toString(new Date(e), 'yyyy');
                        }
                    },
                    firstJob: { type: 'date',
                        parse: function (e) {
                            return kendo.toString(new Date(e), 'yyyy-MM');
                        }
                    },
                    mobile: { type: 'string' },
                    email: { type: 'string' },
                    homepage: { type: 'string' },
                    getUp: { type: 'date',
                        parse: function (e) {
                            return kendo.toString(kendo.parseDate(e), 'HH:mm');
                        }
                    },
                    importantMoment: { type: 'date',
                        parse: function (e) {
                            return kendo.toString(kendo.parseDate(e), 'yyyy-MM-dd HH:mm');
                        }
                    },
                    character: { type: 'number' },
                    color: { type: 'string' },
                    constellation: { type: 'object' },
                    tourism: { type: 'object' },
                    evaluation: { type: 'number' },
                    summary: { type: 'string' },
                    photo: { type: 'object' },
                    sign: { type: 'string' }
                }
            }
        },
        pageSize: 10
    });
    return gridDataSource;
}

// 表格字段列
function getGridColumns() {
    var gridColumns = [
        { field: 'userName', title: '用户名', width: '80px' },
        { field: 'realName', title: '姓名', width: '100px' },
        { field: 'nickName', title: '昵称', width: '110px' },
        { hidden: true, field: 'password', title: '密码', width: '70px',
            template: function (dataItem) {
                return dataItem.password.replace(dataItem.password.substr(0), '******');
            }
        },
        { field: 'online', title: '状态', width: '70px',
            template:
                '# if (online) { #' +
                    '<span class="dot-color k-notification-success"></span><span class="k-notification-success bg-transparent ml-2">在线</span>' +
                '# } else { #' +
                    '<span class="dot-color k-notification-error"></span><span class="k-notification-error bg-transparent ml-2">离线</span>' +
                '# } #'
        },
        { field: 'gender', title: '性别', width: '60px',
            values: [
                { text: '男', value: '1' },
                { text: '女', value: '2' }
            ]
        },
        { field: 'age', title: '年龄', width: '70px',
            template: '#= age # 岁'
        },
        { field: 'height', title: '身高', width: '80px',
            template: '#= kendo.toString(height, "0.00") # m'
        },
        { field: 'bloodType', title: '血型', width: '70px',
            values: [
                { text: 'A 型', value: '1' },
                { text: 'B 型', value: '2' },
                { text: 'O 型', value: '3' },
                { text: 'AB 型', value: '4' },
                { text: '其他', value: '5' }
            ]
        },
        { field: 'birthday', title: '生日', width: '110px' },
        { field: 'mateBirthday', title: '配偶生日', width: '110px' },
        { field: 'creditCard', title: '银行卡', width: '150px',
            template: function (dataItem) {
                return dataItem.creditCard.replace(dataItem.creditCard.substr(2, 12), '** **** **** **');
            }
        },
        { field: 'asset', title: '资产', width: '140px',
            format: '{0:c}'
        },
        { field: 'nativePlace', title: '籍贯', width: '250px',
            template: '#= nativePlace.provinceName # - #= nativePlace.cityName # - #= nativePlace.areaName #'
        },
        { field: 'domicile', title: '居住地', width: '100px',
            template: '#= domicile.name #'
        },
        { field: 'nation', title: '民族', width: '100px',
            template: '#= nation.nationName #'
        },
        { field: 'zodiac', title: '生肖', width: '60px',
            template: '#= zodiac.zodiacName #'
        },
        { field: 'language', title: '语言', width: '210px' },
        { field: 'education', title: '教育程度', width: '130px',
            template:
                '# for (var i = 0; i < education.length; i++) { #' +
                    '# if (education[i] === "1") { #' +
                        '小学&nbsp;' +
                    '# } else if (education[i] === "2") { #' +
                        '初中&nbsp;' +
                    '# } else if (education[i] === "3") { #' +
                        '高中&nbsp;' +
                    '# } else if (education[i] === "4") { #' +
                        '中专&nbsp;' +
                    '# } else if (education[i] === "5") { #' +
                        '大专&nbsp;' +
                    '# } else if (education[i] === "6") { #' +
                        '本科&nbsp;' +
                    '# } else if (education[i] === "7") { #' +
                        '硕士&nbsp;' +
                    '# } else if (education[i] === "8") { #' +
                        '博士&nbsp;' +
                    '# } else if (education[i] === "9") { #' +
                        '其他&nbsp;' +
                    '# } #' +
                '# } #'
        },
        { field: 'graduation', title: '毕业年份', width: '90px' },
        { field: 'firstJob', title: '参加工作年月', width: '110px' },
        { field: 'mobile', title: '手机', width: '120px' },
        { field: 'email', title: '电子邮件', width: '180px' },
        { field: 'homepage', title: '个人主页', width: '190px' },
        { field: 'getUp', title: '起床时间', width: '90px' },
        { field: 'importantMoment', title: '最有意义的时刻', width: '150px' },
        { field: 'character', title: '性格', width: '90px',
            values: [
                { text: '超级开朗', value: 10 },
                { text: '非常开朗', value: 8 },
                { text: '很开朗', value: 6 },
                { text: '比较开朗', value: 4 },
                { text: '有点开朗', value: 2 },
                { text: '普通', value: 0 },
                { text: '有点内向', value: -2 },
                { text: '比较内向', value: -4 },
                { text: '很内向', value: -6 },
                { text: '非常内向', value: -8 },
                { text: '超级内向', value: -10 }
            ]
        },
        { field: 'color', title: '颜色喜好', width: '90px',
            template: '<span style="display: inline-block; width: 100%; height: 24px; background: #= color #; border: 1px solid \\#c5c5c5; border-radius: 4px; vertical-align: middle;"></span>'
        },
        { field: 'constellation', title: '相配的星座', width: '170px',
            template:
                '# for (var i = 0; i < constellation.length; i++) { #' +
                    '# if (constellation[i] === "1") { #' +
                        '白羊座&nbsp;' +
                    '# } else if (constellation[i] === "2") { #' +
                        '金牛座&nbsp;' +
                    '# } else if (constellation[i] === "3") { #' +
                        '双子座&nbsp;' +
                    '# } else if (constellation[i] === "4") { #' +
                        '巨蟹座&nbsp;' +
                    '# } else if (constellation[i] === "5") { #' +
                        '狮子座&nbsp;' +
                    '# } else if (constellation[i] === "6") { #' +
                        '处女座&nbsp;' +
                    '# } else if (constellation[i] === "7") { #' +
                        '天秤座&nbsp;' +
                    '# } else if (constellation[i] === "8") { #' +
                        '天蝎座&nbsp;' +
                    '# } else if (constellation[i] === "9") { #' +
                        '射手座&nbsp;' +
                    '# } else if (constellation[i] === "10") { #' +
                        '山羊座&nbsp;' +
                    '# } else if (constellation[i] === "11") { #' +
                        '水瓶座&nbsp;' +
                    '# } else if (constellation[i] === "12") { #' +
                        '双鱼座&nbsp;' +
                    '# } #' +
                '# } #'
        },
        { field: 'tourism', title: '旅游足迹', width: '200px',
            template:
                '# for (var i = 0; i < tourism.length; i++) { #' +
                    '#= tourism[i].name #&nbsp;' +
                '# } #'
        },
        { field: 'evaluation', title: '自我评价', width: '90px',
            values: [
                { text: '不合格', value: 1 },
                { text: '待提升', value: 2 },
                { text: '合格', value: 3 },
                { text: '良好', value: 4 },
                { text: '优秀', value: 5 },
                { text: '完美', value: 6 }
            ]
        },
        { field: 'summary', title: '自我介绍', width: '310px' },
        { field: 'photo', title: '头像', width: '120px',
            template: '<a href="javascript:showBigPic(\'#= photo.url #\');"><img class="w-25 rounded-circle" src="#= photo.url #" alt="#= photo.name ##= photo.extension #"></a><small class="ml-2 text-muted">[#= kendo.toString(photo.size / 1024, "0.00") # KB]</small>'
        },
        { field: 'sign', title: '签名', width: '310px',
            template: '#= sign #'
        }
    ];
    return gridColumns;
}

// 树形列表数据源
function getTreeListDataSource() {
    var treeListDataSource = new kendo.data.TreeListDataSource({
        transport: {
            read: function (options) { readItem(options, 'json/treelist.json') }
        },
        schema: {
            total: 'total',
            data: 'data',
            model: {
                id: 'id',
                parentId: 'parentId',
                expanded: true,
                fields: {
                    userName: { type: 'string' },
                    realName: { type: 'string' },
                    nickName: { type: 'string' },
                    password: { type: 'string' },
                    confirmPassword: { type: 'string' },
                    online: { type: 'boolean' },
                    gender: { type: 'string' },
                    age: { type: 'number' },
                    height: { type: 'number' },
                    bloodType: { type: 'string' },
                    birthday: { type: 'date',
                        parse: function (e) {
                            return kendo.toString(kendo.parseDate(e), 'yyyy-MM-dd');
                        }
                    },
                    mateBirthday: { type: 'date',
                        parse: function (e) {
                            return kendo.toString(kendo.parseDate(e), 'yyyy-MM-dd');
                        }
                    },
                    creditCard: { type: 'string' },
                    asset: { type: 'number' },
                    nativePlace: { type: 'object' },
                    domicile: { type: 'object' },
                    nation: { type: 'object' },
                    zodiac: { type: 'object' },
                    language: { type: 'string' },
                    education: { type: 'object' },
                    graduation: { type: 'date',
                        parse: function (e) {
                            return kendo.toString(new Date(e), 'yyyy');
                        }
                    },
                    firstJob: { type: 'date',
                        parse: function (e) {
                            return kendo.toString(new Date(e), 'yyyy-MM');
                        }
                    },
                    mobile: { type: 'string' },
                    email: { type: 'string' },
                    homepage: { type: 'string' },
                    getUp: { type: 'date',
                        parse: function (e) {
                            return kendo.toString(kendo.parseDate(e), 'HH:mm');
                        }
                    },
                    importantMoment: { type: 'date',
                        parse: function (e) {
                            return kendo.toString(kendo.parseDate(e), 'yyyy-MM-dd HH:mm');
                        }
                    },
                    character: { type: 'number' },
                    color: { type: 'string' },
                    constellation: { type: 'object' },
                    tourism: { type: 'object' },
                    evaluation: { type: 'number' },
                    summary: { type: 'string' },
                    photo: { type: 'object' },
                    sign: { type: 'string' },
                    parentId: {
                        nullable: true
                    },
                    hasChildren: { type: 'boolean' }
                }
            }
        },
        pageSize: 20
    });
    return treeListDataSource;
}

// 树形列表字段列
function getTreeListColumns() {
    var treeListColumns = [
        { hidden: true, field: 'userName', title: '用户名', width: '80px' },
        { field: 'realName', title: '姓名', width: '150px',
            expandable: true
        },
        { field: 'nickName', title: '昵称', width: '110px' },
        { hidden: true, field: 'password', title: '密码', width: '70px',
            template: function (dataItem) {
                return dataItem.password.replace(dataItem.password.substr(0), '******');
            }
        },
        { field: 'online', title: '状态', width: '70px',
            template:
                '# if (online) { #' +
                    '<span class="dot-color k-notification-success"></span><span class="k-notification-success bg-transparent ml-2">在线</span>' +
                '# } else { #' +
                    '<span class="dot-color k-notification-error"></span><span class="k-notification-error bg-transparent ml-2">离线</span>' +
                '# } #'
        },
        { field: 'gender', title: '性别', width: '60px',
            template:
                '# if (gender === "1") { #' +
                    '男' +
                '# } else if (gender === "2") { #' +
                    '女' +
                '# } #'
        },
        { field: 'age', title: '年龄', width: '70px',
            template: '#= age # 岁'
        },
        { field: 'height', title: '身高', width: '80px',
            template: '#= kendo.toString(height, "0.00") # m'
        },
        { field: 'bloodType', title: '血型', width: '70px',
            template:
                '# if (bloodType === "1") { #' +
                    'A 型' +
                '# } else if (bloodType === "2") { #' +
                    'B 型' +
                '# } else if (bloodType === "3") { #' +
                    'O 型' +
                '# } else if (bloodType === "4") { #' +
                    'AB 型' +
                '# } else if (bloodType === "5") { #' +
                    '其他' +
                '# } #'
        },
        { field: 'birthday', title: '生日', width: '110px' },
        { field: 'mateBirthday', title: '配偶生日', width: '110px' },
        { field: 'creditCard', title: '银行卡', width: '150px',
            template: function (dataItem) {
                return dataItem.creditCard.replace(dataItem.creditCard.substr(2, 12), '** **** **** **');
            }
        },
        { field: 'asset', title: '资产', width: '140px',
            format: '{0:c}'
        },
        { field: 'nativePlace', title: '籍贯', width: '250px',
            template: '#= nativePlace.provinceName # - #= nativePlace.cityName # - #= nativePlace.areaName #'
        },
        { field: 'domicile', title: '居住地', width: '100px',
            template: '#= domicile.name #'
        },
        { field: 'nation', title: '民族', width: '100px',
            template: '#= nation.nationName #'
        },
        { field: 'zodiac', title: '生肖', width: '60px',
            template: '#= zodiac.zodiacName #'
        },
        { field: 'language', title: '语言', width: '210px' },
        { field: 'education', title: '教育程度', width: '130px',
            template:
                '# for (var i = 0; i < education.length; i++) { #' +
                    '# if (education[i] === "1") { #' +
                        '小学&nbsp;' +
                    '# } else if (education[i] === "2") { #' +
                        '初中&nbsp;' +
                    '# } else if (education[i] === "3") { #' +
                        '高中&nbsp;' +
                    '# } else if (education[i] === "4") { #' +
                        '中专&nbsp;' +
                    '# } else if (education[i] === "5") { #' +
                        '大专&nbsp;' +
                    '# } else if (education[i] === "6") { #' +
                        '本科&nbsp;' +
                    '# } else if (education[i] === "7") { #' +
                        '硕士&nbsp;' +
                    '# } else if (education[i] === "8") { #' +
                        '博士&nbsp;' +
                    '# } else if (education[i] === "9") { #' +
                        '其他&nbsp;' +
                    '# } #' +
                '# } #'
        },
        { field: 'graduation', title: '毕业年份', width: '90px' },
        { field: 'firstJob', title: '参加工作年月', width: '110px' },
        { field: 'mobile', title: '手机', width: '120px' },
        { field: 'email', title: '电子邮件', width: '180px' },
        { field: 'homepage', title: '个人主页', width: '190px' },
        { field: 'getUp', title: '起床时间', width: '90px' },
        { field: 'importantMoment', title: '最有意义的时刻', width: '150px' },
        { field: 'character', title: '性格', width: '90px',
            template:
                '# if (character === 10) { #' +
                    '超级开朗' +
                '# } else if (character === 8) { #' +
                    '非常开朗' +
                '# } else if (character === 6) { #' +
                    '很开朗' +
                '# } else if (character === 4) { #' +
                    '比较开朗' +
                '# } else if (character === 2) { #' +
                    '有点开朗' +
                '# } else if (character === 0) { #' +
                    '普通' +
                '# } else if (character === -2) { #' +
                    '有点内向' +
                '# } else if (character === -4) { #' +
                    '比较内向' +
                '# } else if (character === -6) { #' +
                    '很内向' +
                '# } else if (character === -8) { #' +
                    '非常内向' +
                '# } else if (character === -10) { #' +
                    '超级内向' +
                '# } #'
        },
        { field: 'color', title: '颜色喜好', width: '90px',
            template: '<span style="display: inline-block; width: 100%; height: 24px; background: #= color #; border: 1px solid \\#c5c5c5; border-radius: 4px; vertical-align: middle;"></span>'
        },
        { field: 'constellation', title: '相配的星座', width: '170px',
            template:
                '# for (var i = 0; i < constellation.length; i++) { #' +
                    '# if (constellation[i] === "1") { #' +
                        '白羊座&nbsp;' +
                    '# } else if (constellation[i] === "2") { #' +
                        '金牛座&nbsp;' +
                    '# } else if (constellation[i] === "3") { #' +
                        '双子座&nbsp;' +
                    '# } else if (constellation[i] === "4") { #' +
                        '巨蟹座&nbsp;' +
                    '# } else if (constellation[i] === "5") { #' +
                        '狮子座&nbsp;' +
                    '# } else if (constellation[i] === "6") { #' +
                        '处女座&nbsp;' +
                    '# } else if (constellation[i] === "7") { #' +
                        '天秤座&nbsp;' +
                    '# } else if (constellation[i] === "8") { #' +
                        '天蝎座&nbsp;' +
                    '# } else if (constellation[i] === "9") { #' +
                        '射手座&nbsp;' +
                    '# } else if (constellation[i] === "10") { #' +
                        '山羊座&nbsp;' +
                    '# } else if (constellation[i] === "11") { #' +
                        '水瓶座&nbsp;' +
                    '# } else if (constellation[i] === "12") { #' +
                        '双鱼座&nbsp;' +
                    '# } #' +
                '# } #'
        },
        { field: 'tourism', title: '旅游足迹', width: '200px',
            template:
                '# for (var i = 0; i < tourism.length; i++) { #' +
                    '#= tourism[i].name #&nbsp;' +
                '# } #'
        },
        { field: 'evaluation', title: '自我评价', width: '90px',
            template:
                '# if (evaluation === 1) { #' +
                    '不合格' +
                '# } else if (evaluation === 2) { #' +
                    '待提升' +
                '# } else if (evaluation === 3) { #' +
                    '合格' +
                '# } else if (evaluation === 4) { #' +
                    '良好' +
                '# } else if (evaluation === 5) { #' +
                    '优秀' +
                '# } else if (evaluation === 6) { #' +
                    '完美' +
                '# } #'
        },
        { field: 'summary', title: '自我介绍', width: '310px' },
        { field: 'photo', title: '头像', width: '120px',
            template: '<a href="javascript:showBigPic(\'#= photo.url #\');"><img class="w-25 rounded-circle" src="#= photo.url #" alt="#= photo.name ##= photo.extension #"></a><small class="ml-2 text-muted">[#= kendo.toString(photo.size / 1024, "0.00") # KB]</small>'
        },
        { field: 'sign', title: '签名', width: '310px',
            template: '#= sign #'
        }
    ];
    return treeListColumns;
}

// 列表视图数据源
function getListDataSource() {
    var listDataSource = new kendo.data.DataSource({
        transport: {
            read: function (options) { readItem(options, 'json/list.json') }
        },
        schema: {
            total: 'total',
            data: 'data',
            model: {
                id: 'id',
                fields: {
                    userName: { type: 'string' },
                    realName: { type: 'string' },
                    nickName: { type: 'string' },
                    password: { type: 'string' },
                    confirmPassword: { type: 'string' },
                    online: { type: 'boolean' },
                    gender: { type: 'string' },
                    age: { type: 'number' },
                    height: { type: 'number' },
                    bloodType: { type: 'string' },
                    birthday: { type: 'date',
                        parse: function (e) {
                            return kendo.toString(kendo.parseDate(e), 'yyyy-MM-dd');
                        }
                    },
                    mateBirthday: { type: 'date',
                        parse: function (e) {
                            return kendo.toString(kendo.parseDate(e), 'yyyy-MM-dd');
                        }
                    },
                    creditCard: { type: 'string' },
                    asset: { type: 'number' },
                    nativePlace: { type: 'object' },
                    domicile: { type: 'object' },
                    nation: { type: 'object' },
                    zodiac: { type: 'object' },
                    language: { type: 'string' },
                    education: { type: 'object' },
                    graduation: { type: 'date',
                        parse: function (e) {
                            return kendo.toString(new Date(e), 'yyyy');
                        }
                    },
                    firstJob: { type: 'date',
                        parse: function (e) {
                            return kendo.toString(new Date(e), 'yyyy-MM');
                        }
                    },
                    mobile: { type: 'string' },
                    email: { type: 'string' },
                    homepage: { type: 'string' },
                    getUp: { type: 'date',
                        parse: function (e) {
                            return kendo.toString(kendo.parseDate(e), 'HH:mm');
                        }
                    },
                    importantMoment: { type: 'date',
                        parse: function (e) {
                            return kendo.toString(kendo.parseDate(e), 'yyyy-MM-dd HH:mm');
                        }
                    },
                    character: { type: 'number' },
                    color: { type: 'string' },
                    constellation: { type: 'object' },
                    tourism: { type: 'object' },
                    evaluation: { type: 'number' },
                    summary: { type: 'string' },
                    photo: { type: 'object' },
                    sign: { type: 'string' }
                }
            }
        },
        pageSize: 12
    });
    return listDataSource;
}

// 树形视图数据源
function getTreeDataSource() {
    var treeDataSource = new kendo.data.HierarchicalDataSource({
        transport: {
            read: function (options) { readNode(options, 'json/tree.json') }
        },
        schema: {
            data: 'data',
            model: {
                id: 'id',
                children: 'items'
            }
        }
    });
    return treeDataSource;
}

// 赋值
function selectRender(type, dom, component) {
    var values = [];
    $.each($('#' + type).find('.ids:checked'), function (i, items) {
        values.push($(items).val());
    });
    $('#' + dom).data(component).value(values);
}

// 回显
function renderSelect(type, dom, component) {
    var values = $('#' + dom).data(component).value();
    if (values.length > 0) {
        if (Array.isArray(values)) {
            $.each(values, function (i, items) {
                if (type === 'treeView') {
                    $('#treeView').find('.ids[value=' + items + ']').not('.k-i-collapse + .k-checkbox-wrapper .ids').trigger('click');
                } else {
                    $('#' + type).find('.ids[value=' + items + ']').trigger('click');
                }
            });
        } else {
            if (type === 'treeView') {
                $('#treeView').find('.ids[value=' + values + ']').trigger('click');
            } else if (type === 'tabStrip') {
                $('#tabStrip').find('.ids[value=' + values + ']').trigger('click');
                $('#tabStrip').data('kendoTabStrip').select($('#tabStrip').find('.ids[value=' + values + ']').closest('.k-content').index() - 1);
            } else {
                $('#' + type).find('.ids[value=' + values + ']').closest('[data-uid]').trigger('click');
            }
        }
    }
}

// 关键字搜索
function keywordsSearch(type, kendoType, dom) {
    if (type === 'treeView') {
        if ($(dom).val() !== '') {
            $('#treeView li').hide();
            $.each($('#treeView label:contains(' + $(dom).val() + ')'), function () {
                $(this).parents('li').show();
            });
        } else {
            $('#treeView li').show();
        }
    } else if (type === 'tabStrip') {
        if ($(dom).val() !== '') {
            $('#tabStrip .k-item').hide();
            $('#tabStrip .k-content label').parent().hide();
            $.each($('#tabStrip .k-content label:contains(' + $(dom).val() + ')').toArray().reverse(), function () {
                $('#tabStrip .k-item').eq($(this).closest('.k-content').index() - 1).show();
                $('#tabStrip').data('kendoTabStrip').select($(this).closest('.k-content').index() - 1);
                $(this).parent().show();
            });
        } else {
            $('#tabStrip .k-item').show();
            $('#tabStrip .k-content label').parent().show();
        }
    } else {
        $('#' + type).data(kendoType).dataSource.filter({
            logic: 'or',
            filters: [
                { field: 'realName', operator: 'contains', value: $(dom).val() },
                { field: 'nickName', operator: 'contains', value: $(dom).val() }
            ]
        });
    }
}

// 全选
function selectAll(type, kendoType, dom) {
    if (type === 'treeView') {
        if ($(dom).prop('checked')) {
            $('#treeView .ids').prop('indeterminate', false).prop('checked', true);
        } else {
            $('#treeView .ids').prop('indeterminate', false).prop('checked', false);
        }
    } else if (type === 'tabStrip') {
        if ($(dom).prop('checked')) {
            $('#tabStrip .ids, #tabStrip .k-tabstrip-items .k-checkbox').prop('indeterminate', false).prop('checked', true);
        } else {
            $('#tabStrip .ids, #tabStrip .k-tabstrip-items .k-checkbox').prop('indeterminate', false).prop('checked', false);
        }
    } else {
        if ($(dom).prop('checked')) {
            $('#' + type).data(kendoType).select($('#' + type).find('[data-uid]'));
        } else {
            $('#' + type).data(kendoType).clearSelection();
        }
    }
}

// 单选
function selectSingle(type, kendoType, dom) {
    if ($(dom).hasClass('k-checkbox')) {
        selectHalf(type);
        if (type === 'tabStrip') {
            selectTabHalf(dom);
        }
    }
    if ($(dom).prop('checked')) {
        $('#' + type).data(kendoType).select($(dom).closest('[data-uid]'));
    } else {
        $(dom).closest('[data-uid]').removeClass('k-state-selected').removeAttr('aria-selected');
    }
}

// 半选
function selectHalf(type) {
    if ($('#' + type).find('.ids:checked').length < $('#' + type).find('.ids').length && $('#' + type).find('.ids:checked').length > 0) {
        $('#selectAll').prop('checked', false).prop('indeterminate', true);
    } else if ($('#' + type).find('.ids:checked').length === $('#' + type).find('.ids').length) {
        $('#selectAll').prop('indeterminate', false).prop('checked', true);
    } else {
        $('#selectAll').prop('indeterminate', false).prop('checked', false);
    }
}

// 选项卡全选
function selectTabAll(e, dom) {
    e.stopPropagation();
    if ($(dom).prop('checked')) {
        $('#tabStrip .k-content').eq($(dom).closest('.k-item').index()).find('.ids').prop('checked', true);
    } else {
        $('#tabStrip .k-content').eq($(dom).closest('.k-item').index()).find('.ids').prop('checked', false);
    }
    if ($('#tabStrip .k-tabstrip-items').find('.k-checkbox:checked').length < $('#tabStrip .k-tabstrip-items').find('.k-checkbox').length && $('#tabStrip .k-tabstrip-items').find('.k-checkbox:checked').length > 0) {
        $('#selectAll').prop('checked', false).prop('indeterminate', true);
    } else if ($('#tabStrip .k-tabstrip-items').find('.k-checkbox:checked').length === $('#tabStrip .k-tabstrip-items').find('.k-checkbox').length) {
        $('#selectAll').prop('indeterminate', false).prop('checked', true);
    } else {
        $('#selectAll').prop('indeterminate', false).prop('checked', false);
    }
}

// 选项卡半选
function selectTabHalf(dom) {
    if ($(dom).closest('.k-content').find('.ids:checked').length < $(dom).closest('.k-content').find('.ids').length && $(dom).closest('.k-content').find('.ids:checked').length > 0) {
        $('#tabStrip .k-item').eq($(dom).closest('.k-content').index() - 1).find('.k-checkbox').prop('checked', false).prop('indeterminate', true);
    } else if ($(dom).closest('.k-content').find('.ids:checked').length === $(dom).closest('.k-content').find('.ids').length) {
        $('#tabStrip .k-item').eq($(dom).closest('.k-content').index() - 1).find('.k-checkbox').prop('indeterminate', false).prop('checked', true);
    } else {
        $('#tabStrip .k-item').eq($(dom).closest('.k-content').index() - 1).find('.k-checkbox').prop('indeterminate', false).prop('checked', false);
    }
}