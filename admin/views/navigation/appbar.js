$(function () {
    // 普通导航栏
    $('#generalAppbar').kendoAppBar({
        themeColor: 'inherit',
        items: [
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<span class="k-icon k-i-menu"></span>' +
                    '</button>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'contentItem',
                template: '<h3 class="mb-0">Kendo UI</h3>'
            },
            {
                type: 'spacer'
            },
            {
                type: 'contentItem',
                template:
                    '<span class="k-searchbox">' +
                        '<span class="k-input-prefix">' +
                            '<span class="k-icon k-i-search"></span>' +
                        '</span>' +
                        '<input class="k-input pl-0" type="search" placeholder="搜索...">' +
                    '</span>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'spacer',
                width: 8,
                className: 'k-appbar-separator'
            },
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<i class="fas fa-envelope"></i>' +
                        '<span class="k-badge k-badge-solid k-badge-secondary k-badge-md k-badge-dot k-badge-inside k-top-end"></span>' +
                    '</button>'
            },
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<i class="fas fa-bell"></i>' +
                        '<span class="k-badge k-badge-solid k-badge-secondary k-badge-md k-badge-dot k-badge-inside k-top-end"></span>' +
                    '</button>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'spacer',
                width: 10,
                className: 'k-appbar-separator'
            },
            {
                type: 'contentItem',
                template: '<strong>IKKI</strong>'
            },
            {
                type: 'contentItem',
                template: '<img class="border rounded-circle shadow-sm" src="img/IKKI.png">'
            }
        ]
    });
    // 主色导航栏
    $('#primaryAppbar').kendoAppBar({
        themeColor: 'primary',
        items: [
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<span class="k-icon k-i-menu"></span>' +
                    '</button>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'contentItem',
                template: '<h3 class="mb-0">Kendo UI</h3>'
            },
            {
                type: 'spacer'
            },
            {
                type: 'contentItem',
                template:
                    '<span class="k-searchbox">' +
                        '<span class="k-input-prefix">' +
                            '<span class="k-icon k-i-search"></span>' +
                        '</span>' +
                        '<input class="k-input pl-0" type="search" placeholder="搜索...">' +
                    '</span>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'spacer',
                width: 8,
                className: 'k-appbar-separator'
            },
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<i class="fas fa-envelope"></i>' +
                        '<span class="k-badge k-badge-solid k-badge-secondary k-badge-md k-badge-dot k-badge-inside k-top-end"></span>' +
                    '</button>'
            },
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<i class="fas fa-bell"></i>' +
                        '<span class="k-badge k-badge-solid k-badge-secondary k-badge-md k-badge-dot k-badge-inside k-top-end"></span>' +
                    '</button>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'spacer',
                width: 10,
                className: 'k-appbar-separator'
            },
            {
                type: 'contentItem',
                template: '<strong>IKKI</strong>'
            },
            {
                type: 'contentItem',
                template: '<img class="border rounded-circle shadow-sm" src="img/IKKI.png">'
            }
        ]
    });
    // 次色导航栏
    $('#secondaryAppbar').kendoAppBar({
        themeColor: 'secondary',
        items: [
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<span class="k-icon k-i-menu"></span>' +
                    '</button>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'contentItem',
                template: '<h3 class="mb-0">Kendo UI</h3>'
            },
            {
                type: 'spacer'
            },
            {
                type: 'contentItem',
                template:
                    '<span class="k-searchbox">' +
                        '<span class="k-input-prefix">' +
                            '<span class="k-icon k-i-search"></span>' +
                        '</span>' +
                        '<input class="k-input pl-0" type="search" placeholder="搜索...">' +
                    '</span>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'spacer',
                width: 8,
                className: 'k-appbar-separator'
            },
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<i class="fas fa-envelope"></i>' +
                        '<span class="k-badge k-badge-solid k-badge-primary k-badge-md k-badge-dot k-badge-inside k-top-end"></span>' +
                    '</button>'
            },
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<i class="fas fa-bell"></i>' +
                        '<span class="k-badge k-badge-solid k-badge-primary k-badge-md k-badge-dot k-badge-inside k-top-end"></span>' +
                    '</button>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'spacer',
                width: 10,
                className: 'k-appbar-separator'
            },
            {
                type: 'contentItem',
                template: '<strong>IKKI</strong>'
            },
            {
                type: 'contentItem',
                template: '<img class="border rounded-circle shadow-sm" src="img/IKKI.png">'
            }
        ]
    });
    // 第三色导航栏
    $('#tertiaryAppbar').kendoAppBar({
        themeColor: 'tertiary',
        items: [
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<span class="k-icon k-i-menu"></span>' +
                    '</button>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'contentItem',
                template: '<h3 class="mb-0">Kendo UI</h3>'
            },
            {
                type: 'spacer'
            },
            {
                type: 'contentItem',
                template:
                    '<span class="k-searchbox">' +
                        '<span class="k-input-prefix">' +
                            '<span class="k-icon k-i-search"></span>' +
                        '</span>' +
                        '<input class="k-input pl-0" type="search" placeholder="搜索...">' +
                    '</span>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'spacer',
                width: 8,
                className: 'k-appbar-separator'
            },
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<i class="fas fa-envelope"></i>' +
                        '<span class="k-badge k-badge-solid k-badge-primary k-badge-md k-badge-dot k-badge-inside k-top-end"></span>' +
                    '</button>'
            },
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<i class="fas fa-bell"></i>' +
                        '<span class="k-badge k-badge-solid k-badge-primary k-badge-md k-badge-dot k-badge-inside k-top-end"></span>' +
                    '</button>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'spacer',
                width: 10,
                className: 'k-appbar-separator'
            },
            {
                type: 'contentItem',
                template: '<strong>IKKI</strong>'
            },
            {
                type: 'contentItem',
                template: '<img class="border rounded-circle shadow-sm" src="img/IKKI.png">'
            }
        ]
    });
    // 信息导航栏
    $('#infoAppbar').kendoAppBar({
        themeColor: 'info',
        items: [
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<span class="k-icon k-i-menu"></span>' +
                    '</button>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'contentItem',
                template: '<h3 class="mb-0">Kendo UI</h3>'
            },
            {
                type: 'spacer'
            },
            {
                type: 'contentItem',
                template:
                    '<span class="k-searchbox">' +
                        '<span class="k-input-prefix">' +
                            '<span class="k-icon k-i-search"></span>' +
                        '</span>' +
                        '<input class="k-input pl-0" type="search" placeholder="搜索...">' +
                    '</span>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'spacer',
                width: 8,
                className: 'k-appbar-separator'
            },
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<i class="fas fa-envelope"></i>' +
                        '<span class="k-badge k-badge-solid k-badge-secondary k-badge-md k-badge-dot k-badge-inside k-top-end"></span>' +
                    '</button>'
            },
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<i class="fas fa-bell"></i>' +
                        '<span class="k-badge k-badge-solid k-badge-secondary k-badge-md k-badge-dot k-badge-inside k-top-end"></span>' +
                    '</button>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'spacer',
                width: 10,
                className: 'k-appbar-separator'
            },
            {
                type: 'contentItem',
                template: '<strong>IKKI</strong>'
            },
            {
                type: 'contentItem',
                template: '<img class="border rounded-circle shadow-sm" src="img/IKKI.png">'
            }
        ]
    });
    // 成功导航栏
    $('#successAppbar').kendoAppBar({
        themeColor: 'success',
        items: [
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<span class="k-icon k-i-menu"></span>' +
                    '</button>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'contentItem',
                template: '<h3 class="mb-0">Kendo UI</h3>'
            },
            {
                type: 'spacer'
            },
            {
                type: 'contentItem',
                template:
                    '<span class="k-searchbox">' +
                        '<span class="k-input-prefix">' +
                            '<span class="k-icon k-i-search"></span>' +
                        '</span>' +
                        '<input class="k-input pl-0" type="search" placeholder="搜索...">' +
                    '</span>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'spacer',
                width: 8,
                className: 'k-appbar-separator'
            },
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<i class="fas fa-envelope"></i>' +
                        '<span class="k-badge k-badge-solid k-badge-secondary k-badge-md k-badge-dot k-badge-inside k-top-end"></span>' +
                    '</button>'
            },
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<i class="fas fa-bell"></i>' +
                        '<span class="k-badge k-badge-solid k-badge-secondary k-badge-md k-badge-dot k-badge-inside k-top-end"></span>' +
                    '</button>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'spacer',
                width: 10,
                className: 'k-appbar-separator'
            },
            {
                type: 'contentItem',
                template: '<strong>IKKI</strong>'
            },
            {
                type: 'contentItem',
                template: '<img class="border rounded-circle shadow-sm" src="img/IKKI.png">'
            }
        ]
    });
    // 警告导航栏
    $('#warningAppbar').kendoAppBar({
        themeColor: 'warning',
        items: [
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<span class="k-icon k-i-menu"></span>' +
                    '</button>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'contentItem',
                template: '<h3 class="mb-0">Kendo UI</h3>'
            },
            {
                type: 'spacer'
            },
            {
                type: 'contentItem',
                template:
                    '<span class="k-searchbox">' +
                        '<span class="k-input-prefix">' +
                            '<span class="k-icon k-i-search"></span>' +
                        '</span>' +
                        '<input class="k-input pl-0" type="search" placeholder="搜索...">' +
                    '</span>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'spacer',
                width: 8,
                className: 'k-appbar-separator'
            },
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<i class="fas fa-envelope"></i>' +
                        '<span class="k-badge k-badge-solid k-badge-secondary k-badge-md k-badge-dot k-badge-inside k-top-end"></span>' +
                    '</button>'
            },
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<i class="fas fa-bell"></i>' +
                        '<span class="k-badge k-badge-solid k-badge-secondary k-badge-md k-badge-dot k-badge-inside k-top-end"></span>' +
                    '</button>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'spacer',
                width: 10,
                className: 'k-appbar-separator'
            },
            {
                type: 'contentItem',
                template: '<strong>IKKI</strong>'
            },
            {
                type: 'contentItem',
                template: '<img class="border rounded-circle shadow-sm" src="img/IKKI.png">'
            }
        ]
    });
    // 错误导航栏
    $('#errorAppbar').kendoAppBar({
        themeColor: 'error',
        items: [
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<span class="k-icon k-i-menu"></span>' +
                    '</button>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'contentItem',
                template: '<h3 class="mb-0">Kendo UI</h3>'
            },
            {
                type: 'spacer'
            },
            {
                type: 'contentItem',
                template:
                    '<span class="k-searchbox">' +
                        '<span class="k-input-prefix">' +
                            '<span class="k-icon k-i-search"></span>' +
                        '</span>' +
                        '<input class="k-input pl-0" type="search" placeholder="搜索...">' +
                    '</span>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'spacer',
                width: 8,
                className: 'k-appbar-separator'
            },
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<i class="fas fa-envelope"></i>' +
                        '<span class="k-badge k-badge-solid k-badge-secondary k-badge-md k-badge-dot k-badge-inside k-top-end"></span>' +
                    '</button>'
            },
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<i class="fas fa-bell"></i>' +
                        '<span class="k-badge k-badge-solid k-badge-secondary k-badge-md k-badge-dot k-badge-inside k-top-end"></span>' +
                    '</button>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'spacer',
                width: 10,
                className: 'k-appbar-separator'
            },
            {
                type: 'contentItem',
                template: '<strong>IKKI</strong>'
            },
            {
                type: 'contentItem',
                template: '<img class="border rounded-circle shadow-sm" src="img/IKKI.png">'
            }
        ]
    });
    // 暗色导航栏
    $('#darkAppbar').kendoAppBar({
        themeColor: 'dark',
        items: [
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<span class="k-icon k-i-menu"></span>' +
                    '</button>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'contentItem',
                template: '<h3 class="mb-0">Kendo UI</h3>'
            },
            {
                type: 'spacer'
            },
            {
                type: 'contentItem',
                template:
                    '<span class="k-searchbox">' +
                        '<span class="k-input-prefix">' +
                            '<span class="k-icon k-i-search"></span>' +
                        '</span>' +
                        '<input class="k-input pl-0" type="search" placeholder="搜索...">' +
                    '</span>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'spacer',
                width: 8,
                className: 'k-appbar-separator'
            },
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<i class="fas fa-envelope"></i>' +
                        '<span class="k-badge k-badge-solid k-badge-secondary k-badge-md k-badge-dot k-badge-inside k-top-end"></span>' +
                    '</button>'
            },
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<i class="fas fa-bell"></i>' +
                        '<span class="k-badge k-badge-solid k-badge-secondary k-badge-md k-badge-dot k-badge-inside k-top-end"></span>' +
                    '</button>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'spacer',
                width: 10,
                className: 'k-appbar-separator'
            },
            {
                type: 'contentItem',
                template: '<strong>IKKI</strong>'
            },
            {
                type: 'contentItem',
                template: '<img class="border rounded-circle shadow-sm" src="img/IKKI.png">'
            }
        ]
    });
    // 亮色导航栏
    $('#lightAppbar').kendoAppBar({
        themeColor: 'light',
        items: [
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<span class="k-icon k-i-menu"></span>' +
                    '</button>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'contentItem',
                template: '<h3 class="mb-0">Kendo UI</h3>'
            },
            {
                type: 'spacer'
            },
            {
                type: 'contentItem',
                template:
                    '<span class="k-searchbox">' +
                        '<span class="k-input-prefix">' +
                            '<span class="k-icon k-i-search"></span>' +
                        '</span>' +
                        '<input class="k-input pl-0" type="search" placeholder="搜索...">' +
                    '</span>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'spacer',
                width: 8,
                className: 'k-appbar-separator'
            },
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<i class="fas fa-envelope"></i>' +
                        '<span class="k-badge k-badge-solid k-badge-secondary k-badge-md k-badge-dot k-badge-inside k-top-end"></span>' +
                    '</button>'
            },
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<i class="fas fa-bell"></i>' +
                        '<span class="k-badge k-badge-solid k-badge-secondary k-badge-md k-badge-dot k-badge-inside k-top-end"></span>' +
                    '</button>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'spacer',
                width: 10,
                className: 'k-appbar-separator'
            },
            {
                type: 'contentItem',
                template: '<strong>IKKI</strong>'
            },
            {
                type: 'contentItem',
                template: '<img class="border rounded-circle shadow-sm" src="img/IKKI.png">'
            }
        ]
    });
    // 反色导航栏
    $('#invertedAppbar').kendoAppBar({
        themeColor: 'inverted',
        items: [
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<span class="k-icon k-i-menu"></span>' +
                    '</button>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'contentItem',
                template: '<h3 class="mb-0">Kendo UI</h3>'
            },
            {
                type: 'spacer'
            },
            {
                type: 'contentItem',
                template:
                    '<span class="k-searchbox">' +
                        '<span class="k-input-prefix">' +
                            '<span class="k-icon k-i-search"></span>' +
                        '</span>' +
                        '<input class="k-input pl-0" type="search" placeholder="搜索...">' +
                    '</span>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'spacer',
                width: 8,
                className: 'k-appbar-separator'
            },
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<i class="fas fa-envelope"></i>' +
                        '<span class="k-badge k-badge-solid k-badge-secondary k-badge-md k-badge-dot k-badge-inside k-top-end"></span>' +
                    '</button>'
            },
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<i class="fas fa-bell"></i>' +
                        '<span class="k-badge k-badge-solid k-badge-secondary k-badge-md k-badge-dot k-badge-inside k-top-end"></span>' +
                    '</button>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'spacer',
                width: 10,
                className: 'k-appbar-separator'
            },
            {
                type: 'contentItem',
                template: '<strong>IKKI</strong>'
            },
            {
                type: 'contentItem',
                template: '<img class="border rounded-circle shadow-sm" src="img/IKKI.png">'
            }
        ]
    });
    // 顶部黏性导航栏
    $('#topStickyAppbar').kendoAppBar({
        themeColor: 'primary',
        position: 'top',
        positionMode: 'sticky',
        items: [
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<span class="k-icon k-i-menu"></span>' +
                    '</button>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'contentItem',
                template: '<h3 class="mb-0">Kendo UI</h3>'
            },
            {
                type: 'spacer'
            },
            {
                type: 'contentItem',
                template:
                    '<span class="k-searchbox">' +
                        '<span class="k-input-prefix">' +
                            '<span class="k-icon k-i-search"></span>' +
                        '</span>' +
                        '<input class="k-input pl-0" type="search" placeholder="搜索...">' +
                    '</span>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'spacer',
                width: 8,
                className: 'k-appbar-separator'
            },
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<i class="fas fa-envelope"></i>' +
                        '<span class="k-badge k-badge-solid k-badge-secondary k-badge-md k-badge-dot k-badge-inside k-top-end"></span>' +
                    '</button>'
            },
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<i class="fas fa-bell"></i>' +
                        '<span class="k-badge k-badge-solid k-badge-secondary k-badge-md k-badge-dot k-badge-inside k-top-end"></span>' +
                    '</button>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'spacer',
                width: 10,
                className: 'k-appbar-separator'
            },
            {
                type: 'contentItem',
                template: '<strong>IKKI</strong>'
            },
            {
                type: 'contentItem',
                template: '<img class="border rounded-circle shadow-sm" src="img/IKKI.png">'
            }
        ]
    });
    // 底部黏性导航栏
    $('#bottomStickyAppbar').kendoAppBar({
        themeColor: 'primary',
        position: 'bottom',
        positionMode: 'sticky',
        items: [
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<span class="k-icon k-i-menu"></span>' +
                    '</button>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'contentItem',
                template: '<h3 class="mb-0">Kendo UI</h3>'
            },
            {
                type: 'spacer'
            },
            {
                type: 'contentItem',
                template:
                    '<span class="k-searchbox">' +
                        '<span class="k-input-prefix">' +
                            '<span class="k-icon k-i-search"></span>' +
                        '</span>' +
                        '<input class="k-input pl-0" type="search" placeholder="搜索...">' +
                    '</span>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'spacer',
                width: 8,
                className: 'k-appbar-separator'
            },
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<i class="fas fa-envelope"></i>' +
                        '<span class="k-badge k-badge-solid k-badge-secondary k-badge-md k-badge-dot k-badge-inside k-top-end"></span>' +
                    '</button>'
            },
            {
                type: 'contentItem',
                template:
                    '<button class="k-button k-flat">' +
                        '<i class="fas fa-bell"></i>' +
                        '<span class="k-badge k-badge-solid k-badge-secondary k-badge-md k-badge-dot k-badge-inside k-top-end"></span>' +
                    '</button>'
            },
            {
                type: 'spacer',
                width: 5
            },
            {
                type: 'spacer',
                width: 10,
                className: 'k-appbar-separator'
            },
            {
                type: 'contentItem',
                template: '<strong>IKKI</strong>'
            },
            {
                type: 'contentItem',
                template: '<img class="border rounded-circle shadow-sm" src="img/IKKI.png">'
            }
        ]
    });
});