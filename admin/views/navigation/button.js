$(function () {
    // 样式按钮
    $('#cssButton').kendoButton({
        iconClass: 'fas fa-yin-yang mr-1'
    });
    // 图标按钮
    $('#iconButton').kendoButton({
        icon: 'star'
    });
    // 纯图标按钮
    $('#iconOnlyButton').kendoButton({
        icon: 'star'
    });
    // 图片按钮
    $('#imageButton').kendoButton({
        imageUrl: 'img/IKKI.png'
    });
    // 禁用按钮
    $('#disabledButton').kendoButton({
        enable: false
    });
    // 徽标按钮
    $('#badgeButton').kendoButton({
        badge: true
    });
    // 圆点徽标按钮
    $('#dotBadgeButton').kendoButton({
        badge: {
            shape: 'dot'
        }
    });
    // 圆形徽标按钮
    $('#circleBadgeButton').kendoButton({
        badge: {
            shape: 'circle',
            text: 8
        }
    });
    // 胶囊徽标按钮
    $('#pillBadgeButton').kendoButton({
        badge: {
            shape: 'pill',
            text: 8
        }
    });
    // 圆角徽标按钮
    $('#roundedBadgeButton').kendoButton({
        badge: {
            shape: 'rounded',
            text: 8
        }
    });
    // 直角徽标按钮
    $('#rectangleBadgeButton').kendoButton({
        badge: {
            shape: 'rectangle',
            text: 8
        }
    });
    // 图标徽标按钮
    $('#iconBadgeButton').kendoButton({
        badge: {
            icon: 'heart'
        }
    });
    // 继承色徽标按钮
    $('#inheritBadgeButton').kendoButton({
        badge: {
            themeColor: 'inherit',
            text: 8
        }
    });
    // 默认色徽标按钮
    $('#defaultBadgeButton').kendoButton({
        badge: {
            themeColor: 'default',
            text: 8
        }
    });
    // 主色徽标按钮
    $('#primaryBadgeButton').kendoButton({
        badge: {
            themeColor: 'primary',
            text: 8
        }
    });
    // 次色徽标按钮
    $('#secondaryBadgeButton').kendoButton({
        badge: {
            themeColor: 'secondary',
            text: 8
        }
    });
    // 第三色徽标按钮
    $('#tertiaryBadgeButton').kendoButton({
        badge: {
            themeColor: 'tertiary',
            text: 8
        }
    });
    // 信息徽标按钮
    $('#infoBadgeButton').kendoButton({
        badge: {
            themeColor: 'info',
            text: 8
        }
    });
    // 成功徽标按钮
    $('#successBadgeButton').kendoButton({
        badge: {
            themeColor: 'success',
            text: 8
        }
    });
    // 警告徽标按钮
    $('#warningBadgeButton').kendoButton({
        badge: {
            themeColor: 'warning',
            text: 8
        }
    });
    // 错误徽标按钮
    $('#errorBadgeButton').kendoButton({
        badge: {
            themeColor: 'error',
            text: 8
        }
    });
    // 暗色徽标按钮
    $('#darkBadgeButton').kendoButton({
        badge: {
            themeColor: 'dark',
            text: 8
        }
    });
    // 亮色徽标按钮
    $('#lightBadgeButton').kendoButton({
        badge: {
            themeColor: 'light',
            text: 8
        }
    });
    // 反色徽标按钮
    $('#invertedBadgeButton').kendoButton({
        badge: {
            themeColor: 'inverted',
            text: 8
        }
    });
    // 实心徽标按钮
    $('#solidBadgeButton').kendoButton({
        badge: {
            fill: 'solid',
            text: 8
        }
    });
    // 空心徽标按钮
    $('#outlineBadgeButton').kendoButton({
        badge: {
            fill: 'outline',
            text: 8
        }
    });
    // 描边徽标按钮
    $('#cutoutBorderBadgeButton').kendoButton({
        badge: {
            cutoutBorder: true,
            text: 8
        }
    });
    // 内嵌徽标按钮
    $('#inlineBadgeButton').kendoButton({
        badge: {
            position: 'inline'
        }
    });
    // 左上角徽标按钮
    $('#topStartBadgeButton').kendoButton({
        badge: {
            align: 'top start',
            position: 'edge'
        }
    });
    // 右上角徽标按钮
    $('#topEndBadgeButton').kendoButton({
        badge: {
            align: 'top end',
            position: 'edge'
        }
    });
    // 左下角徽标按钮
    $('#bottomStartBadgeButton').kendoButton({
        badge: {
            align: 'bottom start',
            position: 'edge'
        }
    });
    // 右下角徽标按钮
    $('#bottomEndBadgeButton').kendoButton({
        badge: {
            align: 'bottom end',
            position: 'edge'
        }
    });
    // 边沿徽标按钮
    $('#edgeBadgeButton').kendoButton({
        badge: {
            align: 'top end',
            position: 'edge'
        }
    });
    // 内沿徽标按钮
    $('#insideBadgeButton').kendoButton({
        badge: {
            align: 'top end',
            position: 'inside'
        }
    });
    // 外沿徽标按钮
    $('#outsideBadgeButton').kendoButton({
        badge: {
            align: 'top end',
            position: 'outside'
        }
    });
    // 小徽标按钮
    $('#smallBadgeButton').kendoButton({
        badge: {
            size: 'small',
            text: 8
        }
    });
    // 中徽标按钮
    $('#mediumBadgeButton').kendoButton({
        badge: {
            size: 'medium',
            text: 8
        }
    });
    // 大徽标按钮
    $('#largeBadgeButton').kendoButton({
        badge: {
            size: 'large',
            text: 8
        }
    });
    // 最大值徽标按钮
    $('#maxBadgeButton').kendoButton({
        badge: {
            text: 100,
            max: 99
        }
    });
    // 自定义徽标按钮
    $('#customBadgeButton').kendoButton({
        badge: {
            data: {
                current: 88,
                total: 100
            },
            template: '#= current # of #= total #'
        }
    });
    // 隐藏徽标按钮
    $('#hiddenBadgeButton').kendoButton({
        badge: {
            text: 0,
            visible: false
        }
    });
});