<div align="center">

[![LOGO](https://gitee.com/IKKI2000/KendoUI-Admin-Site/raw/master/img/logo.png)](https://www.telerik.com/kendo-ui)

# 🌸 Kendo UI Admin &amp; Site 🌌

Kendo UI Admin &amp; Site base on Kendo UI for jQuery and Bootstrap 4.

码云演示：[https://ikki2000.gitee.io/kendoui-admin-site/](https://ikki2000.gitee.io/kendoui-admin-site/)

[![IKKI Studio](https://img.shields.io/badge/IKKI%20Studio-%20-fff.svg?logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAADv0lEQVRoge2Y0XHbVhBFz82kABRgwkAH/BY/rFRgugJTFTipQJMKbFUguYIoFZiaMfgbpgJyqAKiDm4+gAc8iAApcmwRmtHOYAA8PAD37t7dtwC82sDM9uC272+yzHYn3l+e2T9Hmqd9V14GAentIs2zrksvg4A9BrKuSy+DgDTuSQF+fV4kh1uR5pkgQd3XBx8BwbkNPQEYPgHgPaIvAMOW0CLNE+AcuzcCgyZgmGInkl4mAezLcgd9WTDYHFik+UxShgSYcr9tg4xAkeYJ9udwXklo2TV3kASwvwGJKq8bmGxWD11RGJyEilF2LWkMou5A7Xnf/MFEoEjzRHAtqeo8I91L6777BhGBRZqPsb/ZTE1ZdSSBQwXiru/ek0agGGWJpN+By3Ik7hla1Wfe94yTEFikeWaYAZ8MSTkqpPKLUKr0X4JfTjardd+zno1ABXqK/R44h7o8Vr42Tc5G+revdj33pxAoRlmCdA6MgXfYGZAFUK3WoAarCLPD+IOk22chsEjzse2PNfBICrUkDEReL60iU4OvErgkcnV2v374aQSKUZYhfRJMbWcBRfBwkEItCZfQRdPfKFqcmj8PwrAGvuzDcBSBYpRlki4pE7HWboSxlkZVSxqrJeNtKcWNv30x2eP9gwkUoywBPkualS+uAKo8ij3YWvZj7Vf/ekqMbsabBMDSn5P79fwpmLaai74fSIs0nwLXQFK3twFYuLcC8KgMRl6nCRctrcfRuJlsVhedYI/thYo0vwb+sp3UCdehXT16SThSJaU6Bx7fG5J9B/g+20ugAj9zpOmwNSAradSg2kRMnAeRbJrCD3BzdiD4vQQWaX6NPWslZPTPMpzHciqJ0hxX84K0akKNrB5AF5P79cHgYUcSF6NsBsxinbfKYa1ZtfLVqKX5OhKRx9XMm0u6ONvRKuyzziQuRlkG/IOUhKSrk+xRNLaSsyLYbhOal9kOq+vXs81qfhDYjiTujoB0KUi2KlJ03tZv4+UA2NV4WWKZA0ubu8n9emdrcKhtUfr+5m0i6T8/9nw0uY5EWHgafT9U36532GtgOblfd37LHgX2KRGQNHU0OS75DXCCVOZYd0hz28unrJw/2rYI2H4HFdhQv9XoXdIN8PfZZvVDpXCsdUVgHOvbodRJV9hf9nWHz23bEYBxXHEES8OHXV9Fp7SthSyUx6otuLX921DBQweBkMC2l4YntbSntK4IlGVP+mOyWQ0aPHQvZEvbPLUfP7V1ldE74N8TYDnKtiMg3WInJ8Dyaq92CvsfRr5b2fzO9C8AAAAASUVORK5CYII=&style=social)](https://ikki2000.github.io/)
[![License](https://img.shields.io/badge/dynamic/json.svg?url=https://gitee.com/api/v5/repos/IKKI2000/KendoUI-Admin-Site/license&label=license&query=$.license&color=green)](https://ikki2000.gitee.io/kendoui-admin-site/LICENSE)
[![Release](https://img.shields.io/badge/dynamic/json.svg?url=https://gitee.com/api/v5/repos/IKKI2000/KendoUI-Admin-Site/releases/latest&label=release&query=$.name&color=blue)](https://gitee.com/IKKI2000/KendoUI-Admin-Site/releases)
<br>
[![码云仓库](https://img.shields.io/badge/码云仓库-%20-fff.svg?logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA4AAAAOCAMAAAAolt3jAAAAsVBMVEVHcEzHHSPFFBrHHiTHHSPHHSPHHSPHHSPHHSPHHSPHHSPHHiTHHSPHHSPHHSPHHSPHHSPHHSPEERjIICbEDxbEERj////99PXGFRzHHiTKJizJIynIISfxxca+AAC9AADDDRTGGiD++fnxyMn78PG7AADvv8H22dvHHCLBBQvmmJvstLb66+z++frppajww8T44ODMMjjCDBLvvL7rra/55eb11tfnn6H00tPnoKO4AACs08BTAAAAE3RSTlMAX/b10s9Y8COGHfooi1Blgo/5jOhs/AAAAKZJREFUCB0dwQUCglAQBcAHAgu27g9KOmzsuv/BRGfQc+c2ke25+HMolz1hGuhNWW44Zo6CtQMMBzLY7nVV6TJeuvDyzVa1Tdd1R/YnsGSgT0mdpEmSswWSsW7qd9uWT/YJJLlapcfTSxWhIFiSdflJ6/qhQmFhIuK9aprdSl0v/gJDM7jdD8Wh2J2zkQs4aw6jKAqzjA30DFP8+KMZ/saeTWR7YwBfy8EQvhb8Q+0AAAAASUVORK5CYII=&style=social)](https://gitee.com/IKKI2000/KendoUI-Admin-Site/)
[![Gitee Star](https://gitee.com/IKKI2000/KendoUI-Admin-Site/badge/star.svg?theme=white)](https://gitee.com/IKKI2000/KendoUI-Admin-Site/stargazers)
[![Gitee Fork](https://gitee.com/IKKI2000/KendoUI-Admin-Site/badge/fork.svg?theme=white)](https://gitee.com/IKKI2000/KendoUI-Admin-Site/members)
<br>
[![jQuery](https://img.shields.io/badge/jQuery-1.12.4%20(3.5.1)-0769ad.svg?labelColor=f5f5f5&logo=jQuery&logoColor=0769ad)](http://jquery.com/)
[![Kendo UI](https://img.shields.io/badge/Kendo%20UI-2021.1.224-f35800.svg?labelColor=f5f5f5&logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACAAAAAgCAYAAABzenr0AAAIWUlEQVRYhcWXf3AU5RnHP3u3l8tdApfkLuFiAmnI71aEACFCEqA6VkcgVYRBUGtVGK3WqtOOjrUzaGmr1lFRRK3UgdgpykxnWhhRtEpjVYLGDD8k5DDhSO5CYnJ3e3fkfu3e3m7/SDj5cZF2pjM+M+/s7L77Ps/neb7PvvcefMcmAKhJ5ZIvhsNhhoaG8XgHSSTiSw4ePPhIT4/rqng8ZnY6nZ6FCxe+WVtb96TJJIbz8vKoq63GYDB+q0/RlPW/ARzvcdW0tbU9d+TIkes1TUPXdQRBSF8LCwtHVqxo3djc3LStrrZa+78CdHR03LTx8Sd2hMPh3LKyMhYuXERZ2ffQNI1YPM7x7mMcOHAAWU6waNGi93//u03rrFZr4L8CUOTEtwY/dOhw64MPPfT3ZDJp+PENN9LU1EwsFkOWE3R2dtLYeCVWq5VYNMprr/2J0dERGubP79y8+fnFoihO6jwNEIuOTRo8EokW33Lrbd2SJOVfv2w5LS2L+fyzzwiFQ8iyzOHDh5g7dx5Wq5Wa6hrsDgfPP/8s4VCI++6796m1N695dDLfFmsuIkA0EpkUYOfOtx6WJCl/+owZNDe3EAoFKSkpwel0kkwm8Xo91NV9H5NJRDSZ0DSNtWvX8eorL9PW9saD11x91Ys5OTnDk/k3AKgpLeNQkqr17b177wRYdv1yZFnG7w/gD/gZ9Y0SDAYJ+ANIUgDfqA9JChAIBKiurqGqqppIJJK99933fq5qOplGGiAhKxnHocNHlgWDwal2u52KygpOnjyJzzdKKBQiHA4jBSVi8Xj63u/zMzoywunTgyxqagLgo/b2tSlVJdMAxiVQZDljeY4ePboYoLa2DqNRpL6+nkQigSzLJJNJkorCl0eP0jC/AaMoYjZnkZ1tQRRF7HYHAL19feX9/QMlJlE8nSmGCBAKhTICeL3emQDTnE76+voYGOjH7/MhyzKpVIp4PI7b7Wb3nt1kmUwYjUby8wvIL8jniitmY7PZCIfDDHi8lU6nc3KAObNnTdIieimA1WqlsrKSyspKBEFIzyYScaIvvMCGDRsQBMM3q/RxfS0WK+FwmIKCfEdNTVXGCCKAu98zCYAgAaRUld7eXjyeAaSARDQWRZFlFEXB5erh1VdeQTSZsFosTLXZsNvtzJo1i1RqXGe/P3DmxIneyQES8XjGyfy8/FPA0mAwSMOCRqqqzs8imUzy0pYXuedn9160Vtd1QqEwAFkmkzsaybzXiACiMfOeXT6zvBO4o7+/H1VV6er6Ap/PRzAYJJVKkYjH6enp4fU/b8NkMmE2Z1M0rYiqqioMBiOKIlNUVBSeN6/+1AWVJVs+8w3A9Okl6AgXxudHNtu+17dto6+vF0VRaGy8cqIHdHR9PMuXX97KXes3jLud+GECeG/fPgRBoKlp0b5Ch0NLh05E0GMhGPryG4BsUugBD+KMy+GcZsoxm0LNxVnR/YPxnI/a/0XDgkaOH+9GkiSi0SiKonDyZB9tbTvG9Z86leLLLqOqqppPP/0YAzrLrrt2W3TsDAZVxqQpmEbdkO88XwIAVAXtTABjXlH6kbb7mWfXC0dyPqGS/fs/pH7uPFpaFqc3nlRKJRGP09KyGIslm4ICO2azmTd3/pVIJMJ1uUHq9j/zhOGnzx3TjeKIriRJl2LCDOfe6MlEuoRq90e3G//x9B01pjHuLhghlVLZsf11JEnC6/EwOOhlYGAATdcZHPTi8Xjw+/18+MEHdHZ+jkNM8qD9NOYv/tak/2H5oVT/0dZMfSYAJKQR9JE+BNs0jPlOVNfBW8TnVr9hSIQMBnQ0BJ71T2fXmUJsNhvLlq+gvn4uRqMRv9+Pw+FAkiTefWcvXV2d2IwarxZ/RWVWHNABAR0DyR9c9YVau2SrPvua3cLQV8Hcq2+7CCBL73pno7jzkV8b1QQC49XQEUgh8JuRcj6I5qEDVouFsrIyzNnZ+H0+hoaG0HUdi5Biy2WnmGMOZ8xXA9SsHNQZc1w5T35Sl+4B7Wt3k2HXbzebuv8533iBUgI6og6PFXrxJs24FAvxeByXy3WBnjobizxcYZ7sfKFjAIxKnGRecSwtQeSlDV3Z/94+16BrgJbhgxwvpA6MpiysP13J12rWeXMGdO7NH+b2/K/TMJl8JBGRVzz6F8tNv7rPlGMbMwAciE7xx1PCRLEzhR8nNQCFxiRrsvpRk8nz5qxxiTU234Ta5wfXJzx7khbuOV3JE4cjJf4zUZkJn2Rf2brj9lDjWFvISUQT+Tbriwo83BXnq94+Eonxr2Z01McnLg9rPxOQkuJ54XUdVER2jRVx62ANRxJWpky1eQsdjtRZeMJBif3tHzdt3bplnxb25a6x+Vg1VcJmVBD08Rx0QWC/38Sdn2sMRs/J/pzdD6Aw28BPyo1cO8NKrgncSTPvjtlxyVbQYcnSpbtWrVq1bkHDPC19KA0HJVy9J0mpyvynn/7jnsFBb7FVUFmYE6NYlRgMjNER0DkY0NC0C7eSDHIJAra8PCpmlp/NEYPBwOpVqza3LF7yS13XtQUN87gIYFqhg1g8Xrh9+/Yt7e3taxKyzAmXC0W59P+GTHb28FpcXDy8/q677plZUbEnGo2h6zpnAQwXLsrKyvKtXLny5k2bNv1wemnp+6qqXjrlSUzX9ZF169Y9tnHj49Xl5eV7Mr0zaceVlpa2P/CLB9qbm1su7+npvvvAgY4b3G536bl6Z7IpU6Yos2fPbm9tbX2rqLDozZqa6oTBcFGelwY4a3a7/djq1avvv/HGlfcPDw8v7ejoWO31epuOHesuSaVUh8ViSeXm5rprampP1NfPebtiZsXO3Cm5Y9OmOTnldl/K/Xdv/wGItCOBpHMNewAAAABJRU5ErkJggg==)](https://www.telerik.com/kendo-jquery-ui)
[![Bootstrap](https://img.shields.io/badge/Bootstrap-4.6.0-7952b3.svg?labelColor=f5f5f5&logo=Bootstrap&logoColor=7952b3)](https://getbootstrap.com/)
[![Font Awesome](https://img.shields.io/badge/Font%20Awesome-5.15.2-228ae7.svg?labelColor=f5f5f5&logo=data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAYCAYAAADgdz34AAAB4ElEQVRIie2TvWtTURjGf+fcm5vb1mpsUwStUisNiDoICoKbk3UQBBdBB0cHg3+GOOri6uZiXMTqooMgqAiCrShYKrYmrRIag7m9H+fDRaWSm6bmRnDwmQ68nOfH857niN3lh7tyOw9cRzqnAJ/+KMTo+0l1riwmry5VkM6ZPhn/LqPvSqQz/VfMAaQzLemwlrwLrsyM8FMtTpZ8Hlwc4875UUpFNxOhDeA5cOnYEHsKDgd35LhweLC/AGWg2tQAWAuffpx7VVt+Y+HWy4AT+3waoeHRfMTp/T5Hxj2Kg5JmZHlVi3myEFMPDMN5wfEJj4ntLjeefkObLgCAWFsACr7k9rkRtvoSsW5+9tAArdhSa2qKQ5LCgOR9XXHzWQttbHfAr6GEbSk9EMAWTzC1iQJkL+J/wD8P6FoDY2FhVfF6OWHpq2bYExwd9yiN5XAkYKEeGB7PRyTKtt3fENCMLFfuNXixGNOKLT8/g+8KpkZd9o44NNYMbz4r6oFJ9dgQEMSW54sxa4lFrPtpkbLMriTMriRd8nd4g/agvSs1wceG5t2XhA+rmihlr38iMXmtluqQdwXaWFT6ajctCYRpg0hlNwdCidEzmW06yegZqZbfljG6QockPSrE6EpSnbv8HSftsykEPoqFAAAAAElFTkSuQmCC)](https://fontawesome.com/)
<br>
![Internet Explorer](https://img.shields.io/badge/IE-10+-00bcf2.svg?logo=Internet%20Explorer&logoColor=fff)
![Microsoft Edge](https://img.shields.io/badge/Edge-last%202%20versions-037ad7.svg?logo=Microsoft%20Edge&logoColor=fff)
![Firefox Browser](https://img.shields.io/badge/Firefox-last%202%20versions-ff8b04.svg?logo=Firefox%20Browser&logoColor=fff)
![Google Chrome](https://img.shields.io/badge/Chrome-last%202%20versions-19a15f.svg?logo=Google%20Chrome&logoColor=fff)
<br>
![Safari](https://img.shields.io/badge/Safari-last%202%20versions-05aee2.svg?logo=Safari&logoColor=fff)
![Opera](https://img.shields.io/badge/Opera-last%202%20versions-ff1b2d.svg?logo=Opera&logoColor=fff)
![Electron](https://img.shields.io/badge/Electron-last%202%20versions-9feaf9.svg?logo=Electron&logoColor=fff)
<br>
![Language](https://img.shields.io/badge/language-JavaScript-yellow.svg)
![HTML](https://img.shields.io/static/v1?label=HTML&message=28.8%&labelColor=f5f5f5&color=e34f26&logo=HTML5&logoColor=e34f26)
![CSS](https://img.shields.io/static/v1?label=CSS&message=1.9%&labelColor=f5f5f5&color=1572b6&logo=CSS3&logoColor=1572b6)
![JavaScript](https://img.shields.io/static/v1?label=JavaScript&message=69.3%&labelColor=f5f5f5&color=f7df1e&logo=JavaScript&logoColor=f7df1e)
<br>
![Default](https://img.shields.io/badge/❀-默认-1890ff.svg?labelColor=69c0ff)
![Brown](https://img.shields.io/badge/❀-棕色-c39b8f.svg?labelColor=d9b6ac)
![Pink](https://img.shields.io/badge/❀-桃色-d770ad.svg?labelColor=ec87c0)
![Red](https://img.shields.io/badge/❀-红色-da4453.svg?labelColor=ed5565)
![Orange](https://img.shields.io/badge/❀-橙色-ff9800.svg?labelColor=ffb74d)
![Yellow](https://img.shields.io/badge/❀-黄色-f6bb42.svg?labelColor=ffce54)
![White](https://img.shields.io/badge/❀-白色-e6e9ed.svg?labelColor=f5f7fa)
![Grass](https://img.shields.io/badge/❀-翠色-8cc152.svg?labelColor=a0d468)
![Green](https://img.shields.io/badge/❀-绿色-37bc9b.svg?labelColor=48cfad)
![Cyan](https://img.shields.io/badge/❀-青色-3bafda.svg?labelColor=4fc1e9)
![Blue](https://img.shields.io/badge/❀-蓝色-4a89dc.svg?labelColor=5d9cec)
![Purple](https://img.shields.io/badge/❀-紫色-967adc.svg?labelColor=ac92ec)
![Black](https://img.shields.io/badge/❀-黑色-434a54.svg?labelColor=656d78)
![Gray](https://img.shields.io/badge/❀-灰色-aab2bd.svg?labelColor=ccd1d9)

</div>

![Home](https://gitee.com/IKKI2000/KendoUI-Admin-Site/raw/master/img/screenshot/home_pc.png)

![Forms](https://gitee.com/IKKI2000/KendoUI-Admin-Site/raw/master/img/screenshot/forms.png)

![Grid](https://gitee.com/IKKI2000/KendoUI-Admin-Site/raw/master/img/screenshot/grid.png)

![Themes](https://gitee.com/IKKI2000/KendoUI-Admin-Site/raw/master/img/screenshot/themes.png)

## 🌟 特点 <small>Features</small>

* 无前端工程化、零配置
* 下载无需安装、开箱即用
* 前后端分离
* MVVM 视图模型（by Kendo UI for jQuery）
* SPA 单页面应用（by Kendo UI for jQuery）
* Router 页面路由（by Kendo UI for jQuery）
* Templates 模版渲染（by Kendo UI for jQuery）
* Data Source 统一数据源（by Kendo UI for jQuery）
* PC 端、移动端栅格系统响应式布局（by Bootstrap 4）
* 无 CSS Sprites、矢量化图标字体（by Font Awesome）
* 统一风格的前台网站和后台管理界面
* 后台模式支持 6 种组合框架布局
    * [路由版 <small>Router Edition</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/index_router.html)（推荐）
    * [框架版 <small>iFrame Edition</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/index_iframe.html)
    * [标签版 <small>Tabstrip Edition</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/index_tabstrip.html)
    * [布局路由版 <small>Splitter Router Edition</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/index_splitter_router.html)
    * [布局框架版 <small>Splitter iFrame Edition</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/index_splitter_iframe.html)
    * [布局标签版 <small>Splitter Tabstrip Edition</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/index_splitter_tabstrip.html)
* 菜单、导航动态配置
* Sass 样式预处理
* 6 套 80 种主题配色可选
    * IKKI Amikoko（14 种）
    * Ant Design（13 种）
    * Kendo UI Less（16 种）
    * Kendo UI Sass（7 种）
    * Bootstrap v4（10 种）
    * Material Design（20 种）
* [Kendo UI 官方简体、繁体汉化提供者](https://github.com/telerik/kendo-ui-core/blob/master/src/messages/kendo.messages.zh-CN.js)

## 🌐 浏览器支持 <small>Browser Support</small>

* IE10 及以上、现代浏览器和 Electron 桌面应用

| <img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/archive/internet-explorer_9-11/internet-explorer_9-11_32x32.png" alt="IE"><br>IE | <img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/edge/edge_32x32.png" alt="Edge"><br>Edge | <img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/firefox/firefox_32x32.png" alt="Firefox"><br>Firefox | <img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/chrome/chrome_32x32.png" alt="Chrome"><br>Chrome | <img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/safari/safari_32x32.png" alt="Safari"><br>Safari | <img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/opera/opera_32x32.png" alt="Opera"><br>Opera | <img src="https://raw.githubusercontent.com/alrra/browser-logos/master/src/electron/electron_32x32.png" alt="Electron"><br>Electron |
| :----------: | :----------: | :----------: | :----------: | :----------: | :----------: | :----------: |
| 10, 11 | last 2 versions | last 2 versions | last 2 versions | last 2 versions | last 2 versions | last 2 versions |

## 📖 使用指南 <small>Initialization</small>

1. [下载](https://gitee.com/IKKI2000/KendoUI-Admin-Site/repository/archive/master.zip)并解压至项目**根**目录~
2. 将下列 **5** 个 HTML 文件的 `<base>` 修改为前端本地的开发**根**路径~
    ```diff
    index.html
    --- <base href="https://ikki2000.gitee.io/kendoui-admin-site/">
    +++ <base href="http://localhost:8888/YourProject/">
    
    admin/login.html & admin/index.html
    --- <base href="https://ikki2000.gitee.io/kendoui-admin-site/" type="admin">
    +++ <base href="http://localhost:8888/YourProject/" type="admin">
    
    site/login.html & site/index.html
    --- <base href="https://ikki2000.gitee.io/kendoui-admin-site/" type="site">
    +++ <base href="http://localhost:8888/YourProject/" type="site">
    ```
    > 注意：最后的 `/` 不要漏掉~
3. 将下列 JS 文件的 `apiPath` 修改为后端服务器的 API 接口**根**路径~ 并恢复 **3** 个默认参数~
    ```diff
    js/ikki.js
    
    // 配置接口路径
    --- var apiPath = 'https://ikki2000.gitee.io/kendoui-admin-site/';
    +++ var apiPath = 'https://dev.YourDomain.com/api/';
    
    // Ajax 提交
    --- ajaxType: 'get', // Gitee Pages 演示只支持 get 请求，正常使用请改回 post 请求
    +++ ajaxType: 'post',
    --- urlType: 'static', // Gitee Pages 演示接口为静态 json 文件，正常使用请改回 api 类型
    +++ urlType: 'api',
    
    // 带二进制流的 Ajax 提交
    --- ajaxType: 'get', // Gitee Pages 演示只支持 get 请求，正常使用请改回 post 请求
    +++ ajaxType: 'post',
    ```
4. 用 IDE 编辑器（如：WebStorm）打开 `index.html` 并选择浏览器启动本地服务即可~
    ```text
    http://localhost:8888/YourProject/index.html
    ```

## 🔨 开发指南 <small>Developer's Guide</small>

* 所有的子页面模块均存放在 `views` 目录或其自定义的子目录下
* 每一个子页面模块均由同名的 `xxx.html` 和 `xxx.js` 两个文件组成
* 每一个子页面模块的 HTML 页面第一行的模版 ID 由文件名 `xxx` 和 `Temp` 组成
    ```html
    <script id="xxxTemp" type="text/x-kendo-template">
    ```
* 只应用于当前子页面模块的样式写在模块的 HTML 文件中
    ```html
    <style scoped>
        ···
    </style>
    ```
* 只应用于当前子页面模块的 css 文件写在模块的 HTML 文件中并通过 @import 引入
    ```html
    <style scoped>
        @import "css/plugin.min.css";
    </style>
    ```
* 子页面模块的 HTML 文件基本结构如下：
    ```html
    <script id="xxxTemp" type="text/x-kendo-template">
        <div>
            ···
        </div>
        <script id="otherTemplate" type="text/x-kendo-template">
            ···
        </script>
        <style scoped>
            ···
        </style>
    </script>
    ```
* 只应用于当前子页面模块的 JS 插件通过模块的 JS 文件引入
    ```js
    $.getScript('js/plugin.min.js');
    ```
* 顶部菜单本地 Mock 数据位于 `json/menu.json`<br>其中 `url` 键值包含的 `linkTo` 方法为路由函数<br>其中第一个参数为子页面模块相对于 `views` 目录所在的目录<br>第二个参数为子页面模块的名称<br>`cssClass` 键值为面包屑要用到的 DOM 定位，由 `links-模块名称` 组成
* 左侧导航本地 Mock 数据位于 `json/nav.json` 其键值说明如下：
    * `id` 为唯一标识符
    * `parentId` 为父 ID ，一级为 0
    * `sort` 为所在层级排序
    * `name` 为名称
    * `subName` 为副名称
    * `icon` 为图标
    * `dot` 为折叠后的圆点提醒
    * `badge` 为徽标提醒
    * `directory` 为子页面模块相对于 `views` 目录所在的目录
    * `location` 为子页面模块的名称
    * `expanded` 为是否默认展开
* 顶部菜单、左侧导航及 Token 验证的 API 接口地址位于 `ikki.layout.js` 文件内
* `ikki.js` 文件内封装了一些公用方法，具体参数及说明如下：

    | 方法 | 参数 | 类型 | 默认值 | 说明 |
    | ----- | ----- | ----- | ----- | ----- |
    | **$.fn.ajaxPost** | --- | --- | --- | 封装的带 **token** 的 ajax 提交 |
    |  | *ajaxAsync* | boolean | true | ajax 的 async 属性 |
    |  | *ajaxType* | string | 'post' | ajax 的 type 属性 |
    |  | *ajaxData* | string | '' | 用 **JSON.stringify()** 封装的 ajax 的 data 属性 |
    |  | *urlType* | string | 'api' | 读取本地 json 的时候换成 'static' |
    |  | *ajaxUrl* | string | '' | ajax 的 url 属性 |
    |  | *ajaxContentType* | string | 'application/json; charset=UTF-8' | ajax 的 contentType 属性 |
    |  | *finished* | function | {} | ajax 请求完成时的回调 |
    |  | *succeed* | function | {} | ajax 请求完成并且 result === 'y' 时的回调 |
    |  | *failed* | function | {} | ajax 请求完成并且 result === 'n' 时的回调 |
    |  | *isMsg* | boolean | false | result === 'y' 时是否需要信息提示 |
    | **$.fn.ajaxPostBlob** | --- | --- | --- | 封装的带 **token** 的二进制流 ajax 提交 |
    |  | *ajaxAsync* | boolean | true | ajax 的 async 属性 |
    |  | *ajaxType* | string | 'post' | ajax 的 type 属性 |
    |  | *ajaxData* | string | '' | 用 **new FormData()** 封装的 ajax 的 data 属性 |
    |  | *ajaxUrl* | string | '' | ajax 的 url 属性 |
    |  | *finished* | function | {} | ajax 请求完成时的回调 |
    |  | *succeed* | function | {} | ajax 请求完成并且 result === 'y' 时的回调 |
    |  | *failed* | function | {} | ajax 请求完成并且 result === 'n' 时的回调 |
    |  | *isMsg* | boolean | true | result === 'y' 时是否需要信息提示 |
    | **tipMsg** | --- | --- | --- | 提示框 |
    |  | *dom* | object | --- | 触发提示框的 DOM 对象 |
    |  | *msg* | string | --- | 提示框显示的内容 |
    |  | *position* | string | --- | 提示框的位置：<br>'top'<br>'bottom'<br>'left'<br>'right'<br>'center' |
    | **noticeMsg** | --- | --- | --- | 通知框 |
    |  | *msg* | string | --- | 通知框显示的内容 |
    |  | *type* | string | --- | 通知框的类型：<br>'info'<br>'success'<br>'warning'<br>'error' |
    |  | *position* | string | --- | 通知框的位置：<br>'center'<br>'top'<br>'left'<br>'right'<br>'bottom'<br>'left top'<br>'right top'<br>'left bottom'<br>'right bottom' |
    |  | *time* | number | --- | 自动消失的时间<br>单位：ms |
    |  | *hided* | function | --- | 通知框消失后的回调 |
    | **alertMsg** | --- | --- | --- | 警告框 |
    | **alertMsgBtn** | --- | --- | --- | 警告框小按钮 |
    | **alertMsgNoBtn** | --- | --- | --- | 警告框无按钮 |
    |  | *msg* | string | --- | 警告框显示的内容 |
    |  | *type* | string | --- | 警告框的类型：<br>'success'<br>'info'<br>'question'<br>'warning'<br>'error' |
    |  | *closed* | function | --- | 警告框关闭后的回调 |
    | **confirmMsg** | --- | --- | --- | 确认框 |
    | **confirmMsgBtn** | --- | --- | --- | 确认框小按钮 |
    |  | *title* | string | --- | 确认框显示的标题 |
    |  | *msg* | string | --- | 确认框显示的内容 |
    |  | *type* | string | --- | 确认框的类型：<br>'success'<br>'info'<br>'question'<br>'warning'<br>'error' |
    |  | *confirmed* | function | --- | 确认框确认后的回调 |
    | **divWindow** | --- | --- | --- | 弹出层 |
    |  | *title* | string | --- | 弹出层显示的标题 |
    |  | *width* | string | --- | 弹出层宽度<br>单位：px 或 % |
    |  | *height* | string | --- | 弹出层高度<br>单位：px 或 % |
    |  | *content* | object | --- | 弹出层显示的 DOM 对象 |
    | **iframeWindow** | --- | --- | --- | 弹出页 |
    |  | *title* | string | --- | 弹出页显示的标题 |
    |  | *width* | string | --- | 弹出页宽度<br>单位：px 或 % |
    |  | *height* | string | --- | 弹出页高度<br>单位：px 或 % |
    |  | *url* | string | --- | 弹出页显示的 iFrame 链接地址 |
    | **showBigPic** | --- | --- | --- | 大图预览 |
    |  | *url* | string | --- | 大图的绝对路径 |
    | **numericRange** | --- | --- | --- | 数字型范围 |
    |  | *rangeStart* | object | --- | 开始的 DOM 对象 |
    |  | *rangeEnd* | object | --- | 结束的 DOM 对象 |
    |  | *format* | string | --- | 格式 |
    |  | *decimals* | number | --- | 保留几位小数 |
    |  | *step* | number | --- | 步进 |
    |  | *min* | number | --- | 最小值 |
    |  | *max* | number | --- | 最大值 |
    | **dateRange** | --- | --- | --- | 日期型范围 |
    | **dateInputRange** | --- | --- | --- | 日期输入型范围 |
    |  | *rangeStart* | object | --- | 开始的 DOM 对象 |
    |  | *rangeEnd* | object | --- | 结束的 DOM 对象 |
    |  | *type* | string | --- | 日期的类型：<br>'Year'<br>'Month'<br>'Time'<br>'DateTime'<br>'Date' |
    | **serializeObject** | --- | --- | --- | 表单序列化 json 对象 |
    | **steps** | --- | --- | --- | 步骤条 |
    |  | *func* | function | --- | 步骤完成后的回调 |
    | **stepsForm** | --- | --- | --- | 表单步骤条 |
    |  | *func* | function | --- | 步骤完成后的回调 |
    | **stepsNoBack** | --- | --- | --- | 单向步骤条 |
    |  | *func* | function | --- | 步骤完成后的回调 |

#### 前后端交互规范：

* 前后端交互全部采用 `Ajax` 方式提交
* 前后端 `token` 均通过 `header` 的 `Authorization` 属性交互
* 前端提交给后端的数据格式分为标准的 `json` 格式和带二进制流的 `form data` 格式两种
* 后端返回给前端的 `json` 格式标准如下：
    ```json
    {
        "result": "y",
        "msg": "操作成功！",
        "data": []
    }
    ```
* Token 验证不通过时返回：
    ```json
    {
        "result": "denied"
    }
    ```
* 所有日期 `date` 类型的数据全部转换成字符串 `string` 类型进行交互，即前端给到后端和后端给到前端的数据均为字符串
    ```json
    {
        "year": "2019",
        "month": "2019-02",
        "date": "2019-02-03",
        "time": "12:00", 
        "datetime": "2019-02-03 12:00" 
    }
    ```
* 所有组件交互的数据格式请参考前端 Mock 数据目录 `json/`

## 📜 目录结构 <small>Directory Structure</small>

#### 完整版：

> 后台默认使用的是路由版~ 如需使用框架版、标签版和布局版，请直接将对应的 `index_xxx.html` 启动或直接改名为 `index.html` 即可~

```text
ROOT/··················································（项目根目录）
├── admin/·············································（后台管理目录）
│   ├── pages/·········································（iFrame 框架版目录）
│   │   ├── 404.html···································（404 页面）
│   │   ├── home.html··································（主页）
│   ├── views/·········································（SPA 路由版和标签版目录）
│   │   ├── xxx/·······································（后台管理子目录）
│   │   │   ├── xxx.html·······························（后台管理子页面及样式）
│   │   │   └── xxx.js·································（后台管理子页面脚本）
│   │   ├── 403.html···································（403 页面及样式）
│   │   ├── 403.js·····································（403 页面脚本）
│   │   ├── 404.html···································（404 页面及样式）
│   │   ├── 404.js·····································（404 页面脚本）
│   │   ├── 500.html···································（500 页面及样式）
│   │   ├── 500.js·····································（500 页面脚本）
│   │   ├── home.html··································（主页页面及样式）
│   │   ├── home.js····································（主页页面脚本）
│   │   ├── search.html································（搜索结果页面及样式）
│   │   └── search.js··································（搜索结果页面脚本）
│   ├── index.html·····································（后台登录后首页）
│   ├── index_iframe.html······························（首页框架版）
│   ├── index_router.html······························（首页路由版）
│   ├── index_splitter_iframe.html·····················（首页布局框架版）
│   ├── index_splitter_router.html·····················（首页布局路由版）
│   ├── index_splitter_tabstrip.html···················（首页布局标签版）
│   ├── index_tabstrip.html····························（首页标签版）
│   └── login.html·····································（后台登录、注册、找回密码页）
├── css/···············································（样式表目录）
│   ├── themes/········································（主题配色目录）
│   ├── amikoko.admin.css······························（后台管理样式）
│   ├── amikoko.site.css·······························（前台网站样式）
│   ├── bootstrap.min.css······························（Bootstrap 4）
│   ├── flag-icon.min.css······························（矢量国旗图标）
│   ├── fontawesome-all.min.css························（字体图标）
│   ├── kendo.ui.widgets.icon.css······················（Kendo UI 组件图标）
│   └── weather-icons.min.css··························（字体天气图标）
├── flags/·············································（矢量国旗图标目录）
├── fonts/·············································（字体图标目录）
├── img/···············································（图片目录）
│   ├── avatar.png·····································（默认头像）
│   ├── error.png······································（服务器出错图片）
│   ├── favicon.png····································（浏览器标签及收藏夹图标）
│   ├── IKKI.png·······································（用户头像）
│   ├── lock_bg.jpg····································（锁屏背景）
│   ├── logo.png·······································（标准 LOGO）
│   └── logo_s.png·····································（左侧导航折叠后 LOGO）
├── js/················································（公用脚本目录）
│   ├── global/········································（多语言目录）
│   ├── countUp.min.js·································（数字跳动）
│   ├── ikki.iframe.js·································（框架版脚本）
│   ├── ikki.js········································（公用脚本）
│   ├── ikki.layout.js·································（后台公用脚本）
│   ├── ikki.router.js·································（路由脚本）
│   ├── ikki.splitter.js·······························（布局版脚本）
│   ├── ikki.tabstrip.js·······························（标签版脚本）
│   ├── ikki.website.js································（前台公用脚本）
│   ├── jquery.min.js··································（jQuery 库）
│   ├── jquery.particleground.js·······················（登录页背景动画）
│   ├── jquery.verify.js·······························（登录页滑动验证）
│   ├── jszip.min.js···································（Excel 导出）
│   ├── kendo.all.min.js·······························（Kendo UI 库）
│   ├── L2Dwidget.0.min.js·····························（看板娘）
│   ├── L2Dwidget.min.js·······························（看板娘）
│   ├── md5.min.js·····································（MD5 加密）
│   ├── pdf.js·········································（PDF 查看）
│   └── pdf.worker.js··································（PDF 查看）
├── json/··············································（本地 Mock 数据目录）
│   └── geo/···········································（地图 GEO 数据目录）
├── resource/··········································（其他静态资源目录）
├── site/··············································（前台网站目录）
│   ├── pages/·········································（iFrame 框架版目录）
│   │   ├── 404.html···································（404 页面）
│   │   ├── home.html··································（主页）
│   ├── views/·········································（SPA 路由版和标签版目录）
│   │   ├── xxx/·······································（前台网站子目录）
│   │   │   ├── xxx.html·······························（前台网站子页面及样式）
│   │   │   └── xxx.js·································（前台网站子页面脚本）
│   │   ├── 404.html···································（404 页面及样式）
│   │   ├── 404.js·····································（404 页面脚本）
│   │   ├── home.html··································（主页页面及样式）
│   │   └── home.js····································（主页页面脚本）
│   └── index.html·····································（前台首页）
├── index.html·········································（项目首页）
├── LICENSE············································（MIT）
└── README.md··········································（本说明文档）
```
#### 纯后台管理路由精简版：

> 如果只需要后台管理界面的路由版~ 可将原 `admin` 目录下的文件移动至根目录并按照如下目录结构删除多余的文件~ 且将 `login.html` 和 `index.html` 头部 `<base>` 标签内的 `type` 置空即可~

```html
    <base href="http://localhost:8888/YourProject/" type="">
```

```text
ROOT/··················································（项目根目录）
├── css/···············································（样式表目录）
│   ├── themes/········································（主题配色目录）
│   │   └── theme_default.min.css······················（只保留默认样式）
│   ├── amikoko.admin.css······························（后台管理样式）
│   ├── bootstrap.min.css······························（Bootstrap 4）
│   └── fontawesome-all.min.css························（字体图标）
├── fonts/·············································（字体图标目录）
├── img/···············································（图片目录）
│   ├── avatar.png·····································（默认头像）
│   ├── error.png······································（服务器出错图片）
│   ├── favicon.png····································（浏览器标签及收藏夹图标）
│   ├── logo.png·······································（标准 LOGO）
│   └── logo_s.png·····································（左侧导航折叠后 LOGO）
├── js/················································（公用脚本目录）
│   ├── global/········································（多语言目录）
│   │   └── kendo.zh-CHS.js····························（只保留简体中文）
│   ├── countUp.min.js·································（数字跳动）
│   ├── ikki.js········································（后台管理脚本）
│   ├── ikki.layout.js·································（框架脚本）
│   ├── ikki.router.js·································（路由脚本）
│   ├── jquery.min.js··································（jQuery 库）
│   ├── jquery.verify.js·······························（登录页滑动验证）
│   ├── jszip.min.js···································（Excel 导出）
│   ├── kendo.all.min.js·······························（Kendo UI 库）
│   ├── md5.min.js·····································（MD5 加密）
│   ├── pdf.js·········································（PDF 查看）
│   └── pdf.worker.js··································（PDF 查看）
├── json/··············································（本地 Mock 数据目录）
├── views/·············································（SPA 页面目录）
│   ├── xxx/···········································（子目录）
│   │   ├── xxx.html···································（子页面及样式）
│   │   └── xxx.js·····································（子页面脚本）
│   ├── 403.html·······································（403 页面及样式）
│   ├── 403.js·········································（403 页面脚本）
│   ├── 404.html·······································（404 页面及样式）
│   ├── 404.js·········································（404 页面脚本）
│   ├── 500.html·······································（500 页面及样式）
│   ├── 500.js·········································（500 页面脚本）
│   ├── home.html······································（主页页面及样式）
│   ├── home.js········································（主页页面脚本）
│   ├── search.html····································（搜索结果页面及样式）
│   └── search.js······································（搜索结果页面脚本）
├── index.html·········································（登录后首页）
└── login.html·········································（后台入口登录、注册、找回密码页）
```

## 📁 功能列表 <small>Function List</small>

#### 基础功能 <small>[ Basic ]</small> ✔️

* [x] [登录页 <small>[ Login Page ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/login.html)
    * [x] 登录 <small>[ Sign In ]</small>
    * [x] 注册 <small>[ Register ]</small>
    * [x] 滑动验证 <small>[ Slide Verify ]</small>
    * [x] 记住密码 <small>[ Remember Password ]</small>
    * [x] 忘记密码 <small>[ Forget Password ]</small>
    * [x] MD5 加密 <small>[ MD5 Encryption ]</small>
    * [x] 看板娘 <small>[ Live2D ]</small>
* [x] [框架 <small>[ Admin Layout ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/index.html)
    * [x] 路由页面加载进度条 <small>[ Router Progress ]</small>
    * [x] 路由页面动画过渡 <small>[ Animated Transitions ]</small>
    * [x] 骨架屏 <small>[ Skeleton ]</small>
    * [x] 回到顶部 <small>[ Go Top ]</small>
    * [x] 聊天机器人 <small>[ Bot ]</small>
    * [x] 天气预报 <small>[ Weather Forecast ]</small>
    * [x] 万年历 <small>[ Perpetual Calendar ]</small>
    * [x] 便签 <small>[ Note ]</small>
* [x] [主页 <small>[ Home ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/home)
    * [x] 数字跳动 <small>[ Count Up ]</small>
* [x] [403 页 <small>[ 403 ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/403)
* [x] [404 页 <small>[ 404 ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/404)
* [x] [500 页 <small>[ 500 ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/500)

#### 顶部菜单 <small>[ Menu ]</small> ⏳

* [x] 导航折叠 <small>[ Navigation Drawer ]</small>
* [x] 面包屑 <small>[ Breadcrumb ]</small>
* [x] [全局搜索 <small>[ Global Search ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/search)
* [x] 刷新 <small>[ Refresh ]</small>
* [x] 全屏 <small>[ Full Screen ]</small>
* [x] 锁屏 <small>[ Lock Screen ]</small>
* [ ] 主题 <small>[ Theme ]</small>
    * [x] IKKI Amikoko
    * [x] Ant Design
    * [x] Kendo UI Less
    * [x] Kendo UI Sass
    * [x] Bootstrap v4
    * [x] Material Design
    * [ ] Classic
    * [ ] Nouvelle
* [ ] 语言 <small>[ Localization ]</small>
    * [x] 中文简体 <small>[ 汉语简 ]</small>
    * [x] 中文繁體 <small>[ 汉语繁 ]</small>
    * [x] English <small>[ 英语 ]</small>
    * [ ] Русский <small>[ 俄语 ]</small>
    * [ ] Français <small>[ 法语 ]</small>
    * [ ] Deutsch <small>[ 德语 ]</small>
    * [ ] Italiano <small>[ 意大利语 ]</small>
    * [ ] Português <small>[ 葡萄牙语 ]</small>
    * [ ] Español <small>[ 西班牙语 ]</small>
    * [ ] العربية <small>[ 阿拉伯语 ]</small>
    * [ ] 日本語 <small>[ 日语 ]</small>
    * [ ] 한국어 <small>[ 韩语 ]</small>
* [x] [信息 <small>[ Message ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/users/message)
    * [x] 写信 <small>[ Write Letters ]</small>
    * [x] 收信箱 <small>[ Inbox ]</small>
    * [x] 发信箱 <small>[ Outbox ]</small>
    * [x] 短消息 <small>[ SMS ]</small>
    * [x] 通讯录 <small>[ Address Book ]</small>
* [x] [提醒 <small>[ Notice ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/users/notice)
    * [x] 系统通知 <small>[ System Notification ]</small>
    * [x] 个人动态 <small>[ User Updating ]</small>
    * [x] 待办事项 <small>[ To Do Items ]</small>
* [x] 用户名头像显示 <small>[ User Name & Avatar ]</small>
    * [x] [前台切换 <small>[ Goto Website ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/site/index.html)
    * [x] [用户中心 <small>[ User Center ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/users/account)
    * [x] 修改密码 <small>[ Change Password ]</small>
    * [x] [系统设置 <small>[ Setting ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/users/setting) 
    * [x] [导航编辑 <small>[ Navigation Editor ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/users/navigation)
    * [x] 退出登录 <small>[ Sign Out ]</small>

#### 左侧导航 <small>[ Navigation ]</small> ⏳

* [x] 综合 <small>[ Dashboard ]</small>
    * [x] 表单 <small>[ Forms ]</small>
        * [x] [基础表单元素](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/forms/form_elements)
        * [x] [表单 Post 提交](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/forms/form_post)
        * [x] [表单 Ajax 提交](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/forms/form_ajax)
        * [x] [范围选择](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/forms/form_range)
        * [x] [模糊搜索选择](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/forms/form_search)
        * [x] [弹出框选择](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/forms/form_popup)
        * [x] [下拉分组多选级联](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/forms/form_cascade)
        * [x] [地图联动选择](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/forms/form_map)
        * [x] [组合动态复制新增](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/forms/form_copy)
    * [x] 表格 <small>[ Grids ]</small>
        * [x] [全功能搜索及自定义编辑](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/grids/grid_custom)
        * [x] [弹出框带校验编辑](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/grids/grid_popup)
        * [x] [行内带校验编辑](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/grids/grid_inline)
        * [x] [单元格带校验编辑](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/grids/grid_incell)
        * [x] [复制新增及数据联动编辑](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/grids/grid_copy)
        * [x] [自定义功能按钮](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/grids/grid_button)
        * [x] [自定义选择提交](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/grids/grid_select)
        * [x] [分组合计排序筛选](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/grids/grid_group)
        * [x] [子表详情及滚动翻页](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/grids/grid_detail)
        * [x] [合并表头及行内拆分](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/grids/grid_merge)
    * [x] 树形 <small>[ Trees ]</small>
        * [x] [全功能拖放及自定义编辑](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/trees/tree_custom)
        * [x] [节点选择编辑](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/trees/tree_edit)
        * [x] [自定义图标](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/trees/tree_icon)
        * [x] [复选框及半选提交](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/trees/tree_checkbox)
    * [x] 列表 <small>[ Lists ]</small>
        * [x] [全功能排版及自定义编辑](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/lists/list_custom)
        * [x] [多模块自定义排版](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/lists/list_layout)
        * [x] [大图列表切换](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/lists/list_switch)
        * [x] [间隔项模版及滚动翻页](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/lists/list_alternate)
    * [x] 分配 <small>[ Assigns ]</small>
        * [x] [表格搜索双击选择](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/assigns/assign_grid)
        * [x] [树形直接拖放](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/assigns/assign_drag)
        * [x] [树形同级双击选择](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/assigns/assign_tree)
        * [x] [列表搜索双击选择](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/assigns/assign_list)
        * [x] [穿梭框拖放](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/dashboard/assigns/assign_listbox)
* [x] 框架 <small>[ Framework ]</small>
    * [x] [全球化 <small>[ Globalization ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/framework/globalization)
    * [x] [视图模型 <small>[ MVVM ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/framework/mvvm)
    * [x] [数据源 <small>[ DataSource ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/framework/datasource)
    * [x] [模版 <small>[ Templates ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/framework/templates)
    * [x] [绘图 <small>[ Drawing ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/framework/drawing)
    * [x] [单页应用 <small>[ SPA ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/framework/spa)
    * [x] [PDF导出 <small>[ PDF Export ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/framework/pdf_export)
    * [x] [PDF查看 <small>[ PDF Viewer ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/framework/pdf_viewer)
    * [x] [触摸事件 <small>[ Touch Events ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/framework/touch_events)
    * [x] [整合 <small>[ Integration ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/framework/integration)
* [ ] 布局 <small>[ Layout ]</small>
    * [x] [页面布局 <small>[ Splitter ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/layout/splitter)
    * [x] [响应面板 <small>[ Responsive Panel ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/layout/responsive_panel)
    * [x] [徽标 <small>[ Badge ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/layout/badge)
    * [x] [卡片 <small>[ Cards ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/layout/cards)
    * [x] [磁贴布局 <small>[ TileLayout ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/layout/tilelayout)
    * [ ] [表单 <small>[ Form ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/layout/form)
    * [x] [模态框 <small>[ Window ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/layout/window)
    * [x] [对话框 <small>[ Dialog ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/layout/dialog)
    * [x] [通知框 <small>[ Notification ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/layout/notification)
    * [x] [提示框 <small>[ Tooltip ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/layout/tooltip)
* [ ] 导航 <small>[ Navigation ]</small>
    * [x] [导航栏 <small>[ AppBar ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/navigation/appbar)
    * [x] [面包屑 <small>[ Breadcrumb ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/navigation/breadcrumb)
    * [x] [菜单 <small>[ Menu ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/navigation/menu)
    * [x] [折叠面板 <small>[ PanelBar ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/navigation/panelbar)
    * [x] [抽屉 <small>[ Drawer ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/navigation/drawer)
    * [x] [选项卡 <small>[ TabStrip ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/navigation/tabstrip)
    * [ ] [步骤条 <small>[ Stepper ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/navigation/stepper)
    * [ ] [向导 <small>[ Wizard ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/navigation/wizard)
    * [x] [工具栏 <small>[ ToolBar ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/navigation/toolbar)
    * [x] [树形视图 <small>[ TreeView ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/navigation/treeview)
    * [x] [时间线 <small>[ Timeline ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/navigation/timeline)
    * [x] [按钮 <small>[ Button ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/navigation/button)
    * [x] [按钮组 <small>[ ButtonGroup ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/navigation/buttongroup)
* [x] 表单 <small>[ Forms ]</small>
    * [x] [文本框 <small>[ TextBox ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/forms/textbox)
    * [x] [文本域框 <small>[ TextArea ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/forms/textarea)
    * [x] [单选框 <small>[ Radio ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/forms/radio)
    * [x] [复选框 <small>[ CheckBox ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/forms/checkbox)
    * [x] [转换框 <small>[ Switch ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/forms/switch)
    * [x] [数字框 <small>[ NumericTextBox ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/forms/numerictextbox)
    * [x] [日期框 <small>[ DatePicker ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/forms/datepicker)
    * [x] [日期范围框 <small>[ DateRangePicker ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/forms/daterangepicker)
    * [x] [时间框 <small>[ TimePicker ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/forms/timepicker)
    * [x] [时日框 <small>[ DateTimePicker ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/forms/datetimepicker)
    * [x] [时日掩码框 <small>[ DateInput ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/forms/dateinput)
    * [x] [掩码框 <small>[ MaskedTextBox ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/forms/maskedtextbox)
    * [x] [自动完成框 <small>[ AutoComplete ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/forms/autocomplete)
    * [x] [单选下拉框 <small>[ DropDownList ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/forms/dropdownlist)
    * [x] [输入下拉框 <small>[ ComboBox ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/forms/combobox)
    * [x] [表格下拉框 <small>[ MultiColumnComboBox ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/forms/multicolumncombobox)
    * [x] [多选下拉框 <small>[ MultiSelect ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/forms/multiselect)
    * [x] [树形下拉框 <small>[ DropDownTree ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/forms/dropdowntree)
    * [x] [颜色框 <small>[ ColorPicker ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/forms/colorpicker)
    * [x] [滑块框 <small>[ Slider ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/forms/slider)
    * [x] [评级框 <small>[ Rating ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/forms/rating)
    * [x] [穿梭框 <small>[ ListBox ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/forms/listbox)
    * [x] [富文本框 <small>[ Editor ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/forms/editor)
    * [x] [上传框 <small>[ Upload ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/forms/upload)
    * [x] [图像编辑框 <small>[ ImageEditor ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/forms/imageeditor)
    * [x] [验证 <small>[ Validator ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/forms/validator)
* [x] 数据 <small>[ Data ]</small>
    * [x] [表格 <small>[ Grid ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/data/grid)
    * [x] [树形列表 <small>[ TreeList ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/data/treelist)
    * [x] [列表视图 <small>[ ListView ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/data/listview)
    * [x] [分页 <small>[ Pager ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/data/pager)
    * [x] [电子表格 <small>[ Spreadsheet ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/data/spreadsheet)
    * [x] [透视表格 <small>[ PivotGrid ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/data/pivotgrid)
    * [x] [过滤器 <small>[ Filter ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/data/filter)
    * [x] [文件管理 <small>[ FileManager ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/data/filemanager)
* [x] 日程 <small>[ Scheduling ]</small>
    * [x] [日历 <small>[ Calendar ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/scheduling/calendar)
    * [x] [多重日历 <small>[ MultiViewCalendar ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/scheduling/multiviewcalendar)
    * [x] [日程表 <small>[ Scheduler ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/scheduling/scheduler)
    * [x] [甘特图 <small>[ Gantt ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/scheduling/gantt)
* [x] 会话 <small>[ Conversational ]</small>
    * [x] [聊天 <small>[ Chat ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/conversational/chat)
* [x] 媒体 <small>[ Media ]</small>
    * [x] [媒体播放器 <small>[ MediaPlayer ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/media/mediaplayer)
    * [x] [滚动视图 <small>[ ScrollView ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/media/scrollview)
* [x] 交互 <small>[ Interactivity ]</small>
    * [x] [拖放 <small>[ Drag and Drop ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/interactivity/drag_and_drop)
    * [x] [拖放排序 <small>[ Sortable ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/interactivity/sortable)
    * [x] [加载中 <small>[ Loader ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/interactivity/loader)
    * [x] [进度条 <small>[ ProgressBar ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/interactivity/progressbar)
    * [x] [样式 <small>[ Styling ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/interactivity/styling)
    * [x] [特效 <small>[ Effects ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/interactivity/effects)
    * [x] [波纹效果 <small>[ Ripple Container ]</small>](https://ikki2000.gitee.io/kendoui-admin-site/admin/#/interactivity/ripple_container)
* [ ] 图表 <small>[ Charts ]</small>
    * [ ] 区域图 <small>[ Area Charts ]</small>
    * [ ] 条形图 <small>[ Bar Charts ]</small>
    * [ ] 箱线图 <small>[ Box Plot Charts ]</small>
    * [ ] 气泡图 <small>[ Bubble Charts ]</small>
    * [ ] 子弹图 <small>[ Bullet Charts ]</small>
    * [ ] 环形图 <small>[ Donut Charts ]</small>
    * [ ] 漏斗图 <small>[ Funnel Charts ]</small>
    * [ ] 折线图 <small>[ Line Charts ]</small>
    * [ ] 饼图 <small>[ Pie Charts ]</small>
    * [ ] 极坐标图 <small>[ Polar Charts ]</small>
    * [ ] 雷达图 <small>[ Radar Charts ]</small>
    * [ ] 散点图 <small>[ Scatter Charts ]</small>
    * [ ] 波形图 <small>[ Sparklines ]</small>
    * [ ] 股票图 <small>[ Stock Charts ]</small>
    * [ ] 树图 <small>[ TreeMap ]</small>
    * [ ] 瀑布图 <small>[ Waterfall Charts ]</small>
    * [ ] 范区域图 <small>[ Range Area Charts ]</small>
    * [ ] 范条形图 <small>[ Range Bar Charts ]</small>
    * [ ] 量规 <small>[ Gauges ]</small>
        * [ ] 线性计 <small>[ Linear Gauge ]</small>
        * [ ] 径向计 <small>[ Radial Gauge ]</small>
        * [ ] 弧形计 <small>[ Arc Gauge ]</small>
    * [ ] 条码 <small>[ Barcodes ]</small>
        * [ ] 条形码 <small>[ Barcode ]</small>
        * [ ] 二维码 <small>[ QR Code ]</small>
    * [ ] 地图 <small>[ Maps ]</small>
        * [ ] 架构图 <small>[ Diagram ]</small>
        * [ ] 地图 <small>[ Map ]</small>
* [ ] 移动端 <small>[ Hybrid ]</small>
    * [ ] 动作面板 <small>[ ActionSheet ]</small>
    * [ ] 整体应用 <small>[ Application ]</small>
    * [ ] 按钮 <small>[ Button ]</small>
    * [ ] 按钮组 <small>[ ButtonGroup ]</small>
    * [ ] 折叠面板 <small>[ Collapsilble ]</small>
    * [ ] 抽屉 <small>[ Drawer ]</small>
    * [ ] 表单 <small>[ Forms ]</small>
    * [ ] 表格 <small>[ Grid ]</small>
    * [ ] 列表 <small>[ ListView ]</small>
    * [ ] 模态框 <small>[ ModalView ]</small>
    * [ ] 导航栏 <small>[ NavBar ]</small>
    * [ ] 气泡层 <small>[ PopOver ]</small>
    * [ ] 日程表 <small>[ Scheduler ]</small>
    * [ ] 滚动 <small>[ Scroller ]</small>
    * [ ] 滚动视图 <small>[ ScrollView ]</small>
    * [ ] 拆分视图 <small>[ SplitView ]</small>
    * [ ] 样式 <small>[ Styling ]</small>
    * [ ] 滑动开关 <small>[ Switch ]</small>
    * [ ] 标签页 <small>[ TabStrip ]</small>
    * [ ] 触摸事件 <small>[ Touch Events ]</small>
    * [ ] 视图 <small>[ View ]</small>

## 🔗 相关链接 <small>Thanks for</small>

* [jQuery](https://github.com/jquery/jquery) ( v1.12.4 ) < Support for v3.5.1 >
* [Kendo UI for jQuery 官网](https://www.telerik.com/kendo-jquery-ui) ( v2021.1.224 )
* [Kendo UI Default Theme](https://github.com/telerik/kendo-themes/tree/develop/packages/default) ( v4.36.0 )
* [Kendo UI Bootstrap Theme](https://github.com/telerik/kendo-themes/tree/develop/packages/bootstrap) ( v4.33.1 )
* [Kendo UI Material Theme](https://github.com/telerik/kendo-themes/tree/develop/packages/material) ( v3.31.1 )
* [Bootstrap](https://github.com/twbs/bootstrap) ( v4.6.0 )
* [Font Awesome](https://github.com/FortAwesome/Font-Awesome) ( v5.15.2 )
* [Ant Design Colors](https://ant.design/docs/spec/colors-cn)
* [Material Design Colors](https://www.material.io/design/color/the-color-system.html#tools-for-picking-colors)
* [Flag Icon Css](https://github.com/lipis/flag-icon-css) ( v3.5.0 )
* [Weather Icons](https://github.com/erikflowers/weather-icons) ( v2.0.10 )
* [Count Up](https://github.com/inorganik/countUp.js) ( v1.9.3 )
* [Verify](https://github.com/Hibear/verify) ( v0.1.1 )
* [JavaScript MD5](https://github.com/blueimp/JavaScript-MD5) ( v2.18.0 )
* [Particleground](https://github.com/jnicol/particleground) ( v1.1.0 )
* [Live2D Widget](https://github.com/xiazeyu/live2d-widget.js) ( v3.0.5 )
* [JetBrains Open Source Support Program](https://www.jetbrains.com/community/opensource/)
* [JetBrains WebStorm](https://www.jetbrains.com/?from=KendoUI-Admin-Site) ( v2020.3.3 x64 )

## 📷 界面预览 <small>Screenshot</small>

<div align="center">

### -= PC =-

![PC](https://gitee.com/IKKI2000/KendoUI-Admin-Site/raw/master/img/screenshot/home_pc.png)

### -= PAD =-

![PAD](https://gitee.com/IKKI2000/KendoUI-Admin-Site/raw/master/img/screenshot/home_pad.png)

### -= PHONE =-

![PHONE](https://gitee.com/IKKI2000/KendoUI-Admin-Site/raw/master/img/screenshot/home_phone.png)

</div>